// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            March 8th, 2021
// File:			
// Module name:		
// Project Name:	
// Description:		

import top_p2_pkg::*;

module add_subs_q0q1_mux(
    input datos_t i_adder_result,//opcion 0
    input datos_t i_subs_result,//opcion 1
    input datos_t i_a_value,
    input mult_mux_selector i_selector,
    output datos_t o_selection
);

datos_t temp_output;

always_comb begin : operator
    case (i_selector)
        ADDITION: begin
            temp_output = i_adder_result;
        end
        SUBSTRACTION: begin
            temp_output = i_subs_result;
        end
        default: begin
            temp_output = i_a_value;
        end
    endcase
end

assign o_selection = temp_output;

endmodule