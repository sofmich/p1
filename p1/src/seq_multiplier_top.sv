// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            February 8th, 2021
// File:			seq_multiplier_top.sv
// Module name:		seq_multiplier_top
// Project Name:	sequential_multiplier
// Description:		this is the data path module

import top_p1_pkg::*;

module seq_multiplier_top(
input bit       i_clk,
input bit       i_rst_n,
input bit		i_start,
input bit 		i_en_load,
input bit		i_en_processing,
input bit		i_en_output,
input bit		i_clean,
input data_t    i_multiplier,
input data_t	i_multiplicand,
output segments_t o_units,
output segments_t o_tens,
output segments_t o_cents,
output logic o_sign,
output product_t o_product

);
	
data_t mr_reg;
data_t md_reg;
data_t sign_md;
data_t mr_complemented;
product_t shifted_md;
product_t add_product; 
product_t product_complete;
product_t product_signed;
product_t product_display;
bit sign_mr;
bit add;
bit mr_add_enable;
	
flipflopD save_MR(
	.clk(i_clk),
	.rst_n(i_rst_n),
	.en(i_start),
	.d(i_multiplier),
	.q(mr_reg)
	);
assign sign_mr = (mr_reg[N-1]) ? 1'b1 : 1'b0;
assign mr_complemented = (mr_reg[N-1]) ? (~mr_reg + 1'b1): mr_reg ;	

	
flipflopD save_MD(
	.clk(i_clk),
	.rst_n(i_rst_n),
	.en(i_start),
	.d(i_multiplicand),
	.q(md_reg)
	);	
assign sign_md = (md_reg[N-1])?  {N{1'b1}}:
                        
                        
                                 {N{1'b0}};
piso_lsb mr_shift(
	.clk(i_clk),
	.rst(i_rst_n),
	.enb(i_en_processing),
	.l_s(i_en_load),
	.inp(mr_complemented),
	.out(mr_add_enable)
	);

assign add = i_en_processing & mr_add_enable ;  //State machine	
	
shift_left md_shift (
	.clk(i_clk),
	.rst_n(i_rst_n),
	.en(i_en_processing),
	.load(i_en_load),
	.sign(sign_md),
	.number(md_reg),
	.shifted_number(shifted_md)	
	);

assign add_product = shifted_md + product_complete;
	
flipflopD_clean adder_product(
	.clk(i_clk),
	.rst_n(i_rst_n),
	.en(i_en_processing),
	.clean(i_clean),
	.adding(add),
	.d(add_product),
	.q(product_complete)
	);

//sign_mr, product_complete
assign product_signed = (sign_mr)? (~product_complete + 1'b1) : product_complete;

assign o_product = product_display;
flipflopD_2N product_reg(
	.clk(i_clk),
	.rst_n(i_rst_n),
	.en(i_en_output),
	.d(product_signed),
	.q(product_display)
	);
	
decoder_bcd_top display_product(
	.number(product_display),
	.units(o_units),
	.tens(o_tens),
	.cents(o_cents),
	.sign(o_sign)
	);	

endmodule

