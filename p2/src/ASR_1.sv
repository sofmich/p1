// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            March 8th, 2021
// File:			
// Module name:		
// Project Name:	
// Description:		

import top_p2_pkg::*;

module ASR_1(
    input full_reg_t i_full_vector,
    output full_reg_t o_shifted_vector
);
//porque no podemos hacer un ASR de un registro????????????????????/
full_reg_t temp_output;
full_reg_t aux;
always_comb begin : shift_right
    //temp_output = i_full_vector >> 1;
    aux = i_full_vector & 'h100000;
    temp_output = aux | (i_full_vector >> 1);
end

assign o_shifted_vector = temp_output;

endmodule