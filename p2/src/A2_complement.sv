// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            March 8th, 2021
// File:			
// Module name:		
// Project Name:	
// Description:		
// todo: revisar si se puede mejorar esta parte del complemento a2
import top_p2_pkg::*;

module A2_complement(
    input datos_t i_abs_data,
    output datos_t o_signed_data
);

datos_t a1_data;
datos_t temp_data;

//modulo para obtener el signo y magnitud del valor de entrada
always_comb begin : complemento
    a1_data = ~i_abs_data;
    temp_data = a1_data+1;
end

assign o_signed_data = temp_data;

endmodule