// Coder:         Eduardo Ethandrake, Sofi Michel Salazar Valdovinos
// Description:   This code is a flip-flop D

import sdp_dc_ram_pkg::*;

module ff_d_reg#(
		parameter W_ADDR = 4,
		parameter W_DATA = 8
	) (
input bit 	clk,
input logic rst,
input logic enb,
input logic [W_ADDR:0] inp,
output logic [W_ADDR:0] out
);

logic [W_ADDR:0]  ff_d_r;

always_ff@(posedge clk or negedge rst) begin
    if(!rst)
        ff_d_r  <= '0;
    else if (enb)
        ff_d_r  <= inp;
end

assign out  = ff_d_r;

endmodule
