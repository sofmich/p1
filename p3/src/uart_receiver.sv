// Coder:           Sofia Michel, Ethan Castillo
// Date:            April 7th, 2021
// File:			uart_rx.sv
// Description:		This module is the reception interface for a uart driver

 
import uart_pkg::*;

module uart_receiver(
input bit i_clk,
input logic	i_rst_n,
input logic serial_data_rx,
input logic i_clear_interrupt,
output logic rx_interrupt,
output data_t o_received_data,
output logic o_parity_error
);

//captar el flanco de bajada de 1 a 0
//contar medio delay para muestrear el dato (start)
//hacer delays completos para muestrear los siguientes datos
//una vez leido el ultimo dato comparar el bit de paridad recibido contra el generado
//pasar al estado de stop
//pasar al estado bloqueante


//hacer contador


logic en_cntr_cycle;
logic delay_selector;
logic ovf_cycle;
cnt_t delay_selection;
logic en_sipo;
logic ovf_data;
data_t received_data;
logic generated_parity;
logic en_parity;
logic received_parity;
logic parity_error;
logic ovf_start;
logic en_calibrate;

rx_state_machine rx_states(
    .i_clk(i_clk),
    .i_reset_n(i_rst_n),
    .i_reception(serial_data_rx),
    .i_clear_interrupt(i_clear_interrupt),
    .i_ovf_cycle(ovf_cycle),
    .i_ovf_data(ovf_data),
    .o_en_cntr_cycle(en_cntr_cycle),
    .o_delay_selector(delay_selector),
    //.o_en_load(),
    .o_en_sipo(en_sipo),
    .o_en_parity(en_parity),
    .i_ovf_start(ovf_start),
    .o_calibrate(en_calibrate),
    .o_rx_interrupt(rx_interrupt)
);

multiplexor #(
.MUX_DW(DELAY_DW+1)
)delay_mux(
    .i_first(mux_delay_t'(DELAY_MAXCNT)),//opcion 0
    .i_second(mux_delay_t'(HALF_DELAY_MAXCNT)),//opcion 1
    .i_selector(delay_selector),//0),
    .o_selection(delay_selection)
);

/*#(.MAXCNT(delay_selection))*/ 
delay_counter delay_cntr(
	.i_clk(i_clk),
	.i_reset_n(i_rst_n),
	.i_enb(en_cntr_cycle),//provisional),//en_cntr_cycle),
	.i_max_cnt(delay_selection),//delay_selection),
	.o_ovf(ovf_cycle)
);

capture_counter data_cntr(
	.i_clk(i_clk),
	.i_reset_n(i_rst_n),
	.i_enb(en_sipo),//en_cntr_cycle),//provisional),//en_cntr_cycle),
    .i_stimulus(ovf_cycle),
	.i_max_cnt(data_t'(DW)),//delay_selection),
	.o_ovf(ovf_data)//ovf_cycle)
);

capture_counter start_cntr(
	.i_clk(i_clk),
	.i_reset_n(i_rst_n),
	.i_enb(en_calibrate),//en_cntr_cycle),//provisional),//en_cntr_cycle),
    .i_stimulus(ovf_cycle),
	.i_max_cnt(data_t'(2)),//delay_selection),
	.o_ovf(ovf_start)//ovf_cycle)
);

sipo #(.SIZE(DW)) sipo_reg(
    .i_clk(i_clk), 
    .i_rst_n(i_rst_n),
    .i_serial_data(serial_data_rx),
    .capture(ovf_cycle&en_sipo),
    .o_paralel_data(received_data)
    //.ready()
);

flipflop #(.SIZE(1))ff(
    .clk(i_clk),
    .rst_n(i_rst_n),
    .en(ovf_cycle&en_parity),
    .d(serial_data_rx), 
    .q(received_parity)
);

parity_gen parity(
    .i_data(received_data),
    .o_parity_check(generated_parity)
);

always_comb begin : parity_check
    parity_error = (generated_parity == received_parity) ? 1'b0:1'b1;
end
assign o_received_data = received_data;
assign o_parity_error = parity_error;
endmodule

