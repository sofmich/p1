// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            February 8th, 2021
// File:			shift_left.sv
// Module name:		shift_left
// Project Name:	p1
// Description:		this is a shifter to the left 

import top_p1_pkg::*;

module shift_left(
input bit       clk,
input bit       rst_n,
input bit 		en,
input bit		load,
input data_t 	sign,
input data_t    number,
output product_t   shifted_number

	);

product_t shifted_number_aux;


always_ff@(posedge clk, negedge rst_n) begin
	if(!rst_n)
		shifted_number_aux <= '0;
	else if(load)
		shifted_number_aux <= {sign,number}; //When on LOAD status, assign the number
	else if(en) //processing
		shifted_number_aux <= shifted_number_aux << 1; //Makes shifts while processing data
end

assign shifted_number = shifted_number_aux;

endmodule


