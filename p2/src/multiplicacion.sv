// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            March 8th, 2021
// File:			
// Module name:		
// Project Name:	
// Description:		

import top_p2_pkg::*;

module multiplicacion(
    input bit i_clk,
    input logic     i_rst_n,
    input datos_t i_signed_M,
    input datos_t i_signed_Q,
    input q_state_selector i_Q_selector,
    input logic enable_QR,
    output datos_t o_result
);

//definicion de los valores absolutos
datos_t abs_M;
datos_t abs_Q;
//definicion de los signos
logic sign_M;
logic sign_Q;

full_reg_t full_vector;
full_reg_t registered_vector;
full_reg_t shifted_vector;

datos_t q0q1_selected_data;

datos_t add_sub_result;

datos_t final_a2_q;

datos_t result_value;

logic signo;
// entra el valor de M signado
full_reg_t concatenacion;
// entra el valor de Q signado

// cada valor entra a su respectivo modulo de complemento A2
// de cada bloque de complemento A2 sale el respectivo signo y la señal de absoluto
A2_decomplement a2_decomplement_M(
    .i_signed_data(i_signed_M),
    .o_abs_data(abs_M),
    .o_sign(sign_M)
);

A2_decomplement a2_decomplement_Q(
    .i_signed_data(i_signed_Q),
    .o_abs_data(abs_Q),
    .o_sign(sign_Q)
);

always_comb begin : signo_final
    signo = sign_M^sign_Q;
    concatenacion = {add_sub_result,registered_vector[N:0]};
end

multiplexor3 mult_multiplexor(
    .i_q_abs(abs_Q),
    //.i_a_value(add_sub_result),
    .i_full_vector(shifted_vector),
    .i_selector(i_Q_selector),
    //.i_q0_value(registered_vector[0]),
    //.i_q_value(registered_vector[N:1]),
    .o_selection(full_vector)
);

flipflopD_2N_1 reg_q(
    .clk(i_clk), 
    .rst_n(i_rst_n),
    .en(enable_QR),
    .d(full_vector),
    .q(registered_vector)
);

multiplexor2 q1q0_number(
    .i_m_abs(abs_M),
    .i_selector(mult_mux_selector'(registered_vector[1:0])),
    .o_selection(q0q1_selected_data)
);

add_subs operation(
    .i_a_value(registered_vector[2*N:N+1]),
    .i_m_value(q0q1_selected_data),
    .i_selector(mult_mux_selector'(registered_vector[1:0])),
    .o_selection(add_sub_result)
);



ASR_1 shift_r_right(
    .i_full_vector(concatenacion),
    .o_shifted_vector(shifted_vector)
);

A2_complement final_a2_complement(
    .i_abs_data(registered_vector[N:1]),
    .o_signed_data(final_a2_q)
);

multiplexor result_mux(
    .i_first(registered_vector[N:1]),
    .i_second(final_a2_q),
    .i_selector(signo),
    .o_selection(result_value)
);

assign o_result = result_value;

endmodule