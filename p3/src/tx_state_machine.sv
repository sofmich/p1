// Coder:           Sofia Michel, Ethan Castillo
// Date:            April 7th, 2021
// File:            tx_state_machine.sv
// Description:     State machine for transmission


import uart_pkg::*;


module tx_state_machine (
		input bit i_clk,
		input logic i_rst,
		input logic i_transmission,
		input logic i_overflow,
		input logic i_overflow_cycle,
		output logic o_en_cntr,
		output logic o_en_cntr_cycle,
		output logic o_en_start,
		output logic o_load,
		output logic o_en_piso,
		output logic o_en_out,
		output logic o_en_parity,
		output logic o_en_stop,
		output sm_uart_e o_current_state
	);
	sm_uart_e current_state = IDLE;
	sm_uart_e next_state = IDLE;
	always_comb begin: tx_states
		case(current_state)
			IDLE:
				if(i_transmission == TRUE)
					next_state = START; //State when started
				else
					next_state = IDLE; //State idle
			START:
				if(i_overflow_cycle)
					next_state = DATA;
				else
					next_state = START;

			DATA:
				if(!i_overflow)
					next_state = DATA;
				else
					next_state = PARITY;
			PARITY:
				if(!i_overflow_cycle)
					next_state = PARITY;
				else
					next_state = STOP;
			STOP:
				if(!i_overflow_cycle)
					next_state = STOP;
				else
					next_state = IDLE;

		endcase
	end


always_ff @( posedge i_clk or negedge i_rst ) begin 
    if(!i_rst)begin
        current_state <= IDLE;
    end
    else
        current_state <= next_state;
end


always_comb begin
	case(current_state)
		IDLE: begin
			o_en_out = FALSE;
			o_en_cntr = FALSE;
			o_en_cntr_cycle = FALSE;
			o_en_piso = TRUE;
			o_load = TRUE;
			o_en_parity = FALSE;
			o_en_start = FALSE;
			o_en_stop = TRUE;
		end
		START: begin
			o_en_start = TRUE;
			o_en_out = FALSE;
			o_en_cntr = FALSE;
			o_en_cntr_cycle = TRUE;
			o_en_piso = TRUE;
			o_load = TRUE;
			o_en_parity = FALSE;
			o_en_stop = FALSE;
		end
		DATA: begin
			o_en_start = FALSE;
			o_en_out = TRUE;
			o_en_cntr = TRUE;
			o_en_cntr_cycle = TRUE;
			o_en_piso = TRUE;
			o_load = FALSE;
			o_en_parity = FALSE;
			o_en_stop = FALSE;
		end
		PARITY: begin
			o_en_start = FALSE;
			o_en_out = TRUE;
			o_en_cntr = FALSE;
			o_en_cntr_cycle = TRUE;
			o_en_piso = FALSE;
			o_load = FALSE;
			o_en_parity = TRUE;
			o_en_stop = FALSE;
		end
		STOP: begin
			o_en_start = FALSE;
			o_en_out = FALSE;
			o_en_cntr = FALSE;
			o_en_cntr_cycle = TRUE;
			o_en_piso = FALSE;
			o_load = FALSE;
			o_en_parity = FALSE;
			o_en_stop = TRUE;
		end
		default: begin
			o_en_start = FALSE;
			o_load = FALSE;
			o_en_cntr = FALSE;
			o_en_cntr_cycle = FALSE;
			o_en_piso = FALSE;
			o_load = FALSE;
			o_en_parity = FALSE;
			o_en_stop = FALSE;
		end
	endcase
end

assign o_current_state = current_state;

endmodule

