//Coder: Ethan Castillo Sofia Salazar
//Date: 12/02/2021

`ifndef TOP_P2_PKG_SV
    `define TOP_P2_PKG_SV
    
    package top_p2_pkg;
        localparam N = 10;
        localparam N1 = N+1;
        localparam N2 = N*2;
        typedef logic [N-1:0] datos_t;
        typedef logic [(N*2)-1:0] product_t;
        typedef logic [N-1:0] operand_t;
        typedef logic [1:0] operation_t;
        ////////////////////////////////////////
        typedef logic [1:0] bb_mux_selector_t;
        typedef logic [(N*2):0] full_reg_t;
        typedef logic [(N*2)-1:0] reg_2N_t;
        typedef logic signed [(N*2)-1:0] reg_2N_signed_t;
	    localparam ZEROS = '0;
        localparam ZERO = 0;
        localparam ONE = 1;
        localparam THREE = 3;
        ////////////////////////////////////////
        localparam W_ST = 3; 
        typedef enum logic [W_ST-1:0]{
            IDLEE = 3'b000,
            LOAD = 3'b001,
            PROCESSING = 3'b010,
            OUTPUT_DISPLAYY = 3'b011,
            CLEANING = 3'b100
        } ctrl_state;
        localparam      SEGMENTS = 7;
    typedef logic   [3:0] bcd_t;
    typedef logic   [SEGMENTS-1:0] segments_t;
	    
        
        typedef enum logic{
            FIRST = 1'b0,
            SECOND = 1'b1
        } q_state_selector;
        
        localparam W_SS = 2; 
        typedef enum logic [W_SS-1:0]{
            ADDITION = 2'b01,
            SUBSTRACTION = 2'b10
        } mult_mux_selector;

        typedef enum logic [W_SS-1:0]{ 
            MULTIPLICATION = 2'b00,
            DIVISION = 2'b01,
            SQRT = 2'b10,
            NONE = 2'b11
        } op_code_t;
        //////////////////////////////////////////////
        localparam WD = 4; 
        typedef enum logic [WD-1:0]{
            IDLE = 4'b0000,
            LOAD_X = 4'b1101,
            LOAD_Y = 4'b1110,
            N_10 = 4'b0001,
            N_9  = 4'b0010,     
            N_8  = 4'b0011,
            N_7  = 4'b0100,
            N_6  = 4'b0101,
            N_5  = 4'b0110,
            N_4  = 4'b0111,
            N_3  = 4'b1000,
            N_2  = 4'b1001,
            N_1  = 4'b1010,
            N_0  = 4'b1011,
            N_MINUS_1 = 4'b1100
        } ctrl_mult_state;
	/*Data for clock divider*/
	localparam 		DW = 26;
	typedef logic 	[DW-1:0] data_divider_t;
	    /*Data for 10 bit range value*/
	    localparam MIN_VAL = 10'b1000000000;
	    localparam MAX_VAL = 10'b0111111111;
        localparam MINUS_1 = -1;
    endpackage
`endif 