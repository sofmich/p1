// Coder:           Eduardo Castillo, Sofia Michel
// Date:            March 21st, 2021
// File:			fifo_top.sv
// Description:		This is the top module for a fifo register

import sdp_dc_ram_pkg::*;

	
module fifo_top (
input   clk_fpga,
input 	rst_n, // reset low active asynchronous
input 	logic push,
input 	logic pop,
input 	data_t DataInput,
output logic full,
output logic empty,
output 	data_t DataOutput
	);
	
	
register_t rptr_r2w_1, rptr_r2w_2, wq2_rptr;
addr_t ram_w_addr;
	
register_t wptr_w2r_1, wptr_w2r_2, rq2_wptr;
addr_t ram_r_addr;
	
sdp_dc_ram_if ram();



//Read to write sync 
ff_d_reg sync_r2w_1(
	.clk(clk_fpga),
	.rst(rst_n),
	.enb(TRUE),
	.inp(rptr_r2w_1),
	.out(rptr_r2w_2)
	);

ff_d_reg sync_r2w_2(
	.clk(clk_fpga),
	.rst(rst_n),
	.enb(TRUE),
	.inp(rptr_r2w_2),
	.out(wq2_rptr)
	);
fifo_control_write write_ctrl_full(
	.clk_fpga(clk_fpga),
	.rst_n(rst_n),
	.push(push),
	.rptr(wq2_rptr),
	.address(ram_w_addr),
	.full(full),
	.wptr(wptr_w2r_1)	
	);
//Write to read sync
ff_d_reg sync_w2r_1(
	.clk(clk_fpga),
	.rst(rst_n),
	.enb(TRUE),
	.inp(wptr_w2r_1),
	.out(wptr_w2r_2)
	);

ff_d_reg sync_w2r_2(
	.clk(clk_fpga),
	.rst(rst_n),
	.enb(TRUE),
	.inp(wptr_w2r_2),
	.out(rq2_wptr)
	);
fifo_control_read read_ctrl_empty(
	.clk_fpga(clk_fpga),
	.rst_n(rst_n),
	.i_pop(pop),
	.wptr(rq2_wptr),
	.address(ram_r_addr),
	.empty(empty),
	.rptr(rptr_r2w_1)
	);

sdp_dc_ram memory(
	.clk_a(clk_fpga),
	.clk_b(clk_fpga),
	.mem_if(ram.mem)
	);

//Ram data enable 
assign ram.we_a = ~full & push;
assign ram.rd_b = ~empty & pop;
assign ram.data_a = DataInput;
assign ram.wr_addr_a = ram_w_addr;
assign ram.rd_addr_b = ram_r_addr;
assign DataOutput = ram.rd_data_a;
	
endmodule

