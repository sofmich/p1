// Coder:           Sofia Michel, Ethan Castillo
// Date:            May 8th, 2021
// File:			enable_queues.sv
// Description:		This module is a queue enabler 

 
import uart_pkg::*;

	
module control_pop_input(
	input logic i_clk, /*Poping at 50Mhz*/
	input logic i_rst,
	input logic i_enable,
	input logic[MAX_M-1:0] i_rrb_processing,
	input logic[MAX_M-1:0] i_empty_queue_info,
	input data_t[MAX_M-1:0] i_in_info_fifo,
	input data_t i_enabled_M_queues,
	output logic [MAX_M-1:0] o_pop_info,
	output logic [MAX_M-1:0] o_pop_data,
	output logic[MAX_M-1:0] o_data_transfer_overflow,
	output logic[MAX_M-1:0] o_clean_register_dst,
	output logic[MAX_M-1:0] o_enable_len_reg
	
	
);
	
data_t aux_enable_queue;
logic [MAX_M-1:0] aux_pop_info;
logic [MAX_M-1:0] aux_empty_fifo;


logic contr_nx ;
logic countr;


logic [MAX_M-1:0] pop_info_arr;
logic [MAX_M-1:0] pop_data_arr;



genvar index_state;

generate
	for (index_state = 0; index_state<MAX_M; index_state++) begin:gen_sm
		in_poping_state_machine in_poping_sm(
			.i_clk(i_clk),
			.i_rst(i_rst),
			.i_active_CMD5(i_enable), // señal de inicio
			.i_len_to_count(i_in_info_fifo[index_state]),
			.i_fifo_empty(i_empty_queue_info[index_state]),
			.i_rrb_processing(i_rrb_processing[index_state]),
			.o_pop_info(pop_info_arr[index_state]),
			.o_pop_data(pop_data_arr[index_state]),
			.o_enable_length_register(o_enable_len_reg[index_state]),
			.o_data_transfer_overflow(o_data_transfer_overflow[index_state]),
			.o_clean_register_dst(o_clean_register_dst[index_state])
		);
	end
endgenerate


always_ff @( posedge i_clk or negedge i_rst ) begin 
    if(!i_rst)begin
        aux_pop_info <= '0;
	    countr <= 1'b1;
    end
    else begin
	    if (i_enable) begin //When registered command == cmd5
			aux_pop_info <= (~i_empty_queue_info & i_enabled_M_queues & pop_info_arr);
		    // i_empty_queue_info  = 1000 1001
		    // pop = 0111 0110   m = 0011 1111
		    // pop = pop & M =
		    // 1= EMPTY 0 = THERE IS DATA
		    countr <= contr_nx;
	    end
	    else begin
		   aux_pop_info <= '0;
		    countr <= 1'b1;
		end
	    
	end
        
end


always_comb begin: pop_dst_src
        contr_nx    <= countr + 1'b1;
end:pop_dst_src

assign o_pop_info = aux_pop_info;
assign o_pop_data = pop_data_arr;

endmodule

