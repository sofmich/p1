interface tb_p3_if(
    input i_clk,
    input i_clk2
);

import uart_pkg::*;

logic serial_data_rx;
logic serial_output;
logic big_fifo_refill_c_ovf;


modport tstr(
    input i_clk,
    input i_clk2,
    output serial_data_rx,
    input serial_output,
    input big_fifo_refill_c_ovf
); 

modport p3 (
    input i_clk,
    input i_clk2,
    input serial_data_rx,
    output serial_output,
    output big_fifo_refill_c_ovf
);
    
endinterface //tb_p3_if