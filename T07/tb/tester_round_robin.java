// Java program for implementation of RR scheduling
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.io.File;  // Import the File class
import java.io.FileWriter;   // Import the FileWriter class


public class tester_round_robin 
{

    public static void main(String[] args)
    {
       // System.out.println(Arrays.toString(args));
        ArrayList<Integer> grants = new ArrayList<>();
        int decimal=0; 
      
        String line=args[0].replaceAll("\\s", "");
        decimal = Integer.parseInt(line);
        //System.out.println(decimal);
        int position = 1;
        int result =0;
        while (decimal != 0) {
            if((decimal & 1)==1)
                grants.add(position);
            result += decimal & 1;
            position += 1;
            decimal = decimal >> 1;
        }   

        //System.out.println(Arrays.toString(grants.toArray()));
        int grant = 1;
        int poped_grant = 0;
        try {
            BufferedWriter myWriter = new BufferedWriter(new FileWriter("grants.txt",true));
            myWriter.write(Integer.toString(grants.size())+"\n");
            while(!grants.isEmpty())
            {
                grant = 1;
                poped_grant = grants.remove(0);
                for(int i=1; i<poped_grant;i++)
                {
                    grant = grant<<1;
                }
                myWriter.write(Integer.toString(grant)+"\n");
              //  System.out.println("grant");
              //  System.out.println(grant);
            }
            myWriter.close();
            //System.out.println("Successfully wrote to the file.");
          } catch (IOException e) {
           // System.out.println("An error occurred.");
            e.printStackTrace();
          }
        
    }
}