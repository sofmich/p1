interface tb_rrb_if(
    input i_clk
);
import tb_rrb_pkg::*;

vector req_vector;
vector granted;
logic valid;
logic ack;

modport tstr (
    input  i_clk,  			// 50MHz from fpga
    output  req_vector,
    output  ack,
    input valid,
    input granted
);

modport rrb (
    input  i_clk,  			// 50MHz from fpga
    input  req_vector,
    input  ack,
    output valid,
    output granted
);

endinterface