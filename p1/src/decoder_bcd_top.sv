// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            February 8th, 2021
// File:			decoder_bcd_top.sv
// Module name:		decoder_bcd_top
// Project Name:	p1
// Description:		Decoder instantiations  

import top_p1_pkg::*;


module decoder_bcd_top(
input  product_t number ,
output segments_t units,
output segments_t tens,
output segments_t cents,
output logic sign
);

bcd_t units_bcd;
bcd_t tens_bcd;
bcd_t cents_bcd;
	
decoder_bcd conversion_bcd(
	.number(number), 
	.units(units_bcd),
	.tens(tens_bcd),
	.cents(cents_bcd),
	.sign(sign));
decoder7_segments decoder_units(
	.number(units_bcd),
	.digit(units));
decoder7_segments decoder_tens(
	.number(tens_bcd),
	.digit(tens));
decoder7_segments decoder_cents(
	.number(cents_bcd),
	.digit(cents));

endmodule

