onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_rrb/clk
add wave -noupdate /tb_rrb/t.request_vector
add wave -noupdate /tb_rrb/uut/pp/req_vector
add wave -noupdate /tb_rrb/uut/pp/registered_request
add wave -noupdate /tb_rrb/uut/pp/i_ack
add wave -noupdate /tb_rrb/uut/pp/i_rst_n
add wave -noupdate /tb_rrb/uut/pp/o_grant
add wave -noupdate -color Gold /tb_rrb/uut/pp/o_valid
add wave -noupdate -color White /tb_rrb/t.granted_poped
add wave -noupdate -color White /tb_rrb/itf/granted
add wave -noupdate /tb_rrb/itf/valid
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {139384 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 309
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {206587 ps}
