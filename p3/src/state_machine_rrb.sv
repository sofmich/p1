
import uart_pkg::*;
module state_machine_rrb(
    input bit i_clk,  // 10kHz
    input logic i_reset_n,
    input logic i_ack,
    input logic i_no_req_vec,
    output logic o_valid,
    output logic o_processing
);

ctrl_state current_state;
ctrl_state next_state;
logic valid = '0;

always_comb begin : estados
    case (current_state)
        RRB_IDLE: begin
            if(i_no_req_vec)
                next_state = RRB_IDLE;
            else
                next_state = GRANT;
        end 
        GRANT:begin
                next_state = PROCESSING;
        end
        PROCESSING: begin
            if(i_ack)
                next_state = RRB_IDLE;
            else
                next_state = PROCESSING;
        end
        default: begin
            next_state = RRB_IDLE;
        end 
    endcase
end

always_comb begin : acciones
    valid = 1'b0;
    o_processing = 1'b0;
    case (current_state)
        RRB_IDLE: begin
            valid = 1'b0;
            o_processing = 1'b0;
        end 
        GRANT: begin
            valid = 1'b1;
            o_processing = 1'b0;
        end
        PROCESSING: begin
            valid = 1'b0;
            o_processing = 1'b1;
        end
        default: begin
            valid = 1'b0;
            o_processing = 1'b0;
        end
    endcase
end

always_ff @( posedge i_clk or negedge i_reset_n ) begin : cuenta
    if(!i_reset_n)begin
        current_state <= RRB_IDLE;
    end
    else
        current_state <= next_state;
end

assign o_valid = valid;

endmodule