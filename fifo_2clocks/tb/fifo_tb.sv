// Coder:           Eduardo Castillo, Sofia Michel
// Date:            March 21st, 2021
// File:			fifo_tb.sv

`timescale 1us / 1us

module fifo_tb();
import sdp_dc_ram_pkg::*;

localparam  ClkPeriod50MHz = 20;  
localparam  ClkPeriod10MHz = 100;  // 1/10 ns => ~ 10 MHz
localparam W_DATA = 8;
//Signal declaration
bit clk;
bit rst;
logic push;
logic pop;
logic [W_DATA-1:0]  DataInput;
logic full;
logic empty;
logic [W_DATA-1:0]  DataOutput;

fifo_top uut(
	.clk_fpga(clk),
	.clk_uart(clk),
	.rst_n(rst),
	.push(push),
	.pop(pop),
	.DataInput(DataInput),
	.full(full),
	.empty(empty),
	.DataOutput(DataOutput)
	
	);
 // Button Input Stimulus Procedure
 initial begin
	   clk		= '0;
	  #(ClkPeriod50MHz) rst = 0;
      #(ClkPeriod50MHz) rst = 1;
 end 
 initial begin
	 
 end 
always  begin 
	 #(ClkPeriod50MHz/2) clk = ~clk;
end

// Make pop and push untill there is no space
initial begin
	# 0  push = 1'b0;
	# 0 	pop = 1'b0;
	#(ClkPeriod50MHz*2)  push = 1'b1;
	#(ClkPeriod50MHz)  push = 1'b1;
	#(ClkPeriod50MHz*19)  push = 1'b0;

	
	# 0  pop = 1'b0;
	#(ClkPeriod50MHz)  pop = 1'b1;
	#(ClkPeriod50MHz*18)  pop = 1'b0;
	#(ClkPeriod50MHz*2) push = 1'b1;
	#(ClkPeriod50MHz)  DataInput = 5;
	#(ClkPeriod50MHz)  DataInput = 1;
	#(ClkPeriod50MHz)  DataInput = 5;
	#(ClkPeriod50MHz)  DataInput = 7;
	#(ClkPeriod50MHz)  DataInput = 5;
	#(ClkPeriod50MHz)  DataInput = 0;
	#(ClkPeriod50MHz)  DataInput = 1;
	#(ClkPeriod50MHz)  DataInput = 1;
	#(ClkPeriod50MHz)  DataInput = 2;
	#(ClkPeriod50MHz)  DataInput = 7;

end


initial begin
	# 0  DataInput = 1'b0;
	#(ClkPeriod50MHz*3)  DataInput = 8;
	#(ClkPeriod50MHz)  DataInput = 7;
	#(ClkPeriod50MHz)  DataInput = 6;
	#(ClkPeriod50MHz)  DataInput = 5;
	#(ClkPeriod50MHz)  DataInput = 0;
	#(ClkPeriod50MHz)  DataInput = 5;
	#(ClkPeriod50MHz)  DataInput = 7;
	#(ClkPeriod50MHz)  DataInput = 5;
	#(ClkPeriod50MHz)  DataInput = 2;
	#(ClkPeriod50MHz)  DataInput = 5;
	#(ClkPeriod50MHz)  DataInput = 0;
	#(ClkPeriod50MHz)  DataInput = 5;
	#(ClkPeriod50MHz)  DataInput = 2;
	#(ClkPeriod50MHz)  DataInput = 5;
	#(ClkPeriod50MHz)  DataInput = 1;
	#(ClkPeriod50MHz)  DataInput = 5;
	#(ClkPeriod50MHz)  DataInput = 7;
	#(ClkPeriod50MHz)  DataInput = 5;
	#(ClkPeriod50MHz)  DataInput = 0;
	#(ClkPeriod50MHz)  DataInput = 1;
	#(ClkPeriod50MHz)  DataInput = 1;
	#(ClkPeriod50MHz)  DataInput = 2;
	#(ClkPeriod50MHz)  DataInput = 7;
	
end



endmodule

	
	
