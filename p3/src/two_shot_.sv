// Coder:           Sofia Michel, Ethan Castillo
// Date:            May 8th, 2021
// File:            two_shot_.sv
// Description:     State machine for one shot


import uart_pkg::*;

/*Operates at FPGA frequency 50MHz This module receives the uart processing ready to 
 * activate a one shot the cycle after that*/

module two_shot_ (
		input bit i_clk,
		input logic i_rst,
		input logic i_uart_ready,
		output logic o_en_one_shot,
		output logic o_first_shot,
		output logic o_second_shot
	
	);
	one_shot_e current_state = WAIT_SHOT;
	one_shot_e next_state = WAIT_SHOT;
	always_comb begin: uart_input_states
		case(current_state)
			WAIT_SHOT:begin
				if(i_uart_ready == TRUE)
					next_state = DO_FIRST_SHOT; //Go to make the one shot 
				else
					next_state = WAIT_SHOT; //go to wait for a new shot
			end
			DO_FIRST_SHOT:begin
				next_state = WAIT_1;
			end
			WAIT_1:begin
				next_state = DO_SECOND_SHOT;
			end
			DO_SECOND_SHOT:begin
				next_state = WAIT_2;
			end	
			WAIT_2:begin
				next_state = WAIT_SHOT;
			end
			default: begin 
				next_state = WAIT_SHOT;
			end
			
		endcase
	end


always_ff @( posedge i_clk or negedge i_rst ) begin 
    if(!i_rst)begin
        current_state <= WAIT_SHOT;
    end
    else
        current_state <= next_state;
end


always_comb begin
	case(current_state)
		WAIT_SHOT: begin
			o_en_one_shot = FALSE;	
			o_first_shot = FALSE;
			o_second_shot = FALSE;
		end
		DO_FIRST_SHOT: begin
			o_en_one_shot = TRUE;	
			o_first_shot = TRUE;
			o_second_shot = FALSE;
		end
		WAIT_1: begin
			o_en_one_shot = FALSE;	
			o_first_shot = FALSE;
			o_second_shot = FALSE;
		end
		DO_SECOND_SHOT: begin
			o_en_one_shot = TRUE;	
			o_first_shot = FALSE;
			o_second_shot = TRUE;
		end
		WAIT_2: begin
			o_en_one_shot = FALSE;	
			o_first_shot = FALSE;
			o_second_shot = FALSE;
		end
		default: begin
			o_en_one_shot = FALSE;
			o_first_shot = FALSE;
			o_second_shot = FALSE;
		end
	endcase
end
endmodule
