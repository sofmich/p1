// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            March 8th, 2021
// File:			
// Module name:		
// Project Name:	
// Description:		

import top_p2_pkg::*;

module divider(
    input bit i_clk,
    input logic     i_rst_n,
    input datos_t i_signed_M,
    input datos_t i_signed_Q,
    input bit i_en_abs_Q, //Q phase selector
    input bit i_enable_QR, //Enable the flip flop load
    output datos_t o_result,
    output bit div_error
);

//Abs values to make algorithm work
datos_t abs_M;
datos_t abs_Q;

//Save signs to fix result
logic sign_M;
logic sign_Q;
//Fixed sign
logic sign_result;
//A2 of Q to fix result sign 	
datos_t final_a2_q;
//Final result value
datos_t result_value;	

// Substract A: A-M 
datos_t substracted_A;
// Shifted left A with LSB Q[N]
datos_t shifted_left_A;
//Shifted left Q with LSB subs_A[0]
datos_t shifted_left_Q;
// Restore A: A+M
datos_t restored_A;


//Mux to select which Q will be used
datos_t Q_to_register;
//Registered flip flop vector
full_reg_t pre_register_vector;
full_reg_t registered_vector;
datos_t registered_Q;
datos_t registered_A;
datos_t mux_restored_A;
datos_t mux_processing_A;

always_comb begin : signo_final
    sign_result = sign_M^sign_Q;  //Obtain final sign 	
    //Make concatenation for full register
    registered_Q = registered_vector[N2-1:N];
	registered_A = registered_vector[N-1:0];
    pre_register_vector = {1'b0, Q_to_register,mux_processing_A}; //high 0,Q[],A[] TODO: add mux restored A or not
end
//Check if divisor is 0, then there is a error
divider_error div_error_chek(
	.i_signed_M(i_signed_M),
	.o_div_error(div_error)
	);
//Get abs values of M and Q
A2_decomplement a2_decomplement_M(
    .i_signed_data(i_signed_M),
    .o_abs_data(abs_M),
    .o_sign(sign_M)
);

A2_decomplement a2_decomplement_Q(
    .i_signed_data(i_signed_Q),
    .o_abs_data(abs_Q),
    .o_sign(sign_Q)
);
shift_left_LSB shift_A(
	.i_data_in(registered_A), // Registered A
	.i_new_lsb(registered_Q[N-1]), //Q[N]
	.o_shifted_data(shifted_left_A)
	);
//Make A = A - M
add_subs subs_A_M(
	.i_a_value(shifted_left_A), //Shifted A
	.i_m_value(abs_M),
	.i_selector(SUBSTRACTION),
	.o_selection(substracted_A)
	);
//Make A = A + M
add_subs add_A_M(
	.i_a_value(substracted_A), //Shifted A
	.i_m_value(abs_M),
	.i_selector(ADDITION),
	.o_selection(restored_A)
	);
// Shift Q and set LSB from A-M
shift_left_LSB shift_Q (
	.i_data_in(registered_Q), //Registered  Q
	.i_new_lsb(~substracted_A[N-1]), // Q[0] = !A[0]
	.o_shifted_data(shifted_left_Q)
	);

//Mux 2:1 N bits
multiplexor select_Q(
	.i_first(shifted_left_Q),
    .i_second(abs_Q),
    .i_selector(i_en_abs_Q), //sel == 1 then i_second
    .o_selection(Q_to_register)
	);
//Mux A restored or not
multiplexor select_A(
	.i_first(substracted_A),
    .i_second(restored_A),
    .i_selector(substracted_A[N-1]), //sel == 1 then save restored A, not then A-M
    .o_selection(mux_restored_A)
	);
//If there will be the abs A saved or the new As values while processing
multiplexor selec_A_processing_or_abs(
	.i_first(mux_restored_A),
    .i_second('0),
    .i_selector(i_en_abs_Q), //sel == 1 then save restored A, not then A-M
    .o_selection(mux_processing_A)
	);

//Register full vector each clock edge
flipflopD_2N_1 reg_q(
    .clk(i_clk), 
    .rst_n(i_rst_n),
    .en(i_enable_QR),
    .d(pre_register_vector),
    .q(registered_vector)
);

A2_complement final_a2_complement(
    .i_abs_data(registered_vector[N2-1:N]),
    .o_signed_data(final_a2_q)
);

multiplexor result_mux(
    .i_first(registered_vector[N2-1:N]), //Unsigned Q original value
    .i_second(final_a2_q), //A2 Q value
    .i_selector(sign_result),
    .o_selection(result_value)
);

assign o_result = result_value;

endmodule