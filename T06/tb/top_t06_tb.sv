// Coder:         Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:          March 8th, 2021
// File:			    top_t6_tb.sv
// Module name:		top_t6_tb
// Project Name:	t06 uart
// Description:		El archivo tb para la tarea 6
`timescale 1ns / 1ns

module top_t06_tb();
import uart_pkg::*;

localparam  ClkPeriod50MHz = 20;  // 1/20 ns => ~ 50 MHz


//Signal declaration
bit clk;
bit rst_n;
logic serial_data_rx;
logic clear_interrupt;
logic transmit;
uart_data_t data_to_transmit;
//Instance top module
uart_top top_module(
    .i_clk(clk),  // 50MHz
    .i_rst_n(rst_n),
    .i_serial_data_rx(serial_data_rx),
    .i_data_to_transmit(data_to_transmit),
    .i_transmit(transmit),
    .clear_interrupt(clear_interrupt)
);

 // Button Input Stimulus Procedure
   initial begin
       clear_interrupt='0;
	   clk		= '0;
	  @(posedge clk);
      rst_n = 0;
      @(posedge clk);
      rst_n = 1;
	  data_to_transmit = 8'b01011011;
	  transmit = 1;
	  @(posedge clk); transmit = 0;
	  @(posedge clk);data_to_transmit = '0;
	   
	  /*
      #(8680)serial_data_rx = 1'b1;
      #(8680)serial_data_rx = 1'b1;
      #(8680)serial_data_rx = 1'b1;
      #(8680)serial_data_rx = 1'b1;

      #(8680)serial_data_rx = 1'b0;
      
      #(8680)serial_data_rx = 1'b1;
      #(8680)serial_data_rx = 1'b0;
      #(8680)serial_data_rx = 1'b1;
      #(8680)serial_data_rx = 1'b1;
      #(8680)serial_data_rx = 1'b1;
      #(8680)serial_data_rx = 1'b0;
      #(8680)serial_data_rx = 1'b1;
      #(8680)serial_data_rx = 1'b1;
      
      #(8680)serial_data_rx = 1'b1; //parity
      
      #(8680)serial_data_rx = 1'b1;
      #(8680)
      clear_interrupt = 1'b1;
      #(8680)
      clear_interrupt='0;

      ///////////////////////////////////////
      #(8680)serial_data_rx = 1'b1;
      #(8680)serial_data_rx = 1'b1;
      #(8680)serial_data_rx = 1'b1;

      #(8680)serial_data_rx = 1'b0;
      
      #(8680)serial_data_rx = 1'b1;
      #(8680)serial_data_rx = 1'b1;
      #(8680)serial_data_rx = 1'b0;
      #(8680)serial_data_rx = 1'b1;
      #(8680)serial_data_rx = 1'b1;
      #(8680)serial_data_rx = 1'b0;
      #(8680)serial_data_rx = 1'b1;
      #(8680)serial_data_rx = 1'b1;
      
      #(8680)serial_data_rx = 1'b0; //parity
      
      #(8680)serial_data_rx = 1'b1;
      #(8680)
      clear_interrupt = 1'b1;
      #(8680)
      clear_interrupt = 1'b0;
      */
   end


always begin
    #(ClkPeriod50MHz/2) clk = ~clk;
end

endmodule