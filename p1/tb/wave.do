onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_seq_mult/clk
add wave -noupdate /tb_seq_mult/start
add wave -noupdate /tb_seq_mult/ready
add wave -noupdate /tb_seq_mult/rst
add wave -noupdate /tb_seq_mult/top_module/control_m/machine_state/current_state
add wave -noupdate -radix decimal /tb_seq_mult/top_module/i_multd
add wave -noupdate -radix decimal /tb_seq_mult/top_module/i_multr
add wave -noupdate -radix decimal -childformat {{{/tb_seq_mult/product[7]} -radix decimal} {{/tb_seq_mult/product[6]} -radix decimal} {{/tb_seq_mult/product[5]} -radix decimal} {{/tb_seq_mult/product[4]} -radix decimal} {{/tb_seq_mult/product[3]} -radix decimal} {{/tb_seq_mult/product[2]} -radix decimal} {{/tb_seq_mult/product[1]} -radix decimal} {{/tb_seq_mult/product[0]} -radix decimal}} -subitemconfig {{/tb_seq_mult/product[7]} {-height 15 -radix decimal} {/tb_seq_mult/product[6]} {-height 15 -radix decimal} {/tb_seq_mult/product[5]} {-height 15 -radix decimal} {/tb_seq_mult/product[4]} {-height 15 -radix decimal} {/tb_seq_mult/product[3]} {-height 15 -radix decimal} {/tb_seq_mult/product[2]} {-height 15 -radix decimal} {/tb_seq_mult/product[1]} {-height 15 -radix decimal} {/tb_seq_mult/product[0]} {-height 15 -radix decimal}} /tb_seq_mult/product
add wave -noupdate /tb_seq_mult/top_module/data_path/add_product
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {10600 ps} 0} {{Cursor 2} {6378826 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 342
configure wave -valuecolwidth 204
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {85701 ps}
