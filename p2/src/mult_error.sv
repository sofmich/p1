// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            March 8th, 2021
// File:			
// Module name:		
// Project Name:	
// Description:		

import top_p2_pkg::*;

module mult_error(
    input reg_2N_t i_mult_value,
    output bit o_mult_error 
);

assign o_mult_error = (i_mult_value < MIN_VAL | i_mult_value > MAX_VAL)? 1'b1 : 1'b0;
 
endmodule