// Coder:         Sofi Michel Salazar Valdovinos
// Description:   This code is a flip-flop D

import sdp_dc_ram_pkg::addr_t;

module ff_d_address (
input bit 	clk,
input logic rst,
input logic enb,
input addr_t inp,
output addr_t out
);

addr_t  ff_d_r;

always_ff@(posedge clk or negedge rst) begin
    if(!rst)
        ff_d_r  <= '0;
    else if (enb)
        ff_d_r  <= inp;
end

assign out  = ff_d_r;

endmodule
