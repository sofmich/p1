// Coder: Ethan Castillo, Sofia Salazar
// Date: 2021

class tester_p3;

virtual tb_p3_if itf;

`protect
localparam UART_FREQ_DELAY = 52083; // 1/Rb -> 52083 ns
function new(virtual tb_p3_if.tstr t);
    itf =t;
endfunction

//***init task***////
task automatic init();
    itf.serial_data_rx = 1'b1;
    //itf.serial_output = 1'b0;
endtask

int index;

logic[7:0] start_FE = 'hFE;
int transmission_temp;
logic parity;

task automatic inject_FE();
    parity = ((start_FE[7] ^ start_FE[6]) ^ (start_FE[5] ^ start_FE[4])) ^ ((start_FE[3] ^ start_FE[2]) ^ (start_FE[1] ^ start_FE[0]));
    transmission_temp = start_FE;
    for(index = 0; index<8;index++)begin
        #(UART_FREQ_DELAY)itf.serial_data_rx = transmission_temp & 'h01;
        transmission_temp = transmission_temp >> 1;
    end
    #(UART_FREQ_DELAY)itf.serial_data_rx = parity; //parity
endtask

task automatic inject_data(input logic[7:0] data);
   // $display("data: %d",data);
    parity = ((data[7] ^ data[6]) ^ (data[5] ^ data[4])) ^ ((data[3] ^ data[2]) ^ (data[1] ^ data[0]));
    transmission_temp = data;
    for(index = 0; index<8;index++)begin
        #(UART_FREQ_DELAY)itf.serial_data_rx = transmission_temp & 'h01;
        transmission_temp = transmission_temp >> 1;
    end
    #(UART_FREQ_DELAY)itf.serial_data_rx = parity; //parity
endtask

task automatic start_uart_frame();
    for(index = 0; index<2;index++)begin
        #(UART_FREQ_DELAY)itf.serial_data_rx = 1'b1;
    end
    #(UART_FREQ_DELAY)itf.serial_data_rx = 1'b0;
endtask

task automatic end_uart_frame();
    #(UART_FREQ_DELAY)itf.serial_data_rx = 1'b1;
endtask

task automatic set_ports(input logic[7:0] m);

    start_uart_frame();
    //1111 1110
    inject_data('hFE);
    end_uart_frame();

    start_uart_frame();
    //lENGTH
    inject_data('h03);
    end_uart_frame();

    start_uart_frame();
    //CMD 1
    inject_data('h01);
    end_uart_frame();

    start_uart_frame();
    //M VALUE
    inject_data(m);
    end_uart_frame();

    start_uart_frame();
    //EF
    inject_data('hEF);
    end_uart_frame();

endtask

task automatic retransmitir();
     start_uart_frame();
    //1111 1110
    inject_data('hFE);
    end_uart_frame();

    start_uart_frame();
    //lENGTH
    inject_data('h02);
    end_uart_frame();

    start_uart_frame();
    //CMD 2
    inject_data('h02);
    end_uart_frame();

    start_uart_frame();
    //EF
    inject_data('hEF);
    end_uart_frame();

        @(posedge itf.big_fifo_refill_c_ovf);
   // $display("aqui termina");
endtask

task automatic iniciar_capturas();
     start_uart_frame();
    //1111 1110
    inject_data('hFE);
    end_uart_frame();

    start_uart_frame();
    //lENGTH
    inject_data('h02);
    end_uart_frame();

    start_uart_frame();
    //CMD 3
    inject_data('h03);
    end_uart_frame();

    start_uart_frame();
    //EF
    inject_data('hEF);
    end_uart_frame();
endtask

task automatic iniciar_procesamiento();
     start_uart_frame();
    //1111 1110
    inject_data('hFE);
    end_uart_frame();

    start_uart_frame();
    //lENGTH
    inject_data('h02);
    end_uart_frame();

    start_uart_frame();
    //CMD 5
    inject_data('h05);
    end_uart_frame();

    start_uart_frame();
    //EF
    inject_data('hEF);
    end_uart_frame();

    //retransmitir();
    @(posedge itf.big_fifo_refill_c_ovf);
endtask

logic [7:0] payload_temp;
int index_payload;
task automatic cargar_datos(input logic[7:0] length, input logic[15:0][7:0] payload, input logic[7:0] src, input logic[7:0] dst);
    start_uart_frame();
    //1111 1110
    inject_data('hFE);
    end_uart_frame();

    start_uart_frame();
    //lENGTH
    inject_data(length);
    end_uart_frame();

    start_uart_frame();
    //CMD 4
    inject_data('h04);
    end_uart_frame();

    start_uart_frame();
    //SRC
    inject_data(src);
    end_uart_frame();

    start_uart_frame();
    //DST
    inject_data(dst);
    end_uart_frame();

    for(index_payload = 0; index_payload<length-4; index_payload++)begin
        start_uart_frame();
        //PAYLOAD
       // $display("payload%d: %d",index_payload,payload[index_payload]);
        inject_data(payload[index_payload]);
       end_uart_frame();
       
    end

    start_uart_frame();
    //EF
    inject_data('hEF);
    end_uart_frame();
endtask

logic[7:0] recovered_data;
logic received_serial;
logic has_data = 1'b1;
int data_wait = 0;
logic[7:0]  q_a[$];

task automatic detect_start();
    data_wait = 0;
    has_data = 1'b1;
    #(UART_FREQ_DELAY);
    received_serial = itf.serial_output;
    while(received_serial == 1'b1)begin
        #(UART_FREQ_DELAY);
        received_serial = itf.serial_output;
        data_wait++;
        /*if(data_wait>30)begin
            has_data = 1'b0;
            break;
        end*/
    end
    if(has_data)begin
        get_data_from_uart();
    end
endtask

task automatic print_all();
 for(int j=0; j<256;j++)begin
        #(UART_FREQ_DELAY);
        received_serial = itf.serial_output;
        q_a.push_front(received_serial);
    end
    display_data();
endtask

task automatic get_data_from_uart();
    for(int j=0; j<128;j++)begin
        #(UART_FREQ_DELAY);
        received_serial = itf.serial_output;
        q_a.push_front(received_serial);
    end
    display_data();
endtask

logic[7:0] recovered;
task automatic display_data();
    while(q_a.size() > 0)begin
        recovered   = q_a.pop_front();
        $display("%d",recovered);
    end
endtask
/*
int rx_state;
logic i_reception;
task automatic record_data_sm();
    rx_state = 0;
    while(1)begin
        i_reception = itf.serial_output;
        case(rx_state)
            0:begin    //IDLE
                if(i_reception = '0)begin
                    rx_state = 1'b1;
                end
            end
            1:begin     //START

            end
            2:begin     //START 2
            end
            3:begin
            end

            default:begin
            end
        endcase
    end

endtask
*/
`endprotect
endclass