//Date: 12/02/2021
`timescale 1us / 1us

module top_p2_tb();
import top_p2_pkg::*;

localparam  ClkPeriod10kHz = 100;  // 1/100 us => ~ 10 kHz


//Signal declaration
bit clk;
bit rst_n;
logic start;
logic load;
op_code_t  operation;
operand_t operand;
datos_t result;
logic rdy;
logic error;
logic sign;


//Instance top module
top_p2_MDR top_module(
    .i_clk(clk),  // 50MHz
    .i_reset_n(rst_n),
    .i_start(start),
    .i_operation(operation),
    .i_load(load),
    .i_operand(operand),
    .o_result(result),
    .o_ready(rdy),
    .o_error(error),
    .o_sign(sign)
);

 // Button Input Stimulus Procedure
   initial begin
	   clk		= '0;
	   start = '0;
	   operation = NONE;
	   load = '0;
	  @(posedge clk);
      rst_n = 0;
      @(posedge clk);
      rst_n = 1;
	  #(ClkPeriod10kHz) 
      
	  
	 
	   
	  operation = MULTIPLICATION;
      @(posedge clk);
      start <= 1;
      @(posedge clk);
      start <= 0;
	  operand = 1;   /*Load x = Q  = Dividend*/
	  @(posedge clk);
      load <= 1;
      @(posedge clk);
      load <= 0;
	  operand = -512; /*Load Y = M = Divisor*/
	  @(posedge clk);
      load <= 1;
      @(posedge clk);
      load <= 0;
	  
	   @(posedge rdy);

     operation = MULTIPLICATION;
      @(posedge clk);
      start <= 1;
      @(posedge clk);
      start <= 0;
    operand = 2;   /*Load x = Q  = Dividend*/
    @(posedge clk);
      load <= 1;
      @(posedge clk);
      load <= 0;
    operand = -512; /*Load Y = M = Divisor*/
    @(posedge clk);
      load <= 1;
      @(posedge clk);
      load <= 0;
    
     @(posedge rdy);
	   
	 


   end


always begin
    #(ClkPeriod10kHz/2) clk = ~clk;
end

endmodule