module rrb_wrapper

import rrb_pkg::*;
//import tb_rrb_pkg::*;
(
    input clk,
    input rst,
    tb_rrb_if.rrb itf
);

vector granted;

top_round_robin pp(
    .i_clk(itf.i_clk),  			// 50MHz from fpga
    .i_rst_n(rst),	
    .req_vector(itf.req_vector),
    .i_ack(itf.ack),
    .o_valid(itf.valid),
    .o_grant(granted)
);

assign itf.granted = vector'(granted);

endmodule