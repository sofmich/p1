// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            February 8th, 2021
// File:			piso_lsb.sv
// Module name:		piso_lsb
// Project Name:	sequential_multiplier
// Description:		this is a piso register module



import top_p1_pkg::*;

module piso_lsb (
	input               clk,    // Clock
	input               rst,    // asynchronous reset low active 
	input               enb,    // Enable
	input               l_s,    // load or shift
	input  data_t	    inp,    // data input
	output              out     // Serial output
	);

data_t rgstr_r     ;
/*logic aux;

assign aux = l_s||enb;

always_ff@(posedge clk or negedge rst) begin: rgstr_label
    if(!rst)
        rgstr_r     <= '0           ;
    else if (aux) 
		    rgstr_r  <= inp;	
        	rgstr_r     <= rgstr_r  >> 1'b1  ;
end:rgstr_label
*/

always_ff@(posedge clk or negedge rst) begin: rgstr_label
    if(!rst)
        rgstr_r     <= '0           ;
    else if (l_s) 
		    rgstr_r  <= inp;	
    else if (enb)
        	rgstr_r     <= rgstr_r  >> 1'b1  ;
end:rgstr_label

assign out  = rgstr_r[0];    // lsB bit is the first to leave

endmodule
