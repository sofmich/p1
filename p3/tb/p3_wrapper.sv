module p3_wrapper

import uart_pkg::*;
(
    input clk,
    input clk2,
    input rst,    
    tb_p3_if.p3 itf
);

logic serial_output;

P3_top pp(
    .i_clk(itf.i_clk), 
    .i_clk2(itf.i_clk2),
    .i_rst_n(rst),
    .i_serial_data_rx(itf.serial_data_rx),
    .o_serial_output(serial_output),
    .o_big_fifo_refill_c_ovf(itf.big_fifo_refill_c_ovf)
);

assign itf.serial_output = serial_output;

endmodule