// Coder:           Sofia Michel, Ethan Castillo
// Date:            May 8th, 2021
// File:			enable_queues.sv
// Description:		This module is a queue enabler 

 
import uart_pkg::*;

	
module enable_queue(

	input data_t i_registered_cmd,
	input data_t i_registered_src,
	input data_t i_registered_M,
	input data_t i_enabled_M_queues,
	input logic i_shot_uart,
	input logic i_en_one_shot,
	input logic en_length,
	input logic en_dst,
	input logic en_src,
	input logic en_rx_data,



	input logic [MAX_M-1:0] i_pop_info_enable,
	input logic [MAX_M-1:0] i_pop_data_enable,
	output data_t o_enabled_queue,
	output logic o_push_info_queue, //input queues only
	output logic o_push_data_queue, //input queues only
	output logic[MAX_M-1:0] o_pop_info_queue,
	output logic[MAX_M-1:0] o_pop_data_queue	
);
	
data_t aux_enable_queue;
logic [MAX_M-1:0] aux_pop_info;
logic [MAX_M-1:0] aux_empty_fifo;
m_queues_e next_queue;
m_queues_e aux_next_queue;

logic contr_nx ;
logic countr;


logic [MAX_M-1:0] pop_info_arr;
logic [MAX_M-1:0] pop_data_arr;

always_comb begin : push_pop
	case(i_registered_cmd)
	CMD1: begin
		o_push_info_queue = FALSE; 
		o_push_data_queue = FALSE; 
		aux_enable_queue = '0;
		o_enabled_queue =  '0;
		o_pop_info_queue = '0; 
		o_pop_data_queue = '0;
	end
	CMD2: begin
		o_push_info_queue = FALSE; 
		o_push_data_queue = FALSE; 
		aux_enable_queue = '0;
		o_enabled_queue =  '0;
		o_pop_info_queue = '0; 
		o_pop_data_queue = '0;
	end
	CMD3: begin
		o_push_info_queue = FALSE; 
		o_push_data_queue = FALSE; 
		aux_enable_queue = '0;
		o_enabled_queue =  '0;
		o_pop_info_queue = '0; 
		o_pop_data_queue = '0;
	end
	CMD4: begin
		o_push_info_queue = (i_en_one_shot) ? TRUE : FALSE; 
		o_push_data_queue = (i_shot_uart & en_rx_data) ? TRUE : FALSE; 
		o_pop_info_queue = '0; 
		o_pop_data_queue = '0;
		aux_enable_queue = (i_registered_src > i_registered_M)? i_registered_src%i_registered_M : i_registered_src;
		o_enabled_queue =  data_t'('h01 << (aux_enable_queue - 1'b1));
	end
	CMD5: begin
		o_push_info_queue = FALSE; 
		o_push_data_queue = FALSE; 
		o_pop_info_queue = i_pop_info_enable;
		aux_enable_queue = data_t'('0);
		o_enabled_queue =  data_t'('hFF) & i_enabled_M_queues;
		o_pop_data_queue = i_pop_data_enable;
		
	end
	default: begin
		o_push_info_queue = FALSE; 
		o_push_data_queue = FALSE;
		aux_enable_queue = data_t'('0);
		o_enabled_queue =  data_t'('h0);
		o_pop_info_queue = '0; 
		o_pop_data_queue = '0;
		end
	endcase
end




	

endmodule

