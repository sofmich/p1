// Coder: Ethan Castillo, Sofia Salazar
// Date: 2021

class tester_rrb;

localparam N_REQ = 4;

localparam TRUE   = 1'b1;
localparam FALSE  = 1'b0;

`protect

typedef logic [N_REQ-1:0] vector_t;

localparam LIMINF = 0;
localparam LIMSUP = (2**(N_REQ))-1;
localparam PERIOD = 20; // Minimum value 4

localparam INC       = 1;
`endprotect

virtual tb_rrb_if itf;
vector_t request_vector,granted_poped, injected_vector;
vector_t  q_grant[$]; //cola con los grants

vector_t current_vector;

integer operation_time;
integer text_id         ;
integer i         ;
integer  q_n_grants[$];

logic valid;
logic ack;
string call;

`protect
function new(virtual tb_rrb_if.tstr t);
    itf =t;
endfunction

//*** ***//
task automatic init();
    itf.req_vector  = FALSE;
    itf.ack       = TRUE;
    itf.valid     = FALSE;
    itf.granted     = FALSE;
endtask

//*** Inject vector request ***//
task automatic inject_vector_requests();
    for (request_vector = LIMINF; request_vector <= LIMSUP; request_vector = request_vector + INC )begin
        $sformat(call, "java tester_round_robin %d",request_vector);
        $system(call);
        #((request_vector+1)*PERIOD);
    end
endtask

//*** Calculate result based on vector request ***//
task automatic rrb_calc();
    $sformat(call, "java tester_round_robin");
    $system(call);
endtask

integer id_descriptor;
int granted_text;
int index;

task automatic read_file();
    id_descriptor = $fopen("r.txt", "r");
endtask



//*** Open file ***//
int grant_read;
int num_grants;
int index_grants;
task automatic add_grants();
    text_id = $fopen("grants.txt", "r");

  while(!$feof(text_id)) begin

      // leer el numero de grants
      $fscanf(text_id, "%d \n", num_grants);
      q_n_grants.push_front(num_grants);
    $display ("Num grants %d",num_grants);
        for(index_grants = 0; index_grants < num_grants;index_grants++ )begin
            $fscanf(text_id, "%d \n", grant_read);
            q_grant.push_front(grant_read);
            $display ("Grant %d vector %d ",index_grants,grant_read);
        end
	end
		
  $fclose(text_id);
   // removing the results
   $system("del grants.txt"); 
endtask
integer current_count=1;

task automatic inject_rtl();
    @(posedge itf.i_clk);
    for (injected_vector = LIMINF; injected_vector <= LIMSUP; injected_vector = injected_vector + INC )begin
        

        itf.req_vector = injected_vector;
                #((current_count+1)*PERIOD);
                current_count = q_n_grants.pop_back();
      
    end
endtask

vector_t current_ack_vector = LIMINF;
integer index_giver;

task automatic ack_giver();
    while(1)begin
        #(PERIOD)itf.ack = FALSE;
        #(PERIOD)itf.ack = TRUE;
    end
endtask

task automatic compare_values();
    while(1)begin
        @(posedge itf.valid)
        granted_poped = q_grant.pop_back();
        if(granted_poped != itf.granted)begin
            $display("no charcha\t%d %d",granted_poped,itf.granted);
        end
        else begin
            $display("charcha\t%d %d",granted_poped,itf.granted);
        end
    end
endtask

task automatic close_file();
    $fclose(text_id);
	
endtask

`endprotect
endclass