// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            March 8th, 2021
// File:			
// Module name:		
// Project Name:	
// Description:		

import top_p2_pkg::*;

module multiplexor3(
    input datos_t i_q_abs,//opcion 0
   // input datos_t i_a_value,//opcion 1
    input full_reg_t i_full_vector, //opcion 2
    input q_state_selector i_selector,
    //input logic i_q0_value,
    //input datos_t i_q_value,
    output full_reg_t o_selection
);

full_reg_t temp_output;
full_reg_t aux;

always_comb begin : multiplexor
	aux = '0;
	temp_output = '0;
    case (i_selector)
        FIRST: begin
            aux = i_q_abs;
            temp_output = aux << 1;
        end
        /*SECOND: begin
            temp_output = {i_a_value,i_q_value,i_q0_value};
        end*/
        SECOND: begin
            temp_output = i_full_vector;
        end
        default: begin
            temp_output = '0;
	        aux = '0;
        end
    endcase
end

assign o_selection = temp_output;

endmodule