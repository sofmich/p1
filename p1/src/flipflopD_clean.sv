// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            February 8th, 2021
// File:			flipflopD_clean.sv
// Module name:		flipflopD_clean
// Project Name:	sequential_multiplier
// Description:		this is a flipflopD register module which is used to clean a 
//					register value when its flag is set to make it
import top_p1_pkg::*;

module flipflopD_clean(
input bit       clk,
input bit       rst_n,
input bit		en,
input bit 		clean,
input bit		adding,
input product_t    d,
output product_t   q

	);
product_t q_aux;
	
always_ff@(posedge clk, negedge rst_n) begin
	if(!rst_n)
		q_aux <= '0;
	else if(clean)
		q_aux <= {N{1'b0}}; //If there is a clean signal, clean register
	else if(en) /*This is PIPO register (array of D flip-flop)*/
	begin
		if(adding)
			q_aux <= d;
	end 
end

assign q = q_aux;

endmodule

