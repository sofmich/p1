onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /top_t06_tb/top_module/reception/serial_data_rx
add wave -noupdate /top_t06_tb/top_module/reception/sipo_reg/i_clk
add wave -noupdate /top_t06_tb/top_module/reception/sipo_reg/i_rst_n
add wave -noupdate -color Gold /top_t06_tb/top_module/reception/sipo_reg/i_serial_data
add wave -noupdate /top_t06_tb/top_module/reception/sipo_reg/capture
add wave -noupdate /top_t06_tb/top_module/reception/sipo_reg/o_paralel_data
add wave -noupdate /top_t06_tb/top_module/reception/sipo_reg/registered_data
add wave -noupdate /top_t06_tb/top_module/reception/sipo_reg/shifted_data
add wave -noupdate /top_t06_tb/top_module/reception/sipo_reg/data
add wave -noupdate /top_t06_tb/top_module/reception/delay_cntr/o_ovf
add wave -noupdate -radix decimal /top_t06_tb/top_module/reception/delay_cntr/i_max_cnt
add wave -noupdate /top_t06_tb/top_module/reception/rx_states/current_state
add wave -noupdate /top_t06_tb/top_module/reception/sipo_reg/capture
add wave -noupdate /top_t06_tb/top_module/reception/rx_states/o_en_sipo
add wave -noupdate /top_t06_tb/top_module/reception/data_cntr/i_stimulus
add wave -noupdate /top_t06_tb/top_module/reception/data_cntr/o_ovf
add wave -noupdate /top_t06_tb/top_module/reception/data_cntr/i_enb
add wave -noupdate /top_t06_tb/top_module/reception/data_cntr/i_max_cnt
add wave -noupdate -radix decimal /top_t06_tb/top_module/reception/data_cntr/o_count
add wave -noupdate /top_t06_tb/top_module/reception/data_cntr/i_max_cnt
add wave -noupdate /top_t06_tb/top_module/reception/parity/o_parity_check
add wave -noupdate /top_t06_tb/top_module/reception/received_parity
add wave -noupdate -color Red /top_t06_tb/top_module/reception/o_parity_error
add wave -noupdate /top_t06_tb/top_module/reception/rx_states/o_en_parity
add wave -noupdate /top_t06_tb/top_module/reception/ff/en
add wave -noupdate /top_t06_tb/top_module/reception/ovf_start
add wave -noupdate /top_t06_tb/top_module/reception/start_cntr/i_enb
add wave -noupdate /top_t06_tb/top_module/reception/parity/o_parity_check
add wave -noupdate /top_t06_tb/top_module/reception/ovf_cycle
add wave -noupdate /top_t06_tb/top_module/reception/received_parity
add wave -noupdate /top_t06_tb/top_module/reception/received_data
add wave -noupdate /top_t06_tb/top_module/reception/rx_interrupt
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 2} {111608 ns} 0} {{Cursor 4} {194568 ns} 0} {{Cursor 5} {11092 ns} 0}
quietly wave cursor active 3
configure wave -namecolwidth 409
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {307047 ns} {404893 ns}
