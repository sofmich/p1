// Coder:           Sofia Michel, Ethan Castillo
// Date:            April 7th, 2021
// File:			rx_state_machine.sv
// Description:		This module is the reception interface for a uart driver

 
import uart_pkg::*;
module multiplexor#(
parameter MUX_DW = DELAY_DW
)(
    input logic[MUX_DW-1:0] i_first,//opcion 0
    input logic[MUX_DW-1:0]i_second,//opcion 1
    input logic i_selector,
    output logic[MUX_DW-1:0] o_selection
);

logic[MUX_DW-1:0] temp_output;

always_comb begin : multiplexor
    if(i_selector)begin
        temp_output = i_second;
    end else begin
        temp_output = i_first;
    end
    
end

assign o_selection = temp_output;

endmodule