// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            February 8th, 2021
// File:			control.sv
// Module name:		control
// Project Name:	p1
// Description:		control path to check on state machine 

import top_p1_pkg::*;
module control(
    input bit i_clk,  // 10kHz
    input logic i_reset_n,
    input logic i_start,
    output logic o_ready,
    output logic o_enable,
    output logic o_load_flag,    
    output logic o_cleaning_flag,
    output logic o_display_flag    
);

logic ready;
logic enable;

logic count_overflow;
logic cleaning_flag;
logic load_flag;
logic display_flag;


counter counter(
    .i_clk(i_clk),
    .i_rst_n(i_reset_n),
    .i_en(enable),
    .o_overflow(count_overflow)
);

state_machine machine_state(
    .i_clk(i_clk),
    .i_reset_n(i_reset_n),
    .i_start(i_start),
    .i_count_overflow(count_overflow),
    .o_ready(ready),
    .o_enable(enable),
    .o_load_flag(load_flag),
    .o_cleaning_flag(cleaning_flag),
    .o_display_flag(display_flag)
);

assign o_load_flag = load_flag;
assign o_cleaning_flag = cleaning_flag;
assign o_display_flag = display_flag;
assign o_enable = enable;
assign o_ready = ready;

endmodule