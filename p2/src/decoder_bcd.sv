// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            February 8th, 2021
// File:			decoder_bcd.sv
// Module name:		decoder_bcd
// Project Name:	p1
// Description:		Decoder from number with maxium 3 digits to units, tens and cents 

import top_p2_pkg::*;


module decoder_bcd(
input  datos_t i_number ,
output bcd_t o_units,
output bcd_t o_tens,
output bcd_t o_cents,
output logic o_sign
);
	
datos_t tens_aux;
datos_t cents_aux;
datos_t number_aux;

	
always_comb begin
	if(i_number[N-1] == 1)  /*Negative number so sign is on 0*/
		begin
			o_sign = 1'b1; //Active on 0 to display
			number_aux = (~i_number) + 1'b1; 
		end
	else
		begin
			o_sign = 1'b0;
			number_aux = i_number;
		end
	/* Get Units*/
	
	tens_aux = (number_aux%100) - (number_aux%10);
	o_units = bcd_t'(number_aux%10);
	cents_aux =  number_aux - tens_aux - o_units;
	case(cents_aux)
		100: o_cents = bcd_t'(4'd1);
		200: o_cents = bcd_t'(4'd2);
		300: o_cents = bcd_t'(4'd3);
		400: o_cents = bcd_t'(4'd4);
		500: o_cents = bcd_t'(4'd5);
		default:
		o_cents = bcd_t'('0); 
	endcase
	case(tens_aux)
		10: o_tens = bcd_t'(4'd1);
		20: o_tens = bcd_t'(4'd2);
		30: o_tens = bcd_t'(4'd3);
		40: o_tens = bcd_t'(4'd4);
		50: o_tens = bcd_t'(4'd5);
		60: o_tens = bcd_t'(4'd6);
		70: o_tens = bcd_t'(4'd7);
		80: o_tens = bcd_t'(4'd8);
		90: o_tens = bcd_t'(4'd9);
		default:
		o_tens = bcd_t'(4'd0); 
	endcase
	end
endmodule