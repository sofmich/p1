//Coder: Ethan Castillo, Sofia Salazar
// file: binary decoding
import uart_pkg::*;

module granter(
    input vector i_ppc_output,
 //   output vector o_mask,
    output vector o_masked_grant_vector 
);

vector mask;

always_comb begin : decoding
    mask = i_ppc_output<<1;
    o_masked_grant_vector = mask^i_ppc_output;
end

endmodule