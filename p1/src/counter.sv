// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            February 8th, 2021
// File:			counter.sv
// Module name:		counter
// Project Name:	p1
// Description:		Counter to check overflow cycles on main state machine 

import top_p1_pkg::*;

module counter(
input bit       i_clk,
input bit       i_rst_n,
input logic     i_en,
output logic    o_overflow
);

data_t count_aux;
data_t count_nx;
logic o_overflow_w;

//Code the next value with always_comb, so counter increments
always_comb begin
    count_nx = count_aux + 1'd1;
    if(count_aux >= N-1)
        o_overflow_w = 1'b1;
    else
        o_overflow_w = '0;
end

//This is pipo register
always_ff@(posedge i_clk, negedge i_rst_n) begin
    if(!i_rst_n)
        count_aux <=  '0;
    else if(i_en)
	    if(o_overflow_w)
		    count_aux <=  '0;
	    else
        	count_aux <=  count_nx;
end

assign o_overflow = o_overflow_w;

endmodule