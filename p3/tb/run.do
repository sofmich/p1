if [file exists work] {vdel -all}
vlib work
vlog +define+SIMULATION -f files.f
onbreak {resume}
set NoQuitOnFinish 1
vsim -voptargs=+acc work.top_t06_tb
do wave.do
run 70 ms
