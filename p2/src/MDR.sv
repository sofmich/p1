import top_p2_pkg::*;

module MDR(
    //multiplication signals
    input bit i_clk,
    input logic  i_rst_n,
    input q_state_selector i_Q_selector,
    input logic i_enable_QR,
  
    //sqrt signals
    input reg_2N_t i_signed_D,
    input logic i_enable_Q,
    input logic i_enable_R,
    input logic i_R_flag, 
    input logic i_Q_flag,
    input logic i_final_flag,
    /* Divider signals*/
    input logic i_en_abs_Q,
    input op_code_t  i_OPcode_selector,
    
    output reg_2N_t o_D_shifted_2_left,
    output datos_t o_result,
    output datos_t o_remainder,
    output datos_t o_parcial_result,
    
    output datos_t o_abs_Q,
    output datos_t o_abs_M,
    output datos_t o_sign_Q,
    output datos_t o_sign_M,
    output reg_2N_t o_product_vector
);


//definicion de los valores absolutos
datos_t abs_X;
datos_t abs_Y;
//definicion de los signos
logic sign_X;
logic sign_Y;

logic signo;
logic error;

full_reg_t full_vector_mult;
full_reg_t full_vector_pre_reg;
full_reg_t registered_vector;
full_reg_t shifted_vector_mult;

//comon signals
datos_t subs_element_1;
datos_t subs_element_2;
datos_t adder_element_1;
datos_t adder_element_2;

datos_t substract_result;
datos_t adder_result;

datos_t result_value;
datos_t remainder_value;

//multiplier signals
datos_t add_subs_result;
full_reg_t concatenacion_mult;
datos_t q0q1_selected_data;


//mult / div signals
datos_t final_a2_q;
datos_t result_value_mult_div;

datos_t final_a2_r;
datos_t remainder_value_mult_div;

datos_t remainder_value_mult_div_zero;

//sqrt signals

reg_2N_t full_D_abs;
reg_2N_t D_shifted_2_left;
datos_t reg_R_output;
datos_t reg_Q_output;

datos_t mux_R_selection;
datos_t mux_Q_selection;

datos_t mux_pre_adder_R_selection;
datos_t mux_pre_adder_R_entry;

datos_t mux_pre_adder_Q_selection;
datos_t mux_pre_adder_Q_entry_0;
datos_t mux_pre_adder_Q_entry_1;

datos_t mux_btw_1_and_3_selection;
datos_t mux_btw_0_and_1_selection;

datos_t mux_entry_Q_1;

datos_t add_sub_result;

datos_t last_mux_selection;

logic mux_pre_adder_selector;

full_reg_t concatenation_R_Q_0;

datos_t post_add_subs_sqrt_mux_value;

// cada valor entra a su respectivo modulo de complemento A2
// de cada bloque de complemento A2 sale el respectivo signo y la señal de absoluto
A2_decomplement a2_decomplement_X(
    .i_signed_data(i_signed_D[N-1:0]),
    .o_abs_data(abs_X),
    .o_sign(sign_X)
);
// TODO: adjust width 
A2_decomplement a2_decomplement_Y(
    .i_signed_data(i_signed_D[N2-1:N]),
    .o_abs_data(abs_Y),
    .o_sign(sign_Y)
	);
	
assign o_abs_Q = abs_X;
assign o_abs_M = abs_Y;
assign o_sign_Q = sign_X;
assign o_sign_M = sign_Y;

always_comb begin : blockName
    //calculos para la raiz cuadrada
    D_shifted_2_left = i_signed_D<<2;

    mux_pre_adder_R_entry = ((registered_vector[N2:N+1])<<2)|(abs_Y[1:0]);
    mux_pre_adder_Q_entry_0 = mux_btw_1_and_3_selection|((registered_vector[N:1])<<2);
    mux_pre_adder_Q_entry_1 = ((registered_vector[N:1])<<1)|ONE;
    mux_entry_Q_1 = mux_btw_0_and_1_selection|((registered_vector[N:1])<<1);
    mux_pre_adder_selector = i_final_flag&(registered_vector[N2]);

    concatenation_R_Q_0 = {mux_R_selection,mux_Q_selection,1'b0};
    //calculos para la multiplicacion
    signo = (i_OPcode_selector != SQRT )? sign_X^sign_Y: 0;

    substract_result = subs_element_1 - subs_element_2;
    adder_result = adder_element_1 + adder_element_2;

    concatenacion_mult = {add_subs_result,registered_vector[N:0]};

end
/* todo: Divider architecture Incorporated*/
datos_t Q_to_register;
datos_t shifted_left_Q;
datos_t shifted_left_A;
datos_t registered_A;
datos_t registered_Q;
datos_t substracted_A;
datos_t restored_A;
datos_t mux_restored_A;
datos_t abs_Q;
datos_t mux_processing_A;
full_reg_t concatenation_divider;
assign abs_Q = abs_X;
assign concatenation_divider = {mux_processing_A,Q_to_register,1'b0};
//TODO: fix origian [N-1:0], [N2-1:N]
assign registered_Q = registered_vector[N:1];
assign registered_A = registered_vector[N2-1:N+1];
assign restored_A = adder_result;
assign substracted_A = substract_result;
shift_left_LSB shift_A_divider(
	.i_data_in(registered_A), // Registered A
	.i_new_lsb(registered_Q[N-1]), //Q[N]
	.o_shifted_data(shifted_left_A)
	);
shift_left_LSB shift_Q_divider (
	.i_data_in(registered_Q), //Registered  Q
	.i_new_lsb(~substracted_A[N-1]), // Q[0] = !A[0]
	.o_shifted_data(shifted_left_Q)
	);

multiplexor select_Q_divider(
	.i_first(shifted_left_Q),
    .i_second(abs_Q),
    .i_selector(i_en_abs_Q), //sel == 1 then i_second
    .o_selection(Q_to_register)
	);
//Mux A restored or not
multiplexor select_A_divider(
	.i_first(substracted_A),
    .i_second(restored_A),
    .i_selector(substracted_A[N-1]), //sel == 1 then save restored A, not then A-M
    .o_selection(mux_restored_A)
	);
//If there will be the abs A saved or the new As values while processing
multiplexor selec_A_processing_or_abs(
	.i_first(mux_restored_A),
    .i_second('0),
    .i_selector(i_en_abs_Q), //sel == 1 then save restored A, not then A-M
    .o_selection(mux_processing_A)
	);
/********* End of divider incorporation **********/

multiplexor3 mult_multiplexor(
    .i_q_abs(abs_Y),
    .i_full_vector(shifted_vector_mult),
    .i_selector(i_Q_selector),
    .o_selection(full_vector_mult)
);

multiplexor mux_entry_R(
    .i_first('0),//opcion 0
    .i_second(post_add_subs_sqrt_mux_value),//opcion 1 la salida del componente de suma y resta
    .i_selector(i_R_flag),
    .o_selection(mux_R_selection)
);

multiplexor mux_entry_Q(
    .i_first('0),//opcion 0
    .i_second(mux_entry_Q_1 ),//opcion 1
    .i_selector(i_Q_flag),
    .o_selection(mux_Q_selection)
);


multiplexor_OP_2N_1 pre_reg_full_vector_mux(
    .i_vector_mult(full_vector_mult),//opcion 0
    .i_vector_div(concatenation_divider),//opcion 1
    .i_vector_sqrt(concatenation_R_Q_0), //opcion 2
    .i_op_code_selector(i_OPcode_selector),
    .o_selection(full_vector_pre_reg)
);



flipflopD_2N_1 reg_q(
    .clk(i_clk), 
    .rst_n(i_rst_n),
    .en(i_enable_QR),
    .d(full_vector_pre_reg),
    .q(registered_vector)
);
/** elementos unicos de la raiz cuadrada entre el registro y la suma y resta*/

multiplexor mux_pre_adder_R(
    .i_first(mux_pre_adder_R_entry),//opcion 0
    .i_second(registered_vector[N2:N+1]),//opcion 1
    .i_selector(mux_pre_adder_selector),
    .o_selection(mux_pre_adder_R_selection)
);

multiplexor mux_pre_adder_Q(
    .i_first(mux_pre_adder_Q_entry_0),//opcion 0
    .i_second(mux_pre_adder_Q_entry_1),//opcion 1
    .i_selector(mux_pre_adder_selector),
    .o_selection(mux_pre_adder_Q_selection)
);

multiplexor mux_btw_1_and_3(
    .i_first(ONE),//opcion 0
    .i_second(THREE),//opcion 1
    .i_selector(registered_vector[N2]),
    .o_selection(mux_btw_1_and_3_selection)
);

multiplexor mux_btw_0_and_1(
    .i_first(ONE),//opcion 0
    .i_second(ZERO),//opcion 1
    .i_selector(post_add_subs_sqrt_mux_value[N-1]),//add_sub_result[N-1]),// el bit mas significativo del resultado de la suma
    .o_selection(mux_btw_0_and_1_selection)
);
/////////////////////////////////////////////////////////////////////////////

/** elementos unicos de la multiplicacion entre el registro y la suma y resta*/
multiplexor2 q1q0_number(
    .i_m_abs(abs_X),
    .i_selector(mult_mux_selector'(registered_vector[1:0])),
    .o_selection(q0q1_selected_data)
);
/*******************************************************************//////////

multiplexor_OP_N pre_subs_element_1_mux(
    .i_vector_mult(registered_vector[N2:N+1]),//opcion 0
    .i_vector_div(shifted_left_A),//opcion 1
    .i_vector_sqrt(mux_pre_adder_R_selection), //opcion 2
    .i_op_code_selector(i_OPcode_selector),
    .o_selection(subs_element_1)
);

multiplexor_OP_N pre_subs_element_2_mux(
    .i_vector_mult(q0q1_selected_data),//opcion 0
    .i_vector_div(abs_Y),//opcion 1
    .i_vector_sqrt(mux_pre_adder_Q_selection), //opcion 2
    .i_op_code_selector(i_OPcode_selector),
    .o_selection(subs_element_2)
	);

multiplexor_OP_N pre_adder_element_1_mux(
    .i_vector_mult(q0q1_selected_data),//opcion 0
    .i_vector_div(substracted_A),//opcion 1
    .i_vector_sqrt(mux_pre_adder_R_selection), //opcion 2
    .i_op_code_selector(i_OPcode_selector),
    .o_selection(adder_element_1)
);
multiplexor_OP_N pre_adder_element_2_mux(
    .i_vector_mult(registered_vector[N2:N+1]),//opcion 0
    .i_vector_div(abs_Y),//opcion 1
    .i_vector_sqrt(mux_pre_adder_Q_selection), //opcion 2
    .i_op_code_selector(i_OPcode_selector),
    .o_selection(adder_element_2)
);

add_subs_q0q1_mux post_add_subs_q0q1_mux(
    .i_adder_result(adder_result),//opcion 0
    .i_subs_result(substract_result),//opcion 1
    .i_selector(mult_mux_selector'(registered_vector[1:0])),
    .i_a_value(registered_vector[N2:N+1]),
    .o_selection(add_subs_result)
);

multiplexor post_add_subs_sqrt_mux(
    .i_first(substract_result),
    .i_second(adder_result),
    .i_selector(registered_vector[N2]),
    .o_selection(post_add_subs_sqrt_mux_value)
);

multiplexor last_mux_sqrt(
    .i_first(registered_vector[N2:N+1]),//opcion 0
    .i_second(post_add_subs_sqrt_mux_value),//opcion 1
    .i_selector(registered_vector[N2]),
    .o_selection(last_mux_selection)
);

ASR_1 shift_r_right(
    .i_full_vector(concatenacion_mult),
    .o_shifted_vector(shifted_vector_mult)
);
//TODO: verify at this point it gets the registered Q
A2_complement final_a2_complement(
    .i_abs_data(registered_Q),
    .o_signed_data(final_a2_q)
);

multiplexor result_mux(
    .i_first(registered_Q),
    .i_second(final_a2_q),
    .i_selector(signo),
    .o_selection(result_value_mult_div)
);

A2_complement final_a2_complement_remainder(
    .i_abs_data(registered_vector[N2:N+1]),
    .o_signed_data(final_a2_r)
);

logic enable_rmainder_division_a2;
assign enable_rmainder_division_a2 = (i_OPcode_selector == DIVISION) & ((sign_X & sign_Y) | (sign_X & sign_Y == 0))? 1:0; 

multiplexor remainder_mux(
    .i_first(registered_vector[N2:N+1]),
    .i_second(final_a2_r),
    .i_selector(enable_rmainder_division_a2),
    .o_selection(remainder_value_mult_div)
);

assign o_product_vector = {registered_vector[N2:N+1],registered_Q}; 
assign o_result = result_value_mult_div;
assign o_remainder = remainder_value_mult_div;//registered_vector[N2:N+1];

assign o_D_shifted_2_left = D_shifted_2_left;

assign o_parcial_result = registered_Q;

endmodule