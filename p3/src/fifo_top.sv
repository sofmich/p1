// Coder:           Eduardo Castillo, Sofia Michel
// Date:            March 21st, 2021
// File:			fifo_top.sv
// Description:		This is the top module for a fifo register

import uart_pkg::*;

	
module fifo_top #(
	parameter W_ADDR = 4,
	parameter W_DATA = 8
	)(
	input   clk_fpga,
	input 	clk_uart,
	input 	rst_n, // reset low active asynchronous
	input 	logic push,
	input 	logic pop,
	input 	logic [W_DATA-1:0]  DataInput,
	//output logic full,
	output logic empty,
	output 	logic [W_DATA-1:0]  DataOutput
	);
	
	
logic [W_ADDR:0] rptr_r2w_1, rptr_r2w_2, wq2_rptr;
logic [W_ADDR-1:0]  ram_w_addr;
logic full;
logic [W_ADDR:0] wptr_w2r_1, wptr_w2r_2, rq2_wptr;
logic [W_ADDR-1:0]  ram_r_addr;
	
sdp_dc_ram_if #(.W_ADDR(W_ADDR)) ram();


//Read to write sync (push at fpga clk frequency)
ff_d_reg #(.W_ADDR(W_ADDR), .W_DATA(W_DATA)) sync_r2w_1(
	.clk(clk_fpga),
	.rst(rst_n),
	.enb(TRUE),
	.inp(rptr_r2w_1),
	.out(rptr_r2w_2)
	);

ff_d_reg  #(.W_ADDR(W_ADDR), .W_DATA(W_DATA))  sync_r2w_2(
	.clk(clk_fpga),
	.rst(rst_n),
	.enb(TRUE),
	.inp(rptr_r2w_2),
	.out(wq2_rptr)
	);
fifo_control_write #(.W_ADDR(W_ADDR), .W_DATA(W_DATA))  write_ctrl_full(
	.clk_fpga(clk_fpga),
	.rst_n(rst_n),
	.push(push),
	.rptr(wq2_rptr),
	.address(ram_w_addr),
	.full(full),
	.wptr(wptr_w2r_1)	
	);
//Write to read sync (pop at UART frequency)
ff_d_reg  #(.W_ADDR(W_ADDR), .W_DATA(W_DATA)) sync_w2r_1(
	.clk(clk_uart),
	.rst(rst_n),
	.enb(TRUE),
	.inp(wptr_w2r_1),
	.out(wptr_w2r_2)
	);

ff_d_reg  #(.W_ADDR(W_ADDR), .W_DATA(W_DATA))  sync_w2r_2(
	.clk(clk_uart),
	.rst(rst_n),
	.enb(TRUE),
	.inp(wptr_w2r_2),
	.out(rq2_wptr)
	);
fifo_control_read  #(.W_ADDR(W_ADDR), .W_DATA(W_DATA))  read_ctrl_empty(
	.clk_fpga(clk_uart),
	.rst_n(rst_n),
	.i_pop(pop),
	.wptr(rq2_wptr),
	.address(ram_r_addr),
	.empty(empty),
	.rptr(rptr_r2w_1)
	);

sdp_dc_ram  #(.W_ADDR(W_ADDR), .W_DATA(W_DATA))  memory(
	.clk_a(clk_fpga),
	.clk_b(clk_uart),
	.mem_if(ram.mem)
	);

//Ram data enable 
assign ram.we_a = ~full & push;
assign ram.rd_b = ~empty & pop;
assign ram.data_a = DataInput;
assign ram.wr_addr_a = ram_w_addr;
assign ram.rd_addr_b = ram_r_addr;
assign DataOutput = ram.rd_data_a;
	
endmodule

