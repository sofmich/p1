// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            March 8th, 2021
// File:			
// Module name:		
// Project Name:	
// Description:		
import top_p2_pkg::*;

module flipflopD_2N(
input bit       clk,
input logic     rst_n,
input logic		en,
input reg_2N_t    d,
output reg_2N_t   q

	);
reg_2N_t q_aux;
	
always_ff@(posedge clk, negedge rst_n) begin
	if(!rst_n)
		q_aux <= '0;
	else if(en) /*This is PIPO register (array of D flip-flop)*/
		q_aux <= d;
end

assign q = q_aux;

endmodule

