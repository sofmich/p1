// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            February 8th, 2021
// File:			decoder7_segments.sv
// Module name:		decoder_7segments
// Project Name:	p1
// Description:		This is a decoder from a number from 0-9 to 7 segments 

import top_p2_pkg::*;

module decoder7_segments(
input bcd_t 	number,
output segments_t   digit
);
	
always_comb begin
//Most significant bit starts with A, LSB is F segment
	case(number)
		0: digit = segments_t'(7'b0000001);
		1: digit = segments_t'(7'b1001111);
		2: digit = segments_t'(7'b0010010);
		3: digit = segments_t'(7'b0000110);
		4: digit = segments_t'(7'b1001100);
		5: digit = segments_t'(7'b0100100);
		6: digit = segments_t'(7'b0100000);
		7: digit = segments_t'(7'b0001111);
		8: digit = segments_t'(7'b0000000);
		9: digit = segments_t'(7'b0001100);
		default: 
		   digit = segments_t'(7'b1111111);
	endcase	
end

endmodule