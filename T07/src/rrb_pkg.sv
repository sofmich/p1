//Coder: Ethan Castillo, Sofia Salazar
//Date: 18/04/2021

`ifndef RRB_PKG_SV
    `define RRB_PKG_SV
    package rrb_pkg;

    localparam N_REQ = 4;
    localparam  W_N_REQ = $ceil($clog2(N_REQ));
    typedef logic [W_N_REQ-1:0] dcd_grant_t;
    localparam LEVELS = $clog2(N_REQ)+1;
    typedef logic [N_REQ-1:0] vector;
    typedef logic [N_REQ-1:0][LEVELS] matrix_t;
    
    typedef enum logic{
        COMPUTE = 1'b1,
        GRANT = 1'b0
    } ctrl_state;

    endpackage
`endif