// Coder:           Sofia Michel, Eduardo Castillo
// Date:            April 7th, 2021
// File:			uart_pkg.sv


`ifndef UART_PKG_SV
	`define UART_PKG_SV
	package uart_pkg;
	localparam 		DW = 8;
	localparam 		UART_SM_DW = 3;
	localparam		MAX_M = 8;
	localparam 		STATE_W = 3;
	typedef logic 	[DW-1:0] data_t;
	localparam UART_DW = DW;
	typedef logic [UART_DW-1:0] uart_data_t;
	typedef logic[10:0] mux_delay_t;
	//********PARAMETROS PARA ES ROUND ROBIN*******////
	localparam N_REQ = MAX_M;
    localparam  W_N_REQ = $clog2(N_REQ);//$ceil($clog2(N_REQ));
    typedef logic [W_N_REQ-1:0] dcd_grant_t;
    localparam LEVELS = $clog2(N_REQ)+1;
    typedef logic [N_REQ-1:0] vector;
    typedef logic [N_REQ-1:0][LEVELS:0] matrix_t;
	 typedef enum logic[1:0]{
        RRB_IDLE = 2'b00,
		GRANT = 2'b01,
        PROCESSING = 2'b10,
		BLANK = 2'b11
    } ctrl_state;
	////////*********************//////////////
	typedef enum logic[MAX_M-1:0] { 
		FIRST 	= 8'b00000001,
		SECOND 	= 8'b00000010, 
		THIRD	= 8'b00000100,
		FOURTH	= 8'b00001000,
		FIFTH	= 8'b00010000,
		SIXTH	= 8'b00100000,
		SEVENTH	= 8'b01000000,
		EIGHTH	= 8'b10000000
	} single_ack_position;
	/////////******************////////////////

	//****************/////
	// TENER UN PARAMETRO LOCAL QUE INDIQUE LA CANTIDAD...
	//... DE BITS A UTILIZAR EN LOS CONTADORES PARA LOS DELAYS

	localparam RB = 19_200;
	localparam TP = 1/RB;
	localparam F = 10_000_000;
	//localparam TS = 1/F;
	//localparam DELAY_MAXCNT   	 = TS/TP; ///esto se tiene que calcular en base al bit rate.... 1/Rb * frecuencia
	localparam DELAY_MAXCNT = F/RB+1;
	localparam HALF_DELAY_MAXCNT = DELAY_MAXCNT/2;
	localparam DELAY_DW       	= $clog2(DELAY_MAXCNT);
	typedef logic [DELAY_DW:0] cnt_t;
	typedef logic          ovf_t;

	typedef struct {
	cnt_t       count;
	ovf_t       ovf;
	} cntr_t;
	typedef enum logic [2:0]{ 
	 IDLE = 3'b000,
	 START = 3'b001,
	 START2 = 3'b110,
	 DATA = 3'b010,
	 PARITY = 3'b011,
	 STOP  = 3'b100,
	 WAITING = 3'b101
	} sm_uart_e;
		
	typedef enum logic{ 
	 FALSE = 1'b0,
	 TRUE = 1'b1
	} logic_e;
		/*Data to CMD state machine*/
	 typedef enum logic [2:0]{ 
	 IDLE_FRAME = 3'b000,
	 LENGTH = 3'b001,
	 CMD = 3'b010,
	 /*For CMD 1*/
	 SET_M = 3'b011,
	 /*For CMD 4*/
	 SRC = 3'b100,
	 DST = 3'b101,
	 RECEIVING_LOAD = 3'b110,
	 END = 3'b111
	 } sm_cmd_e;

	typedef enum logic [7:0]{ 
	 CMD1 = 8'b00000001,
	 CMD2 = 8'b00000010,
	 CMD3 = 8'b00000011,
	 CMD4 = 8'b00000100,
	 CMD5 = 8'b00000101
	} cmd_flag_e;


	//Data frame values
	localparam FRAMESTART = 'hFE;
	localparam FRAMEEND = 'hEF;
	localparam FULL = 'hFF;
	localparam EMPTY = 'h00;
	
	/*RAM PKG*/
    localparam W_ADDR_RAM_IN = 4; //2^4 = 16
	localparam W_ADDR_RAM_OUT = 5; //2^7 = 128
	
	//**Big fifo state machine*//
	typedef enum logic [1:0]{
		BIG_FIFO_IDLE = 2'b00,
		PASS_FROM_O_FIFOS = 2'b01,
		REFILL_BIG_FIFO = 2'b10,
		BLANK_STATE = 2'b11
	} big_fifo_e;
		
	/*One shot sm*/
	typedef enum  logic [2:0]{ 
	 WAIT_SHOT = 3'b000,
	 DO_FIRST_SHOT = 3'b001,
	 WAIT_1 = 3'b010,
	 DO_SECOND_SHOT = 3'b011,
	 WAIT_2 = 3'b100
	} one_shot_e;

	/* Registered M queues*/
	typedef enum logic [3:0]{ 
	 ZERO = 4'b0000,
	 ONE = 4'b0001,
	 TWO = 4'b0010,
	 THREE = 4'b0011,
	 FOUR = 4'b0100,
	 FIVE = 4'b0101,
	 SIX = 4'b0110,
	 SEVEN = 4'b0111,
	 EIGHT = 4'b1000
	 } m_queues_e;

	 typedef enum logic[1:0] {
		IDLE_IN_POP = 2'b00,
		POP_DST= 2'b01,
		POP_LEN= 2'b10,
		WAIT_LEN= 2'b11
	 }in_pop_e;
endpackage
`endif