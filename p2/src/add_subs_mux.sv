// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            March 8th, 2021
// File:			
// Module name:		
// Project Name:	
// Description:		

import top_p2_pkg::*;

module add_subs_mux(
    input datos_t i_first_value,//opcion 0
    input datos_t i_second_value,//opcion 1
    input logic i_selector,
    output datos_t o_selection
);

full_reg_t temp_output;

always_comb begin : operator
    if(i_selector)begin
        temp_output = i_first_value + i_second_value;
    end else begin
        temp_output = i_first_value - i_second_value;
    end
end

assign o_selection = temp_output;

endmodule