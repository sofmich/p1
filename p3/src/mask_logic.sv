//Coder: Sofia Salazar, Ethan Castillo
//filename: arbitro

import uart_pkg::*;


module mask_logic(
    input vector i_grant,
    output vector o_mask
);

vector ppc_output;
vector vector_mask;

always_comb begin : mask
    vector_mask = i_grant << 1;
end

ppc_rrb module_ppc(
    .i_req_vector(vector_mask),
    .o_ppc_output(ppc_output)
);



assign o_mask = ppc_output;

endmodule