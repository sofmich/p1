// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            February 15th, 2021
// File:			top_p2_MDR.sv
// Module name:		top_p2
// Project Name:	p2
// Description:		this is the top entity for a sequential multiplier 

import top_p2_pkg::*; 

module top_p2_MDR(
    input bit i_clk,  			// 50MHz from fpga
    input logic i_reset_n,		/* Reset active on high*/
    input operand_t i_operand,  /* Data to operate with*/
    input logic i_start,		/* Start button*/
  ///  input op_code_t i_operation,/* Operation code*/
    input logic i_load,			/* Load action*/
    output bit o_error,			/* Bit error when overflow*/
    output logic o_sign,		/* Sign to operate*/
    output segments_t o_units,
    output segments_t o_tens,
    output segments_t o_cents,
    output datos_t o_result,		/* Result on N bits*/
    output datos_t o_reminder,
    output datos_t o_parcial_result,
    output logic o_ready,		/* Ready flag*/
    output logic o_load_x,
    output logic o_load_y
);

bit clk_10M;
bit clk_5M;
logic start_debounced;
logic op_code_LSB;
logic op_code_MSB;
logic load_debounced; 
logic enable_data_path;
logic load_flag;
logic cleaning_flag;
logic display_flag;

op_code_t i_operation;
assign i_operation = op_code_t'(i_operand[1:0]);

`ifndef SIMULATION
    pll_50M_10k pll(
        .areset(i_reset_n),
        .inclk0(i_clk),
        .c0(clk_10M)  // 10 kHz
    );

    clock_divider_top #(.REFERENCE_CLOCK(10_000_000),
        .FREQUENCY(5_000_000)
    )
    clk_divider(
        .clk_fpga(clk_10M),
        .rst_n(i_reset_n),
        .clk(clk_5M)

    );

    dbcr_top debouncer_start(
    .clk(clk_5M),
    .rst_n(i_reset_n),
    .Din(~i_start),
    .one_shot(start_debounced)
	    );
	
	dbcr_top debouncer_op_code_LSB(
    .clk(clk_5M),
    .rst_n(i_reset_n),
    .Din(~i_operation[0]),
    .one_shot(op_code_LSB)
		    );
	 dbcr_top debouncer_op_code_MSB(
    .clk(clk_5M),
    .rst_n(i_reset_n),
    .Din(~i_operation[1]),
    .one_shot(op_code_MSB)
		    );
	dbcr_top debouncer_load(
    .clk(clk_5M),
    .rst_n(i_reset_n),
    .Din(~i_load),
    .one_shot(load_debounced)
    );

`else
    assign start_debounced = i_start;
	assign op_code_LSB = i_operation[0];
	assign op_code_MSB = i_operation[1];
	assign load_debounced = i_load;
    assign clk_5M = i_clk;

`endif

op_code_t debounced_operation;
//assign debounced_operation = op_code_t'({op_code_MSB , op_code_LSB});
datos_t mult_result;
assign debounced_operation = op_code_t'({op_code_MSB , op_code_LSB});

logic ready;
logic error;
logic enable_M;
logic enable_Q;
logic enable_QR;
logic enable_final_reg;
q_state_selector Q_selector;

//sqrt control selectors

logic enable_D;
logic enable_R;
logic enable_G;
logic shift_D_flag;
logic R_flag;
logic Q_flag;
logic final_flag;

/*Enable flags for loading x and y*/
logic en_load_X;
logic en_load_Y;
	
//op code flag load state
logic enable_load_opcode;
op_code_t registered_opcode;
logic en_abs_Q;
logic en_post_error;
logic en_pre_error;
//Check error TODO:
logic pre_error;
logic post_error;
datos_t parcial_result;

datos_t data_res;
datos_t data_rmndr;

reg_2N_t product_vector;
state_machine estados(
    .i_clk(clk_5M),
    .i_reset_n(i_reset_n),
    .i_start(start_debounced),
    .i_op_code(registered_opcode),
    .i_load(load_debounced),
    .i_pre_error(pre_error),
    .o_ready(ready),
    .o_enable_M(enable_M),
    .o_enable_Q(enable_Q),
    .o_enable_QR(enable_QR),
    .o_Q_selector(Q_selector),
    .o_enable_final_r(enable_final_reg),
    .o_enable_D(enable_D),
    .o_enable_R(enable_R),
    .o_enable_G(enable_G),
    .o_shift_D_flag(shift_D_flag),
    .o_R_flag(R_flag),
    .o_Q_flag(Q_flag),
    .o_final_flag(final_flag),
    .o_en_load_opcode(enable_load_opcode),
    .o_en_abs_Q(en_abs_Q),
    .o_en_load_X(en_load_X),
    .o_en_load_Y(en_load_Y),
    .o_en_post_error(en_post_error),
    .o_en_pre_error(en_pre_error)
	);

/*Operation Code register*/ 
flipflopD_2bits  register_Op_Code(
    .clk(clk_5M),
    .rst_n(i_reset_n),
    .en(enable_load_opcode),
    .d(debounced_operation),
    .q(registered_opcode)
);


reg_2N_t shift_mux_selection;
reg_2N_t registered_D;
datos_t result;
datos_t remainder;
reg_2N_t shifted_D_2_left;
datos_t registered_result;
datos_t registered_remainder;
	
datos_t abs_Q;
datos_t abs_M;
logic sign_Q;
logic sign_M;


logic multiplexor_D_selector;

mux_D_selector mux_selector_D(
    .i_operation(registered_opcode),
    .i_shift_D_flag(shift_D_flag),
    .o_flag(multiplexor_D_selector)
);


reg_2N_t data_x_or_y;

assign data_x_or_y = (load_debounced & en_load_X)? {'0, i_operand}:{i_operand,registered_D[N-1:0]};

multiplexor2N_reg_D multiplexor2N_reg_DD(
    .i_first(data_x_or_y),
    .i_second(shifted_D_2_left),
    .i_selector(multiplexor_D_selector),
    .o_selection(shift_mux_selection)
	);


flipflopD_2N entry_reg_D(
    .clk(clk_5M),
    .rst_n(i_reset_n),
    .en(enable_D),
    .d(shift_mux_selection),
    .q(registered_D)
);


MDR mdr_module(
    //common signals
    .i_clk(clk_5M),
    .i_rst_n(i_reset_n),
    //multiplication signals

    .i_Q_selector(Q_selector),
    .i_enable_QR(enable_QR),
    .o_result(result),
    //sqrt signals
    .i_signed_D(registered_D),
    .i_enable_Q(enable_Q),
    .i_enable_R(enable_R),
    .i_R_flag(R_flag), 
    .i_Q_flag(Q_flag),
    .i_final_flag(final_flag),
    .i_OPcode_selector(registered_opcode),
    /*Divider signals*/
    .i_en_abs_Q(en_abs_Q),
    .o_D_shifted_2_left(shifted_D_2_left),
    .o_remainder(remainder),
   
    .o_parcial_result(parcial_result),
    
	.o_abs_Q(abs_Q),
    .o_abs_M(abs_M),
    .o_sign_Q(sign_Q),
    .o_sign_M(sign_M),
    .o_product_vector(product_vector)
	);




flipflopD registro_result(
    .clk(clk_5M),
    .rst_n(i_reset_n),
    .en(enable_final_reg),
    .d(result),
    .q(registered_result)
);

flipflopD registro_remainder(
    .clk(clk_5M),
    .rst_n(i_reset_n),
    .en(enable_final_reg),
    .d(remainder),
    .q(registered_remainder)
	);

/*reg_2N_t product_vector_a2;

A2_complement_full full_vector_a2(
    .i_full_data({registered_remainder, registered_result}),
    .i_sign((sign_Q ^ sign_M)),
    .o_full_data(product_vector_a2)
);*/

error_check error_mdr(
	.i_op_code(registered_opcode),
	.i_abs_Q(abs_Q),
	.i_abs_M(abs_M),
	.i_sign_Q(sign_Q),
	.i_sign_M(sign_M),
	.i_product(registered_result),
	.i_reminder(registered_remainder),
	.i_en_post_error(en_post_error),
	.i_en_pre_error(en_pre_error),
	.i_product_vector({registered_remainder, registered_result}),//product_vector_a2),//product_vector),
	.o_pre_error(pre_error),
	.o_post_error(post_error)
	);


datos_t decoder_bus;
assign decoder_bus = (o_error)? 10'b1111111111: registered_result;

decoder_bcd_top display_result(
	.i_number(decoder_bus),
	.o_units(o_units),
	.o_tens(o_tens),
	.o_cents(o_cents),
	.sign(o_sign)
	);


result_overflow oveeer(
    .i_register_result(registered_result),
    .flag(((pre_error&ready)|(post_error&ready))),
    .o_data_res(data_res)
);
/*
result_overflow flooow(
    .i_register_result(registered_remainder),
    .flag(((pre_error&ready)|(post_error&ready))),
    .o_data_res(data_rmndr)
);
*/
remainder_overflow rmndr_overflow(
    .i_register_remainder(registered_remainder),
    .flag(((pre_error&ready)|(post_error&ready))),
    .i_op_code(registered_opcode),
    .o_data_remainder(data_rmndr)
);

assign o_ready = ready; 
assign o_result = data_res;//(o_error)? MINUS_1 : registered_result;
assign o_reminder = data_rmndr;//registered_remainder;
assign o_error = ((pre_error&ready)|(post_error&ready)); //active led on low
assign o_parcial_result = parcial_result;
assign o_load_x = en_load_X;
assign o_load_y = en_load_Y;

endmodule