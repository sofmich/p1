// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            March 8th, 2021
// File:			
// Module name:		
// Project Name:	
// Description:		

import top_p2_pkg::*;

module multiplexor2(
    input datos_t i_m_abs,//opcion 0
   // input datos_t i_zeros_value,//opcion 1
    input mult_mux_selector i_selector,
    output datos_t o_selection
);

full_reg_t temp_output;

always_comb begin : multiplexor
    case (i_selector)
        ADDITION: begin
            temp_output = i_m_abs;
        end
        SUBSTRACTION: begin
            temp_output = i_m_abs;
        end
        default: begin
            temp_output = '0;
        end
    endcase
end

assign o_selection = temp_output;

endmodule