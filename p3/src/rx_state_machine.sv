// Coder:           Sofia Michel, Ethan Castillo
// Date:            April 7th, 2021
// File:			rx_state_machine.sv
// Description:		This module is the reception interface for a uart driver

 
import uart_pkg::*;
module rx_state_machine(
    input bit i_clk,
    input logic i_reset_n,
    input logic i_reception,
    input logic i_clear_interrupt,
    input logic i_ovf_cycle,
    input logic i_ovf_data,
    input logic i_ovf_start,
    output logic o_en_cntr_cycle,
    output logic o_delay_selector,
    //output logic //o_en_load,//////
    output logic o_en_sipo,
    output logic o_en_parity,
    output logic o_rx_interrupt,
    output logic o_calibrate
);

sm_uart_e current_state=IDLE;
sm_uart_e next_state=IDLE;

always_comb begin : states
    case (current_state)
        IDLE: begin
            if(FALSE == i_reception)
	            next_state = START;
            else
            	next_state = IDLE;
        end
        START: begin
	        if(i_ovf_cycle)
		        next_state = START2;
            //if(FALSE == i_reception)
	        //    next_state = START;
            else
            	//next_state = IDLE;
                next_state = START;
        end
        START2: begin
             if(i_ovf_start)
		        next_state = DATA;
            //if(FALSE == i_reception)
	        //    next_state = START;
            else
            	//next_state = IDLE;
                next_state = START2;
        end
        DATA: begin
            if(i_ovf_data)
	            next_state = PARITY;
            else
            	next_state = DATA;
        end
        PARITY: begin
	        if(i_ovf_cycle)
		        next_state = STOP;          
            else
            	next_state = PARITY;
        end
        STOP: begin
           if(i_ovf_cycle & i_reception)
		        next_state = WAITING;          
            else
            	next_state = STOP;
           end
        WAITING: begin
           if(i_clear_interrupt)
		        next_state = IDLE;          
            else
            	next_state = WAITING;
        end 
        default: begin  //WAITING STATE
            
        end
    endcase
end

always_comb begin : actions

    case (current_state)
        IDLE: begin
	        o_en_cntr_cycle = FALSE;
            o_delay_selector = FALSE;
	    	//o_en_load = FALSE;
	    	o_en_sipo = FALSE;
	    	o_en_parity = FALSE;
	    	o_rx_interrupt = FALSE;  
            o_calibrate = FALSE;
        end
        START: begin
			o_en_cntr_cycle = TRUE;
	    	o_delay_selector = TRUE;
            //o_en_load = FALSE;
	    	o_en_sipo = FALSE;
	    	o_en_parity = FALSE;
	    	o_rx_interrupt = FALSE;  
            o_calibrate = TRUE;

        end
        START2: begin
            o_en_cntr_cycle = TRUE;
	    	o_delay_selector = FALSE;
            //o_en_load = FALSE;
	    	o_en_sipo = TRUE;
	    	o_en_parity = FALSE;
	    	o_rx_interrupt = FALSE;  
            o_calibrate = TRUE;

        end
        DATA: begin
	        o_en_cntr_cycle = TRUE;
	    	o_delay_selector = FALSE;
            //o_en_load = FALSE;
	    	o_en_sipo = TRUE;
	    	o_en_parity = FALSE;
	    	o_rx_interrupt = FALSE;              
            o_calibrate = FALSE;

            
        end
        PARITY: begin
	        o_en_cntr_cycle = TRUE;
	    	o_delay_selector = FALSE;
            //o_en_load = FALSE;
	    	o_en_sipo = FALSE;
	    	o_en_parity = TRUE;
	    	o_rx_interrupt = FALSE;  
            o_calibrate = FALSE;

        end
        STOP: begin
		    o_en_cntr_cycle = TRUE;
	    	o_delay_selector = FALSE;
            //o_en_load = FALSE;
	    	o_en_sipo = FALSE;
	    	o_en_parity = FALSE;
	    	o_rx_interrupt = FALSE;   
            o_calibrate = FALSE;
    
        end 
        WAITING: begin  //WAITING STATE
        	o_en_cntr_cycle = FALSE;
		  	o_delay_selector = FALSE;
			//o_en_load = FALSE;
			o_en_sipo = FALSE;
			o_en_parity = FALSE;
			o_rx_interrupt = TRUE;
        	o_calibrate = FALSE;            
        end

        default: begin
        	o_en_cntr_cycle = FALSE;
		  	o_delay_selector = FALSE;
			//o_en_load = FALSE;
			o_en_sipo = FALSE;
			o_en_parity = FALSE;
			o_rx_interrupt = TRUE;
        	o_calibrate = FALSE;
        end
    endcase
end

always_ff @( posedge i_clk or negedge i_reset_n ) begin 
    if(!i_reset_n)begin
        current_state <= IDLE;
    end
    else
        current_state <= next_state;
end

endmodule