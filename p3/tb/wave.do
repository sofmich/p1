onerror {resume}
quietly virtual function -install {/top_t06_tb/uut/pp/generate_request[2]/round_robin/rrb_sm} -env /top_t06_tb/uut/pp/pop_cmd5/gen_sm[2]/in_poping_sm/wait_for_len_counter { &{/top_t06_tb/uut/pp/generate_request[2]/round_robin/rrb_sm/current_state, /top_t06_tb/uut/pp/generate_request[2]/round_robin/rrb_sm/i_ack, /top_t06_tb/uut/pp/generate_request[2]/round_robin/rrb_sm/i_no_req_vec, /top_t06_tb/uut/pp/generate_request[2]/round_robin/rrb_sm/o_valid }} rrb3
quietly WaveActivateNextPane {} 0
add wave -noupdate /top_t06_tb/itf/big_fifo_refill_c_ovf
add wave -noupdate /top_t06_tb/clk
add wave -noupdate -radix unsigned /top_t06_tb/uut/pp/registered_cmd
add wave -noupdate -radix unsigned /top_t06_tb/uut/pp/registered_dst
add wave -noupdate -radix unsigned /top_t06_tb/uut/pp/registered_length
add wave -noupdate -radix unsigned /top_t06_tb/uut/pp/registered_M
add wave -noupdate -radix unsigned /top_t06_tb/uut/pp/registered_src
add wave -noupdate -radix hexadecimal /top_t06_tb/uut/pp/uart_received_data
add wave -noupdate /top_t06_tb/uut/pp/state_machine_cmd/current_state
add wave -noupdate -expand -group rrb1 -color White {/top_t06_tb/uut/pp/generate_request[0]/round_robin/rrb_sm/current_state}
add wave -noupdate -expand -group rrb1 -color White {/top_t06_tb/uut/pp/generate_request[0]/round_robin/rrb_sm/i_ack}
add wave -noupdate -expand -group rrb1 -color White {/top_t06_tb/uut/pp/generate_request[0]/round_robin/rrb_sm/i_no_req_vec}
add wave -noupdate -expand -group rrb1 -color White {/top_t06_tb/uut/pp/generate_request[0]/round_robin/rrb_sm/o_valid}
add wave -noupdate -expand -group FIFOS_OUT -color Yellow -radix hexadecimal {/top_t06_tb/uut/pp/generate_M_fifos[0]/gen_M_output_fifos/memory/ram}
add wave -noupdate -expand -group FIFOS_OUT -color Yellow -radix hexadecimal {/top_t06_tb/uut/pp/generate_M_fifos[1]/gen_M_output_fifos/memory/ram}
add wave -noupdate -expand -group FIFOS_OUT -color Yellow -radix hexadecimal {/top_t06_tb/uut/pp/generate_M_fifos[2]/gen_M_output_fifos/memory/ram}
add wave -noupdate -expand -group FIFOS_OUT -color Yellow -radix hexadecimal {/top_t06_tb/uut/pp/generate_M_fifos[3]/gen_M_output_fifos/memory/ram}
add wave -noupdate -expand -group FIFOS_IN -color Yellow -radix hexadecimal {/top_t06_tb/uut/pp/generate_M_fifos[0]/gen_M_data_fifos/memory/ram}
add wave -noupdate -expand -group FIFOS_IN -color Yellow -radix hexadecimal {/top_t06_tb/uut/pp/generate_M_fifos[1]/gen_M_data_fifos/memory/ram}
add wave -noupdate -expand -group FIFOS_IN -color Yellow -radix hexadecimal {/top_t06_tb/uut/pp/generate_M_fifos[2]/gen_M_data_fifos/memory/ram}
add wave -noupdate -expand -group FIFOS_IN -color Yellow -radix hexadecimal {/top_t06_tb/uut/pp/generate_M_fifos[3]/gen_M_data_fifos/memory/ram}
add wave -noupdate -expand -group big_fifo /top_t06_tb/uut/pp/big_fifo_controller/big_fifo_refill_c_ovf
add wave -noupdate -expand -group big_fifo /top_t06_tb/uut/pp/big_fifo_controller/clear_count_flag
add wave -noupdate -expand -group big_fifo -radix decimal /top_t06_tb/uut/pp/big_fifo_controller/cntr
add wave -noupdate -expand -group big_fifo /top_t06_tb/uut/pp/big_fifo_controller/current_state
add wave -noupdate -expand -group big_fifo -radix hexadecimal /top_t06_tb/uut/pp/big_fifo_controller/fifo_count
add wave -noupdate -expand -group big_fifo /top_t06_tb/uut/pp/big_fifo_controller/i_big_fifo_empty
add wave -noupdate -expand -group big_fifo /top_t06_tb/uut/pp/big_fifo_controller/i_clk
add wave -noupdate -expand -group big_fifo /top_t06_tb/uut/pp/big_fifo_controller/i_in_queues_empty
add wave -noupdate -expand -group big_fifo /top_t06_tb/uut/pp/big_fifo_controller/i_out_queues_empty
add wave -noupdate -expand -group big_fifo -radix hexadecimal /top_t06_tb/uut/pp/big_fifo_controller/i_registered_cmd
add wave -noupdate -expand -group big_fifo /top_t06_tb/uut/pp/big_fifo_controller/i_reset_n
add wave -noupdate -expand -group big_fifo /top_t06_tb/uut/pp/big_fifo_controller/o_pop_mux_selector
add wave -noupdate -expand -group big_fifo /top_t06_tb/uut/pp/big_fifo_controller/pass_from_fifos_empty_flag
add wave -noupdate -expand -group big_fifo /top_t06_tb/uut/pp/big_fifo_controller/pass_from_fifos_flag
add wave -noupdate -expand -group big_fifo /top_t06_tb/uut/pp/big_fifo_controller/refill_enabled
add wave -noupdate -expand -group big_fifo /top_t06_tb/uut/pp/big_fifo_controller/refill_fifo_flag
add wave -noupdate -expand -group big_fifo -radix decimal /top_t06_tb/uut/pp/big_fifo_controller/registered_fifo_count
add wave -noupdate -expand -group big_fifo /top_t06_tb/uut/pp/big_fifo_controller/start_counter_and_poping_flag
add wave -noupdate -expand -group rrb_big_fifo -color Gold /top_t06_tb/uut/pp/big_fifo_controller/round_robin/i_ack
add wave -noupdate -expand -group rrb_big_fifo -color Gold -radix binary /top_t06_tb/uut/pp/big_fifo_controller/round_robin/o_grant
add wave -noupdate -expand -group rrb_big_fifo -color Gold /top_t06_tb/uut/pp/big_fifo_controller/round_robin/req_vector
add wave -noupdate /top_t06_tb/uut/pp/big_fifo_controller/round_robin/rrb_sm/current_state
add wave -noupdate -radix hexadecimal {/top_t06_tb/uut/pp/generate_M_fifos[0]/gen_M_output_fifos/DataOutput}
add wave -noupdate {/top_t06_tb/uut/pp/generate_M_fifos[0]/gen_M_output_fifos/empty}
add wave -noupdate {/top_t06_tb/uut/pp/generate_M_fifos[0]/gen_M_output_fifos/pop}
add wave -noupdate -radix hexadecimal {/top_t06_tb/uut/pp/generate_M_fifos[1]/gen_M_output_fifos/DataOutput}
add wave -noupdate {/top_t06_tb/uut/pp/generate_M_fifos[1]/gen_M_output_fifos/empty}
add wave -noupdate {/top_t06_tb/uut/pp/generate_M_fifos[1]/gen_M_output_fifos/pop}
add wave -noupdate -radix hexadecimal {/top_t06_tb/uut/pp/generate_M_fifos[2]/gen_M_output_fifos/DataOutput}
add wave -noupdate {/top_t06_tb/uut/pp/generate_M_fifos[2]/gen_M_output_fifos/empty}
add wave -noupdate {/top_t06_tb/uut/pp/generate_M_fifos[2]/gen_M_output_fifos/pop}
add wave -noupdate -radix hexadecimal {/top_t06_tb/uut/pp/generate_M_fifos[3]/gen_M_output_fifos/DataOutput}
add wave -noupdate {/top_t06_tb/uut/pp/generate_M_fifos[3]/gen_M_output_fifos/empty}
add wave -noupdate {/top_t06_tb/uut/pp/generate_M_fifos[3]/gen_M_output_fifos/pop}
add wave -noupdate -radix hexadecimal -childformat {{{/top_t06_tb/uut/pp/big_fifo/memory/ram[31]} -radix hexadecimal} {{/top_t06_tb/uut/pp/big_fifo/memory/ram[30]} -radix hexadecimal} {{/top_t06_tb/uut/pp/big_fifo/memory/ram[29]} -radix hexadecimal} {{/top_t06_tb/uut/pp/big_fifo/memory/ram[28]} -radix hexadecimal} {{/top_t06_tb/uut/pp/big_fifo/memory/ram[27]} -radix hexadecimal} {{/top_t06_tb/uut/pp/big_fifo/memory/ram[26]} -radix hexadecimal} {{/top_t06_tb/uut/pp/big_fifo/memory/ram[25]} -radix hexadecimal} {{/top_t06_tb/uut/pp/big_fifo/memory/ram[24]} -radix hexadecimal} {{/top_t06_tb/uut/pp/big_fifo/memory/ram[23]} -radix hexadecimal} {{/top_t06_tb/uut/pp/big_fifo/memory/ram[22]} -radix hexadecimal} {{/top_t06_tb/uut/pp/big_fifo/memory/ram[21]} -radix hexadecimal} {{/top_t06_tb/uut/pp/big_fifo/memory/ram[20]} -radix hexadecimal} {{/top_t06_tb/uut/pp/big_fifo/memory/ram[19]} -radix hexadecimal} {{/top_t06_tb/uut/pp/big_fifo/memory/ram[18]} -radix hexadecimal} {{/top_t06_tb/uut/pp/big_fifo/memory/ram[17]} -radix hexadecimal} {{/top_t06_tb/uut/pp/big_fifo/memory/ram[16]} -radix hexadecimal} {{/top_t06_tb/uut/pp/big_fifo/memory/ram[15]} -radix hexadecimal} {{/top_t06_tb/uut/pp/big_fifo/memory/ram[14]} -radix hexadecimal} {{/top_t06_tb/uut/pp/big_fifo/memory/ram[13]} -radix hexadecimal} {{/top_t06_tb/uut/pp/big_fifo/memory/ram[12]} -radix hexadecimal} {{/top_t06_tb/uut/pp/big_fifo/memory/ram[11]} -radix hexadecimal} {{/top_t06_tb/uut/pp/big_fifo/memory/ram[10]} -radix hexadecimal} {{/top_t06_tb/uut/pp/big_fifo/memory/ram[9]} -radix hexadecimal} {{/top_t06_tb/uut/pp/big_fifo/memory/ram[8]} -radix hexadecimal} {{/top_t06_tb/uut/pp/big_fifo/memory/ram[7]} -radix hexadecimal} {{/top_t06_tb/uut/pp/big_fifo/memory/ram[6]} -radix hexadecimal} {{/top_t06_tb/uut/pp/big_fifo/memory/ram[5]} -radix hexadecimal} {{/top_t06_tb/uut/pp/big_fifo/memory/ram[4]} -radix hexadecimal} {{/top_t06_tb/uut/pp/big_fifo/memory/ram[3]} -radix hexadecimal} {{/top_t06_tb/uut/pp/big_fifo/memory/ram[2]} -radix hexadecimal} {{/top_t06_tb/uut/pp/big_fifo/memory/ram[1]} -radix hexadecimal} {{/top_t06_tb/uut/pp/big_fifo/memory/ram[0]} -radix hexadecimal}} -subitemconfig {{/top_t06_tb/uut/pp/big_fifo/memory/ram[31]} {-height 15 -radix hexadecimal} {/top_t06_tb/uut/pp/big_fifo/memory/ram[30]} {-height 15 -radix hexadecimal} {/top_t06_tb/uut/pp/big_fifo/memory/ram[29]} {-height 15 -radix hexadecimal} {/top_t06_tb/uut/pp/big_fifo/memory/ram[28]} {-height 15 -radix hexadecimal} {/top_t06_tb/uut/pp/big_fifo/memory/ram[27]} {-height 15 -radix hexadecimal} {/top_t06_tb/uut/pp/big_fifo/memory/ram[26]} {-height 15 -radix hexadecimal} {/top_t06_tb/uut/pp/big_fifo/memory/ram[25]} {-height 15 -radix hexadecimal} {/top_t06_tb/uut/pp/big_fifo/memory/ram[24]} {-height 15 -radix hexadecimal} {/top_t06_tb/uut/pp/big_fifo/memory/ram[23]} {-height 15 -radix hexadecimal} {/top_t06_tb/uut/pp/big_fifo/memory/ram[22]} {-height 15 -radix hexadecimal} {/top_t06_tb/uut/pp/big_fifo/memory/ram[21]} {-height 15 -radix hexadecimal} {/top_t06_tb/uut/pp/big_fifo/memory/ram[20]} {-height 15 -radix hexadecimal} {/top_t06_tb/uut/pp/big_fifo/memory/ram[19]} {-height 15 -radix hexadecimal} {/top_t06_tb/uut/pp/big_fifo/memory/ram[18]} {-height 15 -radix hexadecimal} {/top_t06_tb/uut/pp/big_fifo/memory/ram[17]} {-height 15 -radix hexadecimal} {/top_t06_tb/uut/pp/big_fifo/memory/ram[16]} {-height 15 -radix hexadecimal} {/top_t06_tb/uut/pp/big_fifo/memory/ram[15]} {-height 15 -radix hexadecimal} {/top_t06_tb/uut/pp/big_fifo/memory/ram[14]} {-height 15 -radix hexadecimal} {/top_t06_tb/uut/pp/big_fifo/memory/ram[13]} {-height 15 -radix hexadecimal} {/top_t06_tb/uut/pp/big_fifo/memory/ram[12]} {-height 15 -radix hexadecimal} {/top_t06_tb/uut/pp/big_fifo/memory/ram[11]} {-height 15 -radix hexadecimal} {/top_t06_tb/uut/pp/big_fifo/memory/ram[10]} {-height 15 -radix hexadecimal} {/top_t06_tb/uut/pp/big_fifo/memory/ram[9]} {-height 15 -radix hexadecimal} {/top_t06_tb/uut/pp/big_fifo/memory/ram[8]} {-height 15 -radix hexadecimal} {/top_t06_tb/uut/pp/big_fifo/memory/ram[7]} {-height 15 -radix hexadecimal} {/top_t06_tb/uut/pp/big_fifo/memory/ram[6]} {-height 15 -radix hexadecimal} {/top_t06_tb/uut/pp/big_fifo/memory/ram[5]} {-height 15 -radix hexadecimal} {/top_t06_tb/uut/pp/big_fifo/memory/ram[4]} {-height 15 -radix hexadecimal} {/top_t06_tb/uut/pp/big_fifo/memory/ram[3]} {-height 15 -radix hexadecimal} {/top_t06_tb/uut/pp/big_fifo/memory/ram[2]} {-height 15 -radix hexadecimal} {/top_t06_tb/uut/pp/big_fifo/memory/ram[1]} {-height 15 -radix hexadecimal} {/top_t06_tb/uut/pp/big_fifo/memory/ram[0]} {-height 15 -radix hexadecimal}} /top_t06_tb/uut/pp/big_fifo/memory/ram
add wave -noupdate -radix hexadecimal /top_t06_tb/uut/pp/big_fifo/DataInput
add wave -noupdate /top_t06_tb/uut/pp/big_fifo_controller/o_pop_mux_selector
add wave -noupdate /top_t06_tb/uut/pp/mux_selector_output_fifo
add wave -noupdate /top_t06_tb/uut/pp/big_fifo_controller/o_fifos_pop_flags
add wave -noupdate /top_t06_tb/uut/pp/big_fifo/push
add wave -noupdate /top_t06_tb/uut/pp/big_fifo/ram/data_a
add wave -noupdate /top_t06_tb/uut/pp/big_fifo/ram/rd_addr_b
add wave -noupdate /top_t06_tb/uut/pp/big_fifo/ram/rd_b
add wave -noupdate /top_t06_tb/uut/pp/big_fifo/ram/rd_data_a
add wave -noupdate /top_t06_tb/uut/pp/big_fifo/ram/we_a
add wave -noupdate -radix hexadecimal /top_t06_tb/uut/pp/big_fifo/ram/wr_addr_a
add wave -noupdate /top_t06_tb/uut/pp/big_fifo/full
add wave -noupdate -radix hexadecimal /top_t06_tb/uut/pp/big_fifo/DataOutput
add wave -noupdate -radix hexadecimal /top_t06_tb/uut/pp/uart_pc/i_data_to_transmit
add wave -noupdate /top_t06_tb/uut/pp/uart_pc/i_transmit
add wave -noupdate /top_t06_tb/uut/pp/uart_pc/o_serial_output
add wave -noupdate /top_t06_tb/uut/pp/uart_pc/transmission/FSM_tx/current_state
add wave -noupdate /top_t06_tb/uut/pp/uart_pc/transmission/i_transmit
add wave -noupdate /top_t06_tb/uut/pp/uart_pc/transmit_dbcd
add wave -noupdate /top_t06_tb/uut/pp/uart_pc/transmission/data_to_transmit
add wave -noupdate /top_t06_tb/uut/pp/uart_pc/transmission/i_data_to_transmit
add wave -noupdate /top_t06_tb/uut/pp/big_fifo_pop_one_shot/current_state
add wave -noupdate /top_t06_tb/uut/pp/big_fifo_pop_one_shot/i_registered_cmd
add wave -noupdate /top_t06_tb/uut/pp/big_fifo_pop_one_shot/i_stimulus
add wave -noupdate /top_t06_tb/uut/pp/big_fifo_pop_one_shot/o_first_shot
add wave -noupdate /top_t06_tb/uut/pp/uart_pc/transmission/PISO_tx/d
add wave -noupdate /top_t06_tb/uut/pp/uart_pc/transmission/PISO_tx/i_enable
add wave -noupdate /top_t06_tb/uut/pp/uart_pc/transmission/PISO_tx/i_load
add wave -noupdate /top_t06_tb/uut/pp/uart_pc/transmission/PISO_tx/o_q
add wave -noupdate -radix unsigned /top_t06_tb/uut/pp/big_fifo_controller/wait_for_len_counter/cntr
add wave -noupdate /top_t06_tb/uut/pp/big_fifo_controller/wait_for_len_counter/i_enb
add wave -noupdate -radix decimal /top_t06_tb/uut/pp/big_fifo_controller/wait_for_len_counter/i_max_cnt
add wave -noupdate /top_t06_tb/uut/pp/big_fifo_controller/wait_for_len_counter/o_ovf
add wave -noupdate -radix decimal /top_t06_tb/uut/pp/big_fifo_controller/registered_fifo_count
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 2} {26440492 ns} 0} {{Cursor 4} {40579029 ns} 0} {{Cursor 5} {68384263 ns} 0} {{Cursor 4} {21896401 ns} 0} {{Cursor 5} {49713770 ns} 0}
quietly wave cursor active 2
configure wave -namecolwidth 446
configure wave -valuecolwidth 489
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {9414438 ns} {42980134 ns}
