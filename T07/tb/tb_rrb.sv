`timescale 1ns / 1ps
include "tester_rrb.svh";
module tb_rrb();

import tb_rrb_pkg::*;
//import rrb_pkg::*;
localparam  PERIOD = 20;  // 1/20 ns => ~ 50 MHz

//Signal declaration
bit clk;
bit rst_n;
vector request_vector;
vector granted;
logic valid;
logic ack;
//definition of tester
 tester_rrb t;
// Instance of interface

    tb_rrb_if itf(
        .i_clk(clk)
    );

    rrb_wrapper uut(
        .clk(clk),
        .rst(rst_n),
        .itf(itf.rrb)
    );

/*
top_round_robin top_module(
    .i_clk(clk),  			// 50MHz from fpga
    .i_rst_n(rst_n),	
    .req_vector(request_vector),
    .i_ack(ack),
    .o_valid(valid),
    .o_grant(granted)
);*/

initial begin
    t = new(itf);
    t.init();
    clk = 'd0; 
    rst_n = 'd1; 
    #(2*PERIOD)   
  	rst_n = 'd0; 
    #(2*PERIOD)   rst_n = 'd1; 
   /* request_vector = '0;
    ack = 1;
	clk	= '0;
    @(posedge clk);
    rst_n = 0;
    @(posedge clk);
    rst_n = 1;
    request_vector = 10'b0010001100;
    #(200)ack=0;
     #(200);
    ack=1;
    #200;
    ack=0;
    #(200);
    ack=1;
    #200;
    ack=0;*/
    //@(posedge clk);
    //ack = 0;
    
        fork
          
           t.inject_vector_requests();
            //t.close_file();
           // t.inject_all_data_rrb();
            //t.review_output();
            t.add_grants();
            t.inject_rtl();
            t.ack_giver();
            t.compare_values();
        join
         
        $stop();
end

always begin
    #(PERIOD/2) clk = ~clk;

end

endmodule