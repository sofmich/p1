// Coder:           Sofia Michel, Ethan Castillo
// Date:            April 7th, 2021
// File:			P3_top.sv
// Description:		this module is the top for a command dispatcher

 
import uart_pkg::*;

	
module P3_top (
input bit i_clk, /*50MHz from fpga*/
`ifdef SIMULATION
input bit i_clk2, /*10MHz from PLL*/
output logic o_big_fifo_refill_c_ovf,
`endif
input logic i_rst_n,
input logic i_serial_data_rx,
output logic o_serial_output
//ELIMINAR ESTOS
//input logic i_transmit,
//input uart_data_t i_data_to_transmit,
//output data_t o_received_data
	);
logic clk2;

`ifndef SIMULATION
    pll_50M_10M pll(
        .areset(i_rst_n),
        .inclk0(i_clk),
        .c0(clk2)  // 10 kHz
    );

`else
    assign clk2 = i_clk2;
`endif

logic uart_rx_interrupt;
logic frame_start_flag;
logic frame_end_flag;
logic save_length;
data_t registered_length;
logic save_cmd;
data_t registered_cmd;
logic save_M;
data_t registered_M;
logic save_src;
data_t registered_src;
logic save_dst;
data_t registered_dst;
logic en_rx_data_load;
logic uart_clean_interrupt;
uart_data_t uart_received_data;
logic payload_overflow;
data_t registered_payload;

/*Enable M fifos as an input */

data_t enable_queue;
data_t push_queue;
data_t pop_queue;
//logic [MAX_M-1:0] full_queue_info;
//logic [MAX_M-1:0] full_queue_data;
logic [MAX_M-1:0] empty_queue_info;
logic [MAX_M-1:0] empty_queue_data;
data_t [MAX_M-1:0] queue_data_output_info;
data_t [MAX_M-1:0] queue_data_output_data;
logic en_one_shot;
logic push_info;
logic push_data;
logic [MAX_M-1:0] pop_info;
logic [MAX_M-1:0] pop_data;
data_t data_to_mem;
logic first_shot;
logic second_shot;

data_t[MAX_M-1:0] registered_dst_from_queue;
data_t[MAX_M-1:0] output_fifo_data;



data_t[MAX_M-1:0] dst_vector;
vector[MAX_M-1:0] vector_request;
vector[MAX_M-1:0] grant_enable_vector;

logic[MAX_M-1:0] ack_rrb_signal;
vector[MAX_M-1:0] request_processing;
logic[MAX_M-1:0] request_processing_combined;
logic[MAX_M-1:0] registered_ack_rrb_signal;

logic[MAX_M-1:0] oneshot_ack;
vector enable_output_fifo_push;

/*control input fifo pop info and data*/
logic enable_cmd5_control;
data_t en_pop_info;
data_t en_pop_data;
data_t enabled_M_queues;
/*Signals for pop cmd 5 control*/
logic[MAX_M-1:0] enable_len_reg;
logic[MAX_M-1:0] data_transfer_overflow;
logic[MAX_M-1:0] clean_register_dst;
logic parity_error;

logic big_fifo_empty;
logic[MAX_M-1:0] output_fifos_pop_flags;

logic big_fifo_push;
logic reg_big_fifo_push;
data_t mux_selector_output_fifo;
data_t big_fifo_output_data;
logic big_fifo_input_selector;
data_t big_fifo_input_data;
logic start_uart_tx;
logic[MAX_M-1:0] big_fifo_push_flag;

sm_uart_e uart_current_state_tx;

logic one_shot_big_fifo_pop;

logic retransmision_flag;
logic big_fifo_refill_c_ovf;
always_comb begin : data_review
	frame_start_flag = (uart_received_data == data_t'(FRAMESTART)) ? 1'b1 : 1'b0; /*comparation with FE*/
	frame_end_flag = (uart_received_data == data_t'(FRAMEEND)) ? 1'b1 : 1'b0; /*comparation with FE*/
	enable_cmd5_control = (registered_cmd == data_t'(CMD5))? 1'b1 : 1'b0; 
end

uart_top uart_pc(
	.i_clk(clk2),
	.i_rst_n(i_rst_n),
	.clear_interrupt(uart_clean_interrupt),
	.i_serial_data_rx(i_serial_data_rx),
	.o_serial_output(o_serial_output),
	.o_rx_interrupt(uart_rx_interrupt),
	.o_received_data(uart_received_data),
	// TODO: Eliminate this ports from final version
	.i_transmit(big_fifo_input_selector),//i_transmit),
	.i_data_to_transmit(big_fifo_output_data),//i_data_to_transmit),
	.o_parity_error(parity_error),
	.o_start_bit(start_uart_tx),
	.o_current_state_tx(uart_current_state_tx)
	);

//	assign o_received_data = registered_M;//(uart_received_data == FRAMESTART) ? FRAMESTART : '0; //uart_received_data;


	uart_one_shot #(.NEXT_STATE_SHOT(IDLE), .ONE_SHOT_DW(UART_SM_DW))big_fifo_pop_one_shot(
		.i_clk(clk2),
		.i_rst(i_rst_n),
		.i_stimulus(start_uart_tx),
		.i_registered_cmd(uart_current_state_tx),
		.o_first_shot(one_shot_big_fifo_pop)
	
	);

logic idle_lock_cmd2_cmd5;
assign idle_lock_cmd2_cmd5 = ((registered_cmd == CMD2)|(registered_cmd == CMD5)) ? big_fifo_refill_c_ovf: 1'b1;

cmd_state_machine state_machine_cmd(
	.i_clk(clk2),
	.i_rst(i_rst_n),
	.i_uart_ready(uart_rx_interrupt),
	.i_flag_start(frame_start_flag), // oxFE as start flag
	.i_flag_end(frame_end_flag),		// 0xEF as end flag
	.i_CMD_active(cmd_flag_e'(uart_received_data)),
	.i_registered_len(registered_length),
	.i_payload_overflow(payload_overflow),
	/*List of flags to set */
	.o_save_length(save_length),
	.o_save_cmd(save_cmd),
	.o_save_M(save_M),
	.o_save_SRC(save_src),
	.o_save_DST(save_dst),
	.o_en_rx_data_load(en_rx_data_load), /*For N incoming data when using command 0x04*/
	.o_clean_interrupt(uart_clean_interrupt),
	.o_retransmision(retransmision_flag),
	.i_idle_lock_cmd2_cmd5(idle_lock_cmd2_cmd5)
	);



two_shot_ create_one_shot(
	.i_clk(clk2),
	.i_rst(i_rst_n),
	.i_uart_ready(uart_rx_interrupt&save_dst),
	.o_en_one_shot(en_one_shot),
	.o_first_shot(first_shot),
	.o_second_shot(second_shot)
	);

capture_counter payload_counter
(
        
    .i_clk(clk2),
    .i_reset_n(i_rst_n),
    .i_enb(en_rx_data_load),
    .i_stimulus(uart_rx_interrupt),
	.i_max_cnt(registered_length - data_t'(4)),
    .o_ovf(payload_overflow)
);

control_pop_input pop_cmd5(
	.i_clk(i_clk), /*Poping at 50Mhz*/
	.i_rst(i_rst_n),
	.i_enable(enable_cmd5_control),
	
	.i_empty_queue_info(empty_queue_info),
	.i_in_info_fifo(queue_data_output_info),
	.i_rrb_processing(request_processing_combined),
	.i_enabled_M_queues(enabled_M_queues),
	.o_pop_info(en_pop_info),
	.o_pop_data(en_pop_data),
	.o_enable_len_reg(enable_len_reg),
	.o_data_transfer_overflow(data_transfer_overflow),
	.o_clean_register_dst(clean_register_dst)

	);
flipflop #(.SIZE(DW))length_register(
    .clk(clk2),
    .rst_n(i_rst_n),
    .en(save_length&uart_rx_interrupt),
    .d(uart_received_data), 
    .q(registered_length)
);



flipflop #(.SIZE(DW))cmd_register(
    .clk(clk2),
    .rst_n(i_rst_n),
    .en(save_cmd&uart_rx_interrupt),
    .d(uart_received_data), 
    .q(registered_cmd)
);

flipflop #(.SIZE(DW))M_register(
    .clk(clk2),
    .rst_n(i_rst_n),
    .en(save_M&uart_rx_interrupt),
    .d(uart_received_data), 
    .q(registered_M)
);

flipflop #(.SIZE(DW))src_register(
    .clk(clk2),
    .rst_n(i_rst_n),
    .en(save_src&uart_rx_interrupt),
    .d(uart_received_data), 
    .q(registered_src)
);

flipflop #(.SIZE(DW))dst_register(
    .clk(clk2),
    .rst_n(i_rst_n),
    .en(save_dst&uart_rx_interrupt),
    .d(uart_received_data), 
    .q(registered_dst)
);


enable_M_queues en_M_queues(
	.i_registered_M(registered_M),
	.o_enabled_in_queues(enabled_M_queues)
	);

enable_queue en_queue_n (
	.i_registered_cmd(registered_cmd),
	.i_registered_src(registered_src),
	.i_registered_M(registered_M),
	.i_enabled_M_queues(enabled_M_queues),
	.i_en_one_shot(en_one_shot),//en_one_shot),  
	.i_shot_uart(uart_rx_interrupt),
	.en_length(save_length), 
	.en_dst(save_dst),
	.en_src(save_src),
	.en_rx_data(en_rx_data_load),

	.i_pop_data_enable(en_pop_data),
	.i_pop_info_enable(en_pop_info),

	.o_enabled_queue(enable_queue),
	.o_push_info_queue(push_info), //input queues only
	.o_push_data_queue(push_data), 
	.o_pop_info_queue(pop_info),
	.o_pop_data_queue(pop_data)
	);

always_comb begin : mux_input_fifo
	if(registered_cmd == CMD4 & first_shot == TRUE)
		data_to_mem = registered_dst;
	else if(registered_cmd == CMD4  & second_shot == TRUE)
		
		data_to_mem = registered_length;
		 		
	else
		data_to_mem = uart_received_data;
end
data_t [MAX_M-1:0] data_to_out_fifo;
logic [MAX_M-1:0] empty_out_queue; 
//logic [MAX_M-1:0] full_out_queue; 

genvar l;
generate
		for ( l = 0; l < MAX_M ; l++ ) begin: generate_M_fifos
		fifo_top #(.W_ADDR(W_ADDR_RAM_IN), .W_DATA(DW))gen_M_info_fifos(
		.clk_fpga(clk2), //10MHz from UART for input 
		.clk_uart(i_clk), //50MHz from fpga for output
		.rst_n(i_rst_n),
		.push(enabled_M_queues[l]&enable_queue[l]&push_info),
		.pop(enabled_M_queues[l]&enable_queue[l]&pop_info[l]),
		.DataInput(data_to_mem),
	//	.full(full_queue_info[l]),
		.empty(empty_queue_info[l]),
		.DataOutput(queue_data_output_info[l])
			);
		fifo_top #(.W_ADDR(W_ADDR_RAM_IN), .W_DATA(DW)) gen_M_data_fifos(
		.clk_fpga(clk2),
		.clk_uart(i_clk),
		.rst_n(i_rst_n),
		.push(enabled_M_queues[l]&enable_queue[l]&push_data),
		.pop(request_processing_combined[l] & (~data_transfer_overflow[l])),//enabled_M_queues[l]&enable_queue[l]&pop_data[l]),
		.DataInput(data_to_mem),
		//.full(full_queue_data[l]),
		.empty(empty_queue_data[l]),
		.DataOutput(queue_data_output_data[l])
		);

		flipflop_pre_clear #(.SIZE(DW))dst_register(
			.clk(i_clk),
			.rst_n(i_rst_n),
			.en(enable_len_reg[l]),
			.d(queue_data_output_info[l]), 
			.q(registered_dst_from_queue[l]),
			.clean_register(clean_register_dst[l])

		);
			
		mux8to1 #(.MUX_DW(DW)) mux_output_data(
			.in_1(queue_data_output_data[0]),
			.in_2(queue_data_output_data[1]),
			.in_3(queue_data_output_data[2]),
			.in_4(queue_data_output_data[3]),
			.in_5(queue_data_output_data[4]),
			.in_6(queue_data_output_data[5]),
			.in_7(queue_data_output_data[6]),
			.in_8(queue_data_output_data[7]),
			.i_request_processing(request_processing[l]),
			.o_data_selected(data_to_out_fifo[l])
			);
		fifo_top #(.W_ADDR(W_ADDR_RAM_IN), .W_DATA(DW))gen_M_output_fifos(
			.clk_fpga(i_clk), //Pushing at 50Mhz
			.clk_uart(clk2), //poping at 10MHz
			.rst_n(i_rst_n),
			.push(enable_output_fifo_push[l]/*enabled_M_queues[l]&enable_queue[l]&push_data*/),
			.pop(output_fifos_pop_flags[l]),//fifo_pop_flag/*enabled_M_queues[l]&enable_queue[l]&pop_data[l]*/),
			.DataInput(data_to_out_fifo[l]),
		//	.full(full_out_queue[l]),
			.empty(empty_out_queue[l]),
			.DataOutput(output_fifo_data[l])
		);


	end
endgenerate

	bit_dec_decoder single_ack_decoder(
			.i_grant_shift_quantity(output_fifos_pop_flags),
			.o_single_shift_quantity(mux_selector_output_fifo)
	);

	assign big_fifo_input_data = (big_fifo_input_selector) ? big_fifo_output_data : output_fifo_data[mux_selector_output_fifo];

	fifo_top #(.W_ADDR(W_ADDR_RAM_OUT))big_fifo(
		.clk_fpga(clk2), //Pushing at 10Mhz
		.clk_uart(clk2), //poping at 10MHz
		.rst_n(i_rst_n),
		.push(reg_big_fifo_push),
		.pop(one_shot_big_fifo_pop),
		.DataInput(big_fifo_input_data),//output_fifo_data[mux_selector_output_fifo]),
		//.full(),
		.empty(big_fifo_empty),
		.DataOutput(big_fifo_output_data)//output_fifo_data[l])
	);
	/*round_robin_arbiter round_robin(
		.i_clk(clk2),  			// 50MHz from fpga
		.i_rst_n(i_rst_n),	
		.req_vector(~empty_out_queue),
		.i_ack(ack_rrb_signal[i]),
		.o_valid(),
		.o_grant(grant_enable_vector[i]),
		.o_processing(request_processing[i]),
		.o_enable_push(enable_output_fifo_push[i])			
	);*/

always_comb begin : assign_sources
	dst_vector[0] = registered_dst_from_queue[0];
	dst_vector[1] = registered_dst_from_queue[1];
	dst_vector[2] = registered_dst_from_queue[2];
	dst_vector[3] = registered_dst_from_queue[3];
	dst_vector[4] = registered_dst_from_queue[4];
	dst_vector[5] = registered_dst_from_queue[5];
	dst_vector[6] = registered_dst_from_queue[6];
	dst_vector[7] = registered_dst_from_queue[7];
end

assign request_processing_combined = request_processing[0] | request_processing[1] | request_processing[2] | request_processing[3] | request_processing[4] | request_processing[5] | request_processing[6] | request_processing[7];

data_t[MAX_M-1:0] grant_shift_quantity;
data_t[MAX_M-1:0] single_shift_quantity;

genvar i;
generate
	for ( i= 0; i<MAX_M ;i++ ) begin: generate_request
		fill_vector_request #(.ARBITER_N(data_t'(i+1))) fill_request(
			.i_dst_vector(dst_vector),
			.o_vector_request(vector_request[i])
		);
		assign grant_shift_quantity[i] = (grant_enable_vector[i] & data_transfer_overflow);

		bit_dec_decoder single_ack_decoder(
			.i_grant_shift_quantity(grant_shift_quantity[i]),
			.o_single_shift_quantity(single_shift_quantity[i])
		);
		assign ack_rrb_signal[i] = logic'(grant_shift_quantity[i] >> single_shift_quantity[i]);

		round_robin_arbiter round_robin(
			.i_clk(i_clk),  			// 50MHz from fpga
			.i_rst_n(i_rst_n),	
			.req_vector(vector_request[i]),
			.i_ack(ack_rrb_signal[i]),
			//.o_valid(),
			.o_grant(grant_enable_vector[i]),
			.o_processing(request_processing[i]),
			.o_enable_push(enable_output_fifo_push[i])			
		);
	end
endgenerate

big_fifo_sm big_fifo_controller(
    .i_clk(clk2),  // 10kHz
    .i_reset_n(i_rst_n),
    .i_registered_cmd(registered_cmd),
    .i_in_queues_empty(empty_queue_data),
    .i_big_fifo_empty(big_fifo_empty),
    .i_out_queues_empty(empty_out_queue),
	.i_uart_next_data(start_uart_tx),
    .o_pop_mux_selector(big_fifo_input_selector),
	.o_fifos_pop_flags(output_fifos_pop_flags),
	.o_big_fifo_push_flag(big_fifo_push_flag),
	.i_oneshot_big_fifo_pop(one_shot_big_fifo_pop),
	.o_big_fifo_refill_c_ovf(big_fifo_refill_c_ovf),
	.i_retransmision_flag(retransmision_flag)
);

assign big_fifo_push = (big_fifo_input_selector) ? one_shot_big_fifo_pop: (big_fifo_push_flag != '0) ? TRUE : FALSE;

flipflop #(.SIZE(1))ff_big_fifo_push(
    .clk(clk2),
    .rst_n(i_rst_n),
    .en(1'b1),
    .d(big_fifo_push), 
    .q(reg_big_fifo_push)
);

`ifdef SIMULATION
assign o_big_fifo_refill_c_ovf = big_fifo_refill_c_ovf;
`endif

/*re_tx_state_machine re_tx_sm(
    .i_clk(clk2),  // 10MHz
    .i_reset_n(i_rst_n),
    .i_start_tx(transmission_flag|re_transmission_flag),
    .i_num_feedbak_elements(),
    .i_next_data(),
    .o_feedback_began()
);
*/
endmodule

