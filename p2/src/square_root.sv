// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            March 17th, 2021
// File:			
// Module name:		
// Project Name:	
// Description:		

import top_p2_pkg::*;

module square_root(
    input bit i_clk,
    input logic i_rst_n, 
    input reg_2N_t i_signed_D,
    input logic i_enable_Q,
    input logic i_enable_R,
    input logic i_R_flag, 
    input logic i_Q_flag,
    input logic i_final_flag,
    output reg_2N_t o_D_shifted_2_left,
    output datos_t o_result, 
    output datos_t o_remainder
);

logic signo;
reg_2N_t full_D_abs;
reg_2N_t D_shifted_2_left;
datos_t reg_R_output;
datos_t reg_Q_output;

datos_t mux_R_selection;
datos_t mux_Q_selection;

datos_t mux_pre_adder_R_selection;
datos_t mux_pre_adder_R_entry;

datos_t mux_pre_adder_Q_selection;
datos_t mux_pre_adder_Q_entry_0;
datos_t mux_pre_adder_Q_entry_1;

datos_t mux_btw_1_and_3_selection;
datos_t mux_btw_0_and_1_selection;

datos_t mux_entry_Q_1;

datos_t add_sub_result;

datos_t last_mux_selection;

logic mux_pre_adder_selector;

always_comb begin : blockName
    D_shifted_2_left = i_signed_D<<2;
    mux_pre_adder_R_entry = (reg_R_output<<2)|(i_signed_D[N+1:N]);
    mux_pre_adder_Q_entry_0 = mux_btw_1_and_3_selection|(reg_Q_output<<2);
    mux_pre_adder_Q_entry_1 = (reg_Q_output<<1)|ONE;
    mux_entry_Q_1 = mux_btw_0_and_1_selection|(reg_Q_output<<1);
    mux_pre_adder_selector = i_final_flag&(reg_R_output[N-1]);
end

multiplexor mux_entry_R(
    .i_first('0),//opcion 0
    .i_second(add_sub_result),//opcion 1 la salida del componente de suma y resta
    .i_selector(i_R_flag),
    .o_selection(mux_R_selection)
);

flipflopD reg_R(
    .clk(i_clk),
    .rst_n(i_rst_n),
    .en(i_enable_R),
    .d(mux_R_selection),
    .q(reg_R_output)
);

multiplexor mux_entry_Q(
    .i_first('0),//opcion 0
    .i_second(mux_entry_Q_1),//opcion 1
    .i_selector(i_Q_flag),
    .o_selection(mux_Q_selection)
);

flipflopD reg_Q(
    .clk(i_clk),
    .rst_n(i_rst_n),
    .en(i_enable_Q),
    .d(mux_Q_selection),
    .q(reg_Q_output)
);

multiplexor mux_pre_adder_R(
    .i_first(mux_pre_adder_R_entry),//opcion 0
    .i_second(reg_R_output),//opcion 1
    .i_selector(mux_pre_adder_selector),
    .o_selection(mux_pre_adder_R_selection)
);

multiplexor mux_pre_adder_Q(
    .i_first(mux_pre_adder_Q_entry_0),//opcion 0
    .i_second(mux_pre_adder_Q_entry_1),//opcion 1
    .i_selector(mux_pre_adder_selector),
    .o_selection(mux_pre_adder_Q_selection)
);

multiplexor mux_btw_1_and_3(
    .i_first(ONE),//opcion 0
    .i_second(THREE),//opcion 1
    .i_selector(reg_R_output[N-1]),
    .o_selection(mux_btw_1_and_3_selection)
);

multiplexor mux_btw_0_and_1(
    .i_first(ONE),//opcion 0
    .i_second(ZERO),//opcion 1
    .i_selector(add_sub_result[N-1]),// el bit mas significativo del resultado de la suma
    .o_selection(mux_btw_0_and_1_selection)
);

add_subs_mux adder_sub(
    .i_first_value(mux_pre_adder_R_selection),
    .i_second_value(mux_pre_adder_Q_selection),
    .i_selector(reg_R_output[N-1]),
    .o_selection(add_sub_result)
);

multiplexor last_mux(
    .i_first(reg_R_output),//opcion 0
    .i_second(add_sub_result),//opcion 1
    .i_selector(reg_R_output[N-1]),
    .o_selection(last_mux_selection)
);

assign o_D_shifted_2_left = D_shifted_2_left;
assign o_remainder = last_mux_selection;
assign o_result = reg_Q_output;

endmodule