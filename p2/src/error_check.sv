// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            March 8th, 2021
// File:			
// Module name:		
// Project Name:	
// Description:		

import top_p2_pkg::*;

module error_check(
	input op_code_t i_op_code,
	input datos_t i_abs_Q,
	input datos_t i_abs_M,
	input logic i_sign_Q,
	input logic i_sign_M,
	input datos_t i_product,
	input datos_t i_reminder,
	input logic i_en_post_error,
	input logic i_en_pre_error,
	input reg_2N_t i_product_vector,
    output logic o_pre_error,
    output logic o_post_error
    
);
reg_2N_t aux_mult_error ;
reg_2N_t aux_mult_error_abs;
datos_t high_part;
logic spccs;
always_comb begin: check_error
	o_post_error = 0;
	o_pre_error = 0;
	aux_mult_error ='0;
	aux_mult_error_abs = '0;
	high_part = '0;
	spccs = '0;
	case(i_op_code)
		MULTIPLICATION: begin
			if(1 == i_en_post_error ) 
				if(((i_abs_Q == '0) | (i_abs_M == '0))) begin
					o_post_error = '0;
				end
				else if(i_sign_Q ^ i_sign_M) begin//If it comes on a2, then we see the low part [9 bits]
					//aux_mult_error = (~i_product_vector)+1'b1; //get the mag
					high_part = ~(i_product_vector[N2-1:N]);
					aux_mult_error = {high_part,i_product_vector[N-1:0]};
					aux_mult_error_abs = (~aux_mult_error)+1'b1;
					spccs = ((i_abs_Q == 3) & (i_abs_M == 512)) ? 1 : 0;
					o_post_error = (spccs | (aux_mult_error_abs > 512))? 1 : 0;
				//(i_product_vector < -512 | i_product_vector > 511)? 1:0;// (i_reminder != '0 | i_product >= 512)? 1:0;
				end 
				else
					o_post_error = (i_product_vector > 511)? 1 : 0;
			else 
				o_post_error = 0;
		end
		DIVISION: begin
			if(1==i_en_pre_error)
				o_pre_error = ('0 == i_abs_M) ? 1:0; //absM == 0 funcona //| (i_abs_M < i_abs_Q
			else
				o_pre_error = 0;
		end
		SQRT: begin
			if(1==i_en_pre_error)
				o_pre_error = (1 == i_sign_Q) ? 1:0;
			else
				o_pre_error = 0;
		end
		default: begin
			o_post_error = 0;
			o_pre_error = 0;
			end
		
	endcase
end
 
endmodule