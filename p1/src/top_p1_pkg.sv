//Coder: Ethan Castillo Sofia Salazar
//Date: 12/02/2021

`ifndef TOP_P1_PKG_SV
    `define TOP_P1_PKG_SV
    
    package top_p1_pkg;
        localparam N = 4;
        typedef logic [N-1:0] data_t;
        typedef logic [(N*2)-1:0] product_t;
        typedef logic [N-1:0] operand_t;
        localparam W_ST = 3; 
        typedef enum logic [W_ST-1:0]{
            IDLE = 3'b000,
            LOAD = 3'b001,
            PROCESSING = 3'b010,
            OUTPUT_DISPLAY = 3'b011,
            CLEANING = 3'b100
        } ctrl_state;
        localparam      SEGMENTS = 7;
    typedef logic   [3:0] bcd_t;
    typedef logic   [SEGMENTS-1:0] segments_t;

    endpackage
`endif 