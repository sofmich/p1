// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            February 15th, 2021
// File:			state_machine.sv
// Module name:		state_machine
// Project Name:	p1
// Description:		This is the state machine flow for the sequential multiplier

import top_p1_pkg::*;

module state_machine(
    input bit i_clk,  // 10kHz
    input logic i_reset_n,
    input logic i_start,
    input logic i_count_overflow,
    output logic o_ready,		//ready flag
    output logic o_enable,      //processing flag which enables data path
    output logic o_load_flag,	//load flag for LOAD status
    output logic o_cleaning_flag,//cleaning flag for CLEANING status
    output logic o_display_flag	//display flag for OUTPUT status
);

ctrl_state current_state;
ctrl_state next_state;
logic cleaning_flag = '0;
logic load_flag = '0;
logic display_flag = '0;
logic enable = '0;
logic ready;

always_comb begin: cto_comb_entrada
    case(current_state)
        IDLE: begin
            if (i_start)
      		    next_state = LOAD; // Changes status to the next
      	    else
      		    next_state = IDLE; // Stays on IDLE status
        end
        LOAD: begin
      		    next_state = PROCESSING; // Changes to next status
        end
        PROCESSING: begin
            if (i_count_overflow)
      		    next_state = OUTPUT_DISPLAY; // Changes to next status
      	    else
      		    next_state = PROCESSING; // Stays while processing data
        end
        OUTPUT_DISPLAY: begin
            next_state = CLEANING; // Changes to next status
        end
        default: begin //Cleaning state
      		    next_state = IDLE; // Changes to IDLE main status
        end
    endcase
end


always_comb begin : state_machine_action //Checks, cleans and sets flags
    case (current_state)
        IDLE:begin 
            cleaning_flag = '0;
            ready = 1'b1;
            enable = '0;
			load_flag = '0;
			display_flag = '0;
        end 
        LOAD: begin
            ready = '0;
            load_flag = 1'b1;
			cleaning_flag = '0;
            enable = '0;
			display_flag = '0;
        end
        PROCESSING:begin
            load_flag = '0;
            enable = '1;
			cleaning_flag = '0;
            ready = '0;
			display_flag = '0;
        end
        OUTPUT_DISPLAY:begin   
            display_flag = 1'b1;
            ready = '0;
            enable = '0;
			cleaning_flag = '0;
			load_flag = '0;
        end 
        default:begin//Cleaning ready state
            display_flag = '0;
            cleaning_flag = 1'b1;
            ready = '0;
            enable = '0;
			load_flag = '0;
        end
    endcase
end

//This is pipo register for changing SM status
always_ff@(posedge i_clk or negedge i_reset_n)begin:state_machine_ff 
   if (!i_reset_n)begin
        current_state <= IDLE ;
   end
   else
        current_state <= next_state ;
end

assign o_enable = enable;
assign o_ready = ready;
assign o_load_flag = load_flag;
assign o_cleaning_flag = cleaning_flag;
assign o_display_flag = display_flag;

endmodule