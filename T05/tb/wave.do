onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /fifo_tb/uut/clk_fpga
add wave -noupdate /fifo_tb/uut/rst_n
add wave -noupdate /fifo_tb/uut/push
add wave -noupdate /fifo_tb/uut/pop
add wave -noupdate -radix decimal /fifo_tb/uut/DataInput
add wave -noupdate -radix decimal /fifo_tb/uut/ram/data_a
add wave -noupdate -radix decimal /fifo_tb/uut/ram/rd_data_a
add wave -noupdate /fifo_tb/uut/ram/wr_addr_a
add wave -noupdate /fifo_tb/uut/ram/rd_addr_b
add wave -noupdate /fifo_tb/uut/write_ctrl_full/rptr
add wave -noupdate /fifo_tb/uut/write_ctrl_full/wptr
add wave -noupdate /fifo_tb/uut/read_ctrl_empty/rptr
add wave -noupdate -expand /fifo_tb/uut/memory/ram
add wave -noupdate /fifo_tb/uut/read_ctrl_empty/wptr
add wave -noupdate /fifo_tb/uut/full
add wave -noupdate /fifo_tb/uut/empty
add wave -noupdate /fifo_tb/uut/ram/we_a
add wave -noupdate /fifo_tb/uut/ram/rd_b
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {6050 us} 0}
quietly wave cursor active 1
configure wave -namecolwidth 243
configure wave -valuecolwidth 101
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits us
update
WaveRestoreZoom {4962 us} {6258 us}
