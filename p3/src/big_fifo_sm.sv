
import uart_pkg::*;
module big_fifo_sm(
    input bit i_clk,  // 10kHz
    input logic i_reset_n,
    input data_t i_registered_cmd,
    input logic[MAX_M-1:0] i_in_queues_empty,
    input logic i_big_fifo_empty,
    input logic[MAX_M-1:0] i_out_queues_empty,
    input logic i_uart_next_data,
    output logic o_pop_mux_selector,
    output data_t o_fifos_pop_flags,
    output logic[MAX_M-1:0] o_big_fifo_push_flag,
    input logic i_oneshot_big_fifo_pop,
    output logic o_big_fifo_refill_c_ovf,
    input logic i_retransmision_flag
);

big_fifo_e current_state;
big_fifo_e next_state;
logic refill_enabled;
logic refill_fifo_flag;
logic pass_from_fifos_flag;
logic pass_from_fifos_empty_flag;
logic big_fifo_refill_c_ovf;
data_t registered_fifo_count;
data_t fifo_count;
data_t cntr;
data_t cntr_nxt;
logic clear_count_flag;
logic start_counter_and_poping_flag;

///**RRB SIGNALS**////
data_t grant_shift_quantity;
data_t single_shift_quantity;
logic ack_rrb_signal;
data_t grant_enable_vector;

always_comb begin : state_signals
    pass_from_fifos_flag = ((i_in_queues_empty == 'hFF) & i_registered_cmd == CMD5 & (i_out_queues_empty != 'hFF)) ? TRUE : FALSE;
    refill_fifo_flag = ((i_big_fifo_empty == FALSE) & i_registered_cmd == CMD2) ? TRUE : FALSE;
    pass_from_fifos_empty_flag = (i_out_queues_empty == 'hFF) ? TRUE : FALSE;
end



always_comb begin : estados
    case (current_state)
        BIG_FIFO_IDLE: begin
            if(pass_from_fifos_flag)begin
                next_state = PASS_FROM_O_FIFOS;
            end
            else if(refill_fifo_flag & (~big_fifo_refill_c_ovf))begin
                next_state = REFILL_BIG_FIFO;
            end
            else
                next_state = BIG_FIFO_IDLE;
        end 
        PASS_FROM_O_FIFOS:begin
            if(pass_from_fifos_empty_flag)begin
                next_state = REFILL_BIG_FIFO;
            end
            else
                next_state = PASS_FROM_O_FIFOS;
        end
        REFILL_BIG_FIFO: begin
            if(big_fifo_refill_c_ovf)begin
                next_state = BIG_FIFO_IDLE;
            end
            else
                next_state = REFILL_BIG_FIFO;
        end
        default: begin
            next_state = BIG_FIFO_IDLE;
        end 
    endcase
end

data_t_counter wait_for_len_counter(
	.i_clk(i_clk),
	.i_reset_n(i_reset_n),
	.i_enb(/*refill_enabled &*/ i_oneshot_big_fifo_pop),
	.i_max_cnt(cnt_t'(registered_fifo_count)),
	.o_ovf(big_fifo_refill_c_ovf),
    .i_clear_count(i_retransmision_flag)
);

flipflop #(.SIZE(DW))ff_counter(
    .clk(i_clk),
    .rst_n(i_reset_n),
    .en(pass_from_fifos_empty_flag),
    .d(fifo_count), 
    .q(registered_fifo_count)
);

always_comb begin : acciones
    refill_enabled = FALSE;
    clear_count_flag = FALSE;
    start_counter_and_poping_flag = FALSE;
    case (current_state)
        BIG_FIFO_IDLE: begin
            refill_enabled = FALSE;
            clear_count_flag = TRUE;
            start_counter_and_poping_flag = FALSE;
        end 
        PASS_FROM_O_FIFOS: begin
            refill_enabled = FALSE;
            clear_count_flag = FALSE;
            start_counter_and_poping_flag = TRUE;
        end
        REFILL_BIG_FIFO: begin
            refill_enabled = TRUE;
            clear_count_flag = FALSE;
            start_counter_and_poping_flag = FALSE;
        end
        default: begin
            refill_enabled = FALSE;
            clear_count_flag = FALSE;
            start_counter_and_poping_flag = FALSE;
        end
    endcase
end

always_ff @( posedge i_clk or negedge i_reset_n ) begin : ff_state
    if(!i_reset_n)begin
        current_state <= BIG_FIFO_IDLE;
    end
    else
        current_state <= next_state;
end
always_ff @( posedge i_clk or negedge i_reset_n ) begin : ff_count
    if (!i_reset_n)
        cntr         <=  '0      ;
    else if ((grant_enable_vector & (~i_out_queues_empty)) != '0)
        if (clear_count_flag)
            cntr     <= '0       ;
        else
            cntr     <= cntr_nxt ;
end

always_comb begin : comb_counter
    cntr_nxt = cntr + 1'b1  ;
end

    assign grant_shift_quantity = (grant_enable_vector & i_out_queues_empty);

    bit_dec_decoder single_ack_decoder(
        .i_grant_shift_quantity(grant_shift_quantity),
        .o_single_shift_quantity(single_shift_quantity)
    );
    assign ack_rrb_signal = logic'(grant_shift_quantity >> single_shift_quantity);

 logic[MAX_M-1:0] rrb_processing;
 logic rrb_enable_push;

	round_robin_arbiter round_robin(
			.i_clk(i_clk),  			// 50MHz from fpga
			.i_rst_n(i_reset_n),	
			.req_vector(~i_out_queues_empty),
			.i_ack(ack_rrb_signal),
			//.o_valid(),
			.o_grant(grant_enable_vector),
			.o_processing(rrb_processing),
			.o_enable_push(rrb_enable_push)			
		);
assign fifo_count = cntr;
assign o_pop_mux_selector = refill_enabled;
assign o_fifos_pop_flags = (start_counter_and_poping_flag) ? grant_enable_vector  : '0;
assign o_big_fifo_push_flag = (start_counter_and_poping_flag & (~ack_rrb_signal)) ? grant_enable_vector  : '0;
assign o_big_fifo_refill_c_ovf = big_fifo_refill_c_ovf;

endmodule