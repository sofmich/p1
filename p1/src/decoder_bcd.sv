// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            February 8th, 2021
// File:			decoder_bcd.sv
// Module name:		decoder_bcd
// Project Name:	p1
// Description:		Decoder from number with maxium 3 digits to units, tens and cents 

import top_p1_pkg::*;


module decoder_bcd(
input  product_t number ,
output bcd_t units,
output bcd_t tens,
output bcd_t cents,
output logic sign
);
	
product_t tens_aux;
product_t number_aux;
	
always_comb begin
	if(number[2*N-1] == 1)  /*Negative number so sign is on 0*/
		begin
			sign = 1'b0; //Active on 0 to display 
			number_aux = ~(number) + data_t'(1'd1);
		end
	else
		begin
			sign = 1'b1;
			number_aux = number;
		end
	if(number_aux >= 100) cents = bcd_t'(4'b0001);
	else cents = bcd_t'(4'b0000);
	tens_aux = (number_aux%100) - (number_aux%10);
	units = bcd_t'(number_aux%10);
	case(tens_aux)
		10: tens = bcd_t'(4'd1);
		20: tens = bcd_t'(4'd2);
		30: tens = bcd_t'(4'd3);
		40: tens = bcd_t'(4'd4);
		50: tens = bcd_t'(4'd5);
		60: tens = bcd_t'(4'd6);
		70: tens = bcd_t'(4'd7);
		80: tens = bcd_t'(4'd8);
		90: tens = bcd_t'(4'd9);
		default:
		tens = bcd_t'(4'd0); 
	endcase
	end
endmodule