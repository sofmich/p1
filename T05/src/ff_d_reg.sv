// Coder:         Eduardo Ethandrake, Sofi Michel Salazar Valdovinos
// Description:   This code is a flip-flop D

import sdp_dc_ram_pkg::register_t;

module ff_d_reg (
input bit 	clk,
input logic rst,
input logic enb,
input register_t inp,
output register_t out
);

register_t  ff_d_r;

always_ff@(posedge clk or negedge rst) begin
    if(!rst)
        ff_d_r  <= '0;
    else if (enb)
        ff_d_r  <= inp;
end

assign out  = ff_d_r;

endmodule
