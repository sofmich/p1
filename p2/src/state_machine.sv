// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            February 15th, 2021
// File:			state_machine.sv
// Module name:		state_machine
// Project Name:	p1
// Description:		This is the state machine flow for the sequential multiplier

import top_p2_pkg::*;

module state_machine(
    input bit i_clk,  // 10kHz
    input logic i_reset_n,
    input logic i_start,
    input op_code_t i_op_code,
    input logic i_load,
    input logic i_pre_error,
    //input logic i_count_overflow,
    output logic o_ready,		//ready flag
    output logic o_enable_M,     
    output logic o_enable_Q,	
    output logic o_enable_QR,//cleaning flag for CLEANING status
    output q_state_selector o_Q_selector,	//display flag for OUTPUT status
    output logic o_enable_final_r,
    //parte de control para sqrt
    output logic o_enable_D,
    output logic o_enable_R,
    output logic o_enable_G,
    output logic o_shift_D_flag,
    output logic o_R_flag,
    output logic o_Q_flag,
    output logic o_final_flag,
    //Load registers enable x=m , y=q
    output logic o_en_load_X,
    output logic o_en_load_Y,
    output logic o_en_load_opcode,
    //for divider signals
    output logic o_en_abs_Q,
    output logic o_en_post_error,
    output logic o_en_pre_error
);

ctrl_mult_state current_state;
ctrl_mult_state next_state;
logic enable_M = '0;
logic enable_Q = '0;
logic enable_QR = '0;
q_state_selector Q_selector = q_state_selector'('0);
logic ready = '0;
logic enable_final_r = '0;

//sqrt control parameters
logic enable_D;
logic enable_R;
logic enable_G;
logic shift_D_flag;
logic R_flag;
logic Q_flag;
logic final_flag;

always_comb begin: cto_comb_entrada
    case(current_state)
        IDLE: begin
            if (i_start)
      		    next_state = LOAD_X; // Changes status to the next
      	    else
      		    next_state = IDLE; // Stays on IDLE status
        end
       LOAD_X:
			if(i_load) begin
				 if(i_op_code == SQRT)					//In Square Root only one value is needed
					 next_state = N_10;
				 else
				 	next_state = LOAD_Y; //Load Y
			end
			else
				next_state = LOAD_X;
		LOAD_Y:
			if(i_load)
				 next_state = N_10; //State Running
			else
				next_state = LOAD_Y;
        N_10: begin
	        if(i_pre_error) 
		        next_state = IDLE;
	        else
            	next_state = N_9; // Changes status to the next
        end
        N_9: begin
            next_state = N_8; // Changes status to the next
        end
        N_8: begin
            next_state = N_7; // Changes status to the next
        end
        N_7: begin
            next_state = N_6; // Changes status to the next
        end
        N_6: begin
            next_state = N_5; // Changes status to the next
        end
        N_5: begin
            next_state = N_4; // Changes status to the next
        end
        N_4: begin
            next_state = N_3; // Changes status to the next
        end
        N_3: begin
            next_state = N_2; // Changes status to the next
        end
        N_2: begin
            next_state = N_1; // Changes status to the next
        end
        N_1: begin
            next_state = N_0; // Changes status to the next
        end
        N_0: begin
            next_state = N_MINUS_1; // Changes status to the next
        end
        N_MINUS_1: begin
            next_state = IDLE; // Changes to next status
        end
        default: begin //Cleaning state
      		    next_state = IDLE; // Changes to IDLE main status
        end
    endcase
end


always_comb begin : state_machine_action //Checks, cleans and sets flags
    case (current_state)
        IDLE:begin 
            enable_M = 1'b1;
            enable_Q = 1'b1;
            enable_QR = 1'b1;
            Q_selector = q_state_selector'(2'b00);
            ready = '1;
            enable_final_r = 1'b0;

            enable_D = 1;
            enable_R = 1;
            enable_G = 1;
            shift_D_flag = 0;
            R_flag = 0;
            Q_flag = 0;
            final_flag = 0;
	        
	        o_en_load_opcode = 1;
	        o_en_abs_Q = 1;
	        o_en_load_X = 0;
	        o_en_load_Y = 0;
	        
	        o_en_post_error = (i_op_code == MULTIPLICATION)?1:0;
	        o_en_pre_error = (i_op_code == SQRT | i_op_code == DIVISION)? 1:0;
	       
        end 
        LOAD_X:begin
	        enable_M = 1'b0;
            enable_Q = 1'b1;
            enable_QR = 1'b1;
            Q_selector = q_state_selector'(2'b00);
            ready = '1;
            enable_final_r = 1'b0;

            enable_D = 1;
            enable_R = 1;
            enable_G = 1;
            shift_D_flag = 0;
            R_flag = 0;
            Q_flag = 0;
            final_flag = 0;
	        
	        o_en_load_opcode = 0;
	        o_en_abs_Q = 1;
	        o_en_load_X = 1;
	        o_en_load_Y = 0;
	        
	        o_en_post_error = 0;
	        o_en_pre_error = 0;
	        
        end 
        LOAD_Y:begin
	       	enable_M = 1'b0;
            enable_Q = 1'b0;
            enable_QR = 1'b1;
            Q_selector = q_state_selector'(2'b00);
            ready = '1;
            enable_final_r = 1'b0;

            enable_D = 1;
            enable_R = 1;
            enable_G = 1;
            shift_D_flag = 0;
            R_flag = 0;
            Q_flag = 0;
            final_flag = 0;
	        
	        o_en_load_opcode = 0;
	        o_en_abs_Q = 1;
	        o_en_load_X = 0;
	        o_en_load_Y = 1;
	        
	        o_en_post_error = 0;
	        o_en_pre_error = 0;
	        
	    end      
        N_10: begin
            enable_M = 1'b0;
            enable_Q = 1'b0;
            enable_QR = 1'b1;
            Q_selector = q_state_selector'(2'b01);
            ready = '0;
            enable_final_r = 1'b1;

            enable_D = 0;
            enable_R = 1;
            enable_G = 1;
            shift_D_flag = 1;
            R_flag = 1;
            Q_flag = 1;
            final_flag = 0;
	        
	        o_en_abs_Q = 0;
	        o_en_load_opcode = 0;
	        o_en_load_X = 0;
	        o_en_load_Y = 0;
	        
	        o_en_post_error = 0;
	        o_en_pre_error = (i_op_code != MULTIPLICATION) ? 1:0;
	        
        end
        N_9: begin
            enable_M = 1'b0;
            enable_Q = 1'b0;
            enable_QR = 1'b1;
            Q_selector = q_state_selector'(2'b01);
            ready = '0;
            enable_final_r = 1'b1;

            enable_D = 0;
            enable_R = 1;
            enable_G = 1;
            shift_D_flag = 1;
            R_flag = 1;
            Q_flag = 1;
            final_flag = 0;
	        
	        	        
	        o_en_abs_Q = 0;
	        o_en_load_opcode = 0;
	        o_en_load_X = 0;
	        o_en_load_Y = 0;
	        
	        o_en_post_error = 0;
	        o_en_pre_error = 0;
        end
        N_8: begin
            enable_M = 1'b0;
            enable_Q = 1'b0;
            enable_QR = 1'b1;
            Q_selector = q_state_selector'(2'b01);
            ready = '0;
            enable_final_r = 1'b1;

            enable_D = 0;
            enable_R = 1;
            enable_G = 1;
            shift_D_flag = 1;
            R_flag = 1;
            Q_flag = 1;
            final_flag = 0;
	        
	        o_en_abs_Q = 0;
	        o_en_load_opcode = 0;
	        o_en_load_X = 0;
	        o_en_load_Y = 0;
	        
	        o_en_post_error = 0;
	        o_en_pre_error = 0;
        end
        N_7: begin
            enable_M = 1'b0;
            enable_Q = 1'b0;
            enable_QR = 1'b1;
            Q_selector = q_state_selector'(2'b01);
            ready = '0;
            enable_final_r = 1'b1;

            enable_D = 0;
            enable_R = 1;
            enable_G = 1;
            shift_D_flag = 1;
            R_flag = 1;
            Q_flag = 1;
            final_flag = 0;
	        
	        o_en_abs_Q = 0;
	        o_en_load_opcode = 0;
	        o_en_load_X = 0;
	        o_en_load_Y = 0;
	        
	        o_en_post_error = 0;
	        o_en_pre_error = 0;
        end
        N_6: begin
            enable_M = 1'b0;
            enable_Q = 1'b0;
            enable_QR = 1'b1;
            Q_selector = q_state_selector'(2'b01);
            ready = '0;
            enable_final_r = 1'b1;

            enable_D = 0;
            enable_R = 1;
            enable_G = 1;
            shift_D_flag = 1;
            R_flag = 1;
            Q_flag = 1;
            final_flag = 0;
	        
	       	o_en_abs_Q = 0;
	        o_en_load_opcode = 0;
	        o_en_load_X = 0;
	        o_en_load_Y = 0;
	        
	        o_en_post_error = 0;
	        o_en_pre_error = 0;
        end
        N_5: begin
            enable_M = 1'b0;
            enable_Q = 1'b0;
            enable_QR = 1'b1;
            Q_selector = q_state_selector'(2'b01);
            ready = '0;
            enable_final_r = 1'b1;

            enable_D = 1;
            enable_R = 1;
            enable_G = 1;
            shift_D_flag = 1;
            R_flag = 1;
            Q_flag = 1;
            final_flag = 0;	        
	        
	        o_en_abs_Q = 0;
	        o_en_load_opcode = 0;
	        o_en_load_X = 0;
	        o_en_load_Y = 0;
	        
	        o_en_post_error = 0;
	        o_en_pre_error = 0;
        end
        N_4: begin
            enable_M = 1'b0;
            enable_Q = 1'b0;
            enable_QR = 1'b1;
            Q_selector = q_state_selector'(2'b01);
            ready = '0;
            enable_final_r = 1'b1;

            enable_D = 1;
            enable_R = 1;
            enable_G = 1;
            shift_D_flag = 1;
            R_flag = 1;
            Q_flag = 1;
            final_flag = 0;
	        
	        o_en_abs_Q = 0;
	        o_en_load_opcode = 0;
	        o_en_load_X = 0;
	        o_en_load_Y = 0;
	        
	        o_en_post_error = 0;
	        o_en_pre_error = 0;
        end
        N_3: begin
            enable_M = 1'b0;
            enable_Q = 1'b0;
            enable_QR = 1'b1;
            Q_selector = q_state_selector'(2'b01);
            ready = '0;
            enable_final_r = 1'b1;

            enable_D = 1;
            enable_R = 1;
            enable_G = 1;
            shift_D_flag = 1;
            R_flag = 1;
            Q_flag = 1;
            final_flag = 0;
	        
	       	o_en_abs_Q = 0;
	        o_en_load_opcode = 0;
	        o_en_load_X = 0;
	        o_en_load_Y = 0;
	        
	        o_en_post_error = 0;
	        o_en_pre_error = 0;
	        end
        N_2: begin
            enable_M = 1'b0;
            enable_Q = 1'b0;
            enable_QR = 1'b1;
            Q_selector = q_state_selector'(2'b01);
            ready = '0;
            enable_final_r = 1'b1;

            enable_D = 1;
            enable_R = 1;
            enable_G = 1;
            shift_D_flag = 1;
            R_flag = 1;
            Q_flag = 1;
            final_flag = 0;
	        
	       	o_en_abs_Q = 0;
	        o_en_load_opcode = 0;
	        o_en_load_X = 0;
	        o_en_load_Y = 0;
	        
	        o_en_post_error = 0;
	        o_en_pre_error = 0;
        end
        N_1: begin
            enable_M = 1'b0;
            enable_Q = 1'b0;
            enable_QR = 1'b1;
            Q_selector = q_state_selector'(2'b01);
            ready = '0;
            enable_final_r = 1'b1;

            enable_D = 1;
            enable_R = 1;
            enable_G = 1;
            shift_D_flag = 1;
            R_flag = 1;
            Q_flag = 1;
            final_flag = 0;
	        
	        o_en_abs_Q = 0;
	        o_en_load_opcode = 0;
	        o_en_load_X = 0;
	        o_en_load_Y = 0;
	        
	        o_en_post_error = 0;
	        o_en_pre_error = 0;
        end
        N_0: begin
            enable_M = 1'b0;
            enable_Q = 1'b0;
            case (i_op_code)
                SQRT:
                     enable_QR = 1'b1;
                default: 
                     enable_QR = 1'b0;
            endcase
           
            Q_selector = q_state_selector'(2'b01);
            ready = 1'b0;
            enable_final_r = 1'b1;

            enable_D = 1;
            enable_R = 1;
            enable_G = 1;
            shift_D_flag = 1;
            R_flag = 1;
            Q_flag = 1;
            final_flag = 0;
        
        	o_en_abs_Q = 0;
	        o_en_load_opcode = 0;
	        o_en_load_X = 0;
	        o_en_load_Y = 0;
            
            o_en_post_error = 0;
            o_en_pre_error = 0;
        end
        N_MINUS_1:begin   
            enable_M = 1'b0;
            enable_Q = 1'b0;
            enable_QR = 1'b0;
            Q_selector = q_state_selector'(2'b01);
            ready = 1'b0;
            enable_final_r = 1'b1;

            enable_D = 1;
            enable_R = 1;
            enable_G = 1;
            shift_D_flag = 1;
            R_flag = 1;
            Q_flag = 1;
            final_flag = 1;
	       
	        o_en_abs_Q = 0;
	        o_en_load_opcode = 0;
	        o_en_load_X = 0;
	        o_en_load_Y = 0;
	        
	        o_en_post_error = 0;
	        o_en_pre_error = 0;
        end 
        default:begin//Cleaning ready state
            enable_M = 1'b1;
            enable_Q = 1'b1;
            enable_QR = 1'b1;
            Q_selector = q_state_selector'(2'b00);
            ready = '0;
            enable_final_r = 1'b1;
        end
    endcase
end

//This is pipo register for changing SM status
always_ff@(posedge i_clk or negedge i_reset_n)begin:state_machine_ff 
   if (!i_reset_n)begin
        current_state <= IDLE ;
   end
   else
        current_state <= next_state ;
end

assign o_ready = ready;
assign o_enable_M = enable_M;
assign o_enable_Q = enable_Q;
assign o_enable_QR = enable_QR;
assign o_Q_selector = Q_selector;
assign o_enable_final_r = enable_final_r;

//sqrt assigns
assign o_enable_D = enable_D;
assign o_enable_R = enable_R;
assign o_enable_G = enable_G;
assign o_shift_D_flag = shift_D_flag;
assign o_R_flag = R_flag;
assign o_Q_flag = Q_flag;
assign o_final_flag = final_flag;



endmodule