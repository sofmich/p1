// Coder:           Sofia Michel, Ethan Castillo
// Date:            May 8th, 2021
// File:            one_shot.sv
// Description:     State machine for one shot


import uart_pkg::*;

/*Operates at FPGA frequency 50MHz This module receives the uart processing ready to 
 * activate a one shot the cycle after that*/

module uart_one_shot #(
	parameter NEXT_STATE_SHOT = CMD5,
	parameter ONE_SHOT_DW = DW
)(
		input bit i_clk,
		input logic i_rst,
		input logic i_stimulus,
		input logic[ONE_SHOT_DW-1:0] i_registered_cmd,
		output logic o_first_shot
	
	);
	one_shot_e current_state = WAIT_SHOT;
	one_shot_e next_state = WAIT_SHOT;
	always_comb begin: uart_input_states
		case(current_state)
			WAIT_SHOT:begin
				if(i_stimulus == TRUE)
					next_state = WAIT_1; //Go to make the one shot 
				else
					next_state = WAIT_SHOT; //go to wait for a new shot
			end
			WAIT_1:begin
				next_state = DO_FIRST_SHOT;
			end
			DO_FIRST_SHOT:begin
				next_state = WAIT_2;
			end
			WAIT_2:begin
				if(i_registered_cmd != NEXT_STATE_SHOT)
					next_state = WAIT_2;
				else
					next_state = WAIT_SHOT;
			end
			default: begin 
				next_state = WAIT_SHOT;
			end
			
		endcase
	end


always_ff @( posedge i_clk or negedge i_rst ) begin 
    if(!i_rst)begin
        current_state <= WAIT_SHOT;
    end
    else
        current_state <= next_state;
end


always_comb begin
	case(current_state)
		WAIT_SHOT: begin
			o_first_shot = FALSE;
		end
		DO_FIRST_SHOT: begin
			o_first_shot = TRUE;
		end
		WAIT_1: begin
			o_first_shot = FALSE;
		end
		WAIT_2: begin
			o_first_shot = FALSE;
		end
		default: begin
			o_first_shot = FALSE;
		end
	endcase
end
endmodule
