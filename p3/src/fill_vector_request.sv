// Coder:           Sofia Michel, Ethan Castillo
// Date:            May 8th, 2021
// File:            fill_vector_request.sv
// Description:     State machine for cmd reception


import uart_pkg::*;

/*Operates at FPGA frequency 50MHz This module receives all commands from UART
 * and sets their respective flags*/

module fill_vector_request#(
parameter M_VALUE = MAX_M,
parameter ARBITER_N = MAX_M
)(
    input  data_t[M_VALUE-1:0] i_dst_vector,
    output logic[M_VALUE-1:0] o_vector_request
);

genvar i;
generate
    for (i = 0 ; i<MAX_M; i++) begin : generador
        assign o_vector_request[i] = (i_dst_vector[i] == ARBITER_N) ? 1'b1 : 1'b0;
    end
endgenerate

endmodule