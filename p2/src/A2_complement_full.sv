// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            March 8th, 2021
// File:			
// Module name:		
// Project Name:	
// Description:		
// todo: revisar si se puede mejorar esta parte del complemento a2
import top_p2_pkg::*;

module A2_complement_full(
    input reg_2N_t i_full_data,
    input logic i_sign,
    output reg_2N_t o_full_data
);

reg_2N_t a1_data;
reg_2N_t temp_data;

//modulo para obtener el signo y magnitud del valor de entrada
always_comb begin : complemento
    a1_data = '0;
    temp_data = '0;
    if(1==i_sign) begin
        a1_data = ~i_full_data;
        temp_data = a1_data+1;
    end else begin
        temp_data = i_full_data;
    end
    
end

assign o_full_data = temp_data;

endmodule