// Coder:           Sofia Michel, Eduardo Ethankdrake 
// Date:            March 21st, 2021
// File:			fifo_control_write.sv
// Description:		This is the top module for a fifo register

import sdp_dc_ram_pkg::*;

	
module fifo_control_write (
input   clk_fpga,
input 	rst_n, // reset low active asynchronous
input 	logic push,
input 	register_t rptr,
output 	addr_t address,
output 	logic full,
output 	register_t wptr
	);
	
register_t wbin, wgraynext, wbinnext;
logic full_aux;
	
always_comb begin
	wbinnext = wbin + (push & ~full);
	wgraynext = (wbinnext >> 1) ^ wbinnext;
	address = wbin[W_ADDR-1:0];
	full_aux = (wgraynext =={~rptr[W_ADDR:W_ADDR-1],
 rptr[W_ADDR-2:0]});
		
end


always_ff@(posedge clk_fpga or negedge rst_n) begin
    if(!rst_n)
	    begin
			wptr  <= '0;
		    wbin <= '0;
		    full <= '0;
	    end 
    else begin
	    wbin <= wbinnext;
	    full <= full_aux;
	    wptr <= wgraynext;
    	
    end      
end
	
endmodule

