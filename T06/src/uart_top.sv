// Coder:           Sofia Michel, Ethan Castillo
// Date:            April 7th, 2021
// File:			uart_top.sv
// Description:		this module is a uart driver

 
import uart_pkg::*;

	
module uart_top (
input bit i_clk, 
input logic i_rst_n,
input logic i_transmit,
input uart_data_t i_data_to_transmit,
input logic i_serial_data_rx,
output logic o_serial_output,
output logic o_rx_interrupt,
output uart_data_t o_received_data,
output logic o_parity_error,
input logic clear_interrupt
);

uart_data_t received_data;
uart_rx transmission (
	.i_clk(i_clk),
	.i_rst(i_rst_n),
	.i_transmit(i_transmit),
	.i_data_to_transmit(i_data_to_transmit),
	.o_serial_output(o_serial_output)
	);
uart_receiver reception(
.i_clk(i_clk),
.i_rst_n(i_rst_n),
.serial_data_rx(i_serial_data_rx),
.rx_interrupt(o_rx_interrupt),
.o_received_data(received_data),
.o_parity_error(o_parity_error),
.i_clear_interrupt(clear_interrupt)
);

assign o_received_data = received_data;

endmodule

