//Date: 12/02/2021
`timescale 1us / 1us

module top_p1_tb();
import top_p1_pkg::*;

localparam  ClkPeriod10kHz = 100;  // 1/100 us => ~ 10 kHz

reg ButtonIn_t;
wire Debounced_t, Fail_t;

//Signal declaration
bit clk;
bit rst_n;
logic en;
operand_t mtd;
operand_t mtr;
bit rdy;
//Instance top module
top_p1 top_module(
    .clk(clk),  // 50MHz
    .reset_n(rst_n),
    .start(ButtonIn_t),
    .multd(mtd),
    .multr(mtr),
    .ready(rdy)
);
 // Button Input Stimulus Procedure
   initial begin
      rst_n = 1'b1;
      clk = '0;
      mtd = 1'b1;
      mtr = 1'b1;
      ButtonIn_t <= 0;
      @(posedge clk);
      rst_n = 1'b0;
      @(posedge clk);
      rst_n = 1'b1;
      @(posedge clk);
      ButtonIn_t <= 1;
      @(posedge clk);
      ButtonIn_t <= 0;

   end


always begin
    #(ClkPeriod10kHz/2) clk <= ~clk;
end

endmodule