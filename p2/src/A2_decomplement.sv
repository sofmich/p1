// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            March 8th, 2021
// File:			
// Module name:		
// Project Name:	
// Description:		
// todo: revisar si se puede mejorar esta parte del complemento a2
import top_p2_pkg::*;

module A2_decomplement(
    input datos_t i_signed_data,
    output datos_t o_abs_data,
    output logic o_sign
);

datos_t a1_data = 0;
datos_t temp_data = 0;
logic temp_sign;

//modulo para obtener el signo y magnitud del valor de entrada
always_comb begin : complemento
    temp_sign = i_signed_data[N-1]; 
	temp_data = '0;
	a1_data = '0;
    if(temp_sign) begin
        //el valor del MSB es 1, es un numero negativo
        a1_data = i_signed_data-1;
        temp_data = ~a1_data;
    end else begin
        //el valor del MSB es 0, el numero es positivo
        temp_data = i_signed_data;
    end

end

assign o_abs_data = temp_data;
assign o_sign = temp_sign;

endmodule