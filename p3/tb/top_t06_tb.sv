// Coder:         Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:          March 8th, 2021
// File:			    top_t6_tb.sv
// Module name:		top_t6_tb
// Project Name:	t06 uart
// Description:		El archivo tb para la tarea 6
`timescale 1ns / 1ns
`include "tester_p3.svh";


module top_t06_tb();
import uart_pkg::*;


localparam  ClkPeriod50MHz = 20;  // 1/20 ns => ~ 50 MHz
localparam  ClkPeriod10MHz = 100;  // 1/10 ns => ~ 10 MHz

//Signal declaration
bit clk;
bit clk2;
bit rst_n;
logic serial_data_rx;
logic clear_interrupt;
logic transmit;
uart_data_t data_to_transmit;

//definition of tester
tester_p3 tester;
//instance of interface
tb_p3_if itf(
    .i_clk(clk),
    .i_clk2(clk2)
);

p3_wrapper uut(
    .clk(clk),
   .clk2(clk2),
    .rst(rst_n),
    .itf(itf.p3)
);

logic [15:0][7:0] arr_payload;

initial begin
    //#(2)
        tester = new(itf);
        tester.init();
	   clk		= '0;
	clk2 = '0;
	  @(posedge clk);
      rst_n = 0;
      @(posedge clk);
      rst_n = 1;

        tester.set_ports('h04);

        tester.iniciar_capturas();
        arr_payload[0]='h01;
        arr_payload[1]='h02;
        arr_payload[2]='h03;
        arr_payload[3]='h04;
        arr_payload[4]='h05;
        arr_payload[5]='h06;
        arr_payload[6]='h07;
        arr_payload[7]='h08;
        arr_payload[8]='h09;
        arr_payload[9]='h0a;
        arr_payload[10]='h0b;
        arr_payload[11]='h0c;
        arr_payload[12]='h0d;
        arr_payload[13]='h0e;
        arr_payload[14]='h0f;
        arr_payload[15]='h10;
        tester.cargar_datos(20/*length**/, arr_payload /**payload*/, 'h01/*src*/, 'h02/**dst*/);
        arr_payload[0]='h11;
        arr_payload[1]='h12;
        arr_payload[2]='h13;
        arr_payload[3]='h14;
        arr_payload[4]='h15;
        arr_payload[5]='h16;
        arr_payload[6]='h17;
        arr_payload[7]='h18;
        arr_payload[8]='h19;
        arr_payload[9]='h1a;
        tester.cargar_datos(20/*length**/, arr_payload /**payload*/, 'h02/*src*/, 'h03/**dst*/);
        arr_payload[0]='h08;
        arr_payload[1]='h09;
        arr_payload[2]='h0A;
        arr_payload[3]='h0B;
        arr_payload[4]='h0C;
        tester.cargar_datos(20/*length**/, arr_payload /**payload*/, 'h03/*src*/, 'h04/**dst*/);
        arr_payload[0]='h0D;
        arr_payload[1]='h0E;
        arr_payload[2]='h0F;
        arr_payload[3]='h10;
        arr_payload[4]='h11;
        tester.cargar_datos(20/*length**/, arr_payload /**payload*/, 'h04/*src*/, 'h01/**dst*/);
        //arr_payload[0]='h12;
        //arr_payload[1]='h13;
        //arr_payload[2]='h14;
        //arr_payload[3]='h15;
        //arr_payload[4]='h16;
        //tester.cargar_datos(20/*length**/, arr_payload /**payload*/, 'h05/*src*/, 'h05/**dst*/);
        //arr_payload[0]='h17;
        //arr_payload[1]='h18;
        //arr_payload[2]='h19;
        //arr_payload[3]='h1A;
        //arr_payload[4]='h1B;
        //tester.cargar_datos(20/*length**/, arr_payload /**payload*/, 'h06/*src*/, 'h06/**dst*/);
        //arr_payload[0]='h1C;
        //arr_payload[1]='h1D;
        //arr_payload[2]='h1E;
        //arr_payload[3]='h1F;
        //arr_payload[4]='h20;
        //tester.cargar_datos(20/*length**/, arr_payload /**payload*/, 'h07/*src*/, 'h07/**dst*/);
        //arr_payload[0]='h21;
        //arr_payload[1]='h22;
        //arr_payload[2]='h23;
        //arr_payload[3]='h24;
        //arr_payload[4]='h25;
        //tester.cargar_datos(20/*length**/, arr_payload /**payload*/, 'h08/*src*/, 'h08/**dst*/);
     //   #(8680);
        tester.iniciar_procesamiento();
        tester.retransmitir();
        tester.retransmitir();
        
        //tester.print_all();
       $stop();
   end


always begin
    #(ClkPeriod50MHz/2) clk = ~clk; 
end
always begin
	 #(ClkPeriod10MHz/2) clk2 = ~clk2;
	end

endmodule