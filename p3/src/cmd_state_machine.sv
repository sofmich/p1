// Coder:           Sofia Michel, Ethan Castillo
// Date:            May 8th, 2021
// File:            cmd_state_machine.sv
// Description:     State machine for cmd reception


import uart_pkg::*;

/*Operates at FPGA frequency 50MHz This module receives all commands from UART
 * and sets their respective flags*/

module cmd_state_machine (
		input bit i_clk,
		input logic i_rst,
		input logic i_uart_ready,
		input logic i_flag_start, // oxFE as start flag
		input logic i_flag_end,		// 0xEF as end flag
		input data_t i_registered_len,
		input cmd_flag_e i_CMD_active,
		input logic i_payload_overflow, 
		/*List of flags to set */
		output logic o_save_length,
		output logic o_save_cmd,
		output logic o_save_M,
		output logic o_save_SRC,
		output logic o_save_DST,
		output logic o_en_rx_data_load, /*For N incoming data when using command 0x04*/
		output logic o_clean_interrupt,
		output logic o_retransmision,
		input logic i_idle_lock_cmd2_cmd5
	
	);
	sm_cmd_e current_state = IDLE_FRAME;
	sm_cmd_e next_state = IDLE_FRAME;


	
	always_comb begin: uart_input_states
		case(current_state)
			IDLE_FRAME:
				if(i_uart_ready == TRUE && i_flag_start == TRUE && i_idle_lock_cmd2_cmd5 == TRUE)
					next_state = LENGTH; //Go to get LENGTH
				else
					next_state = IDLE_FRAME; //State IDLE_FRAME
			LENGTH:
				if(i_uart_ready == TRUE)
					next_state = CMD;
				else
					next_state = LENGTH;
			CMD:
				if(i_uart_ready == TRUE)
					case(i_CMD_active)
						CMD1: begin 
							next_state = SET_M;
						end
						CMD2: begin 
							next_state = END;
						end
						CMD3: begin 
							next_state = END;
						end
						CMD4: begin 
							next_state = SRC;
						end
						CMD5: begin 
							next_state = END;
						end
						default: begin
							next_state = CMD;
							end
						
					endcase 	
				else
					next_state = CMD;
			SET_M: /*For CMD 1*/
				if(i_uart_ready == TRUE)
					next_state = END;
				else
					next_state = SET_M;
			SRC: /*For CMD 4 */
				if(i_uart_ready == TRUE)
					next_state = DST;
				else
					next_state = SRC;
			DST:	
				if(i_uart_ready == TRUE)
					next_state = RECEIVING_LOAD;
				else
					next_state = DST;
			RECEIVING_LOAD:
				if(i_payload_overflow)
					next_state = END;
				else
					next_state = RECEIVING_LOAD;			
			END: /*For all cases*/
				if(i_uart_ready == TRUE)
					next_state = IDLE_FRAME;
				else
					next_state = END;
			
			default: begin 
				next_state = IDLE_FRAME;
				end
			
		endcase
	end


always_ff @( posedge i_clk or negedge i_rst ) begin 
    if(!i_rst)begin
        current_state <= IDLE_FRAME;
    end
    else
        current_state <= next_state;
end


always_comb begin
	case(current_state)
		IDLE_FRAME: begin
			o_clean_interrupt = TRUE;
			o_save_length = FALSE;
			o_save_cmd = FALSE;
			o_save_M = FALSE;
			o_save_SRC = FALSE;
			o_save_DST = FALSE;
			o_en_rx_data_load = FALSE;	
			o_retransmision = FALSE;
		end
		LENGTH: begin
			o_clean_interrupt = TRUE;
			o_save_length = TRUE;
			o_save_cmd = FALSE;
			o_save_M = FALSE;
			o_save_SRC = FALSE;
			o_save_DST = FALSE;
			o_en_rx_data_load = FALSE;
			o_retransmision = FALSE;
		end
		CMD: begin
			o_clean_interrupt = TRUE;
			o_save_length = FALSE;
			o_save_cmd = TRUE;
			o_save_M = FALSE;
			o_save_SRC = FALSE;
			o_save_DST = FALSE;
			o_en_rx_data_load = FALSE;
			o_retransmision = (current_state == CMD2) ? TRUE : FALSE;
		end
		SET_M: begin
			o_clean_interrupt = TRUE;
			o_save_length = FALSE;
			o_save_cmd = FALSE;
			o_save_M = TRUE;
			o_save_SRC = FALSE;
			o_save_DST = FALSE;
			o_en_rx_data_load = FALSE;
			o_retransmision = FALSE;
		end
		SRC: begin
			o_clean_interrupt = TRUE;
			o_save_length = FALSE;
			o_save_cmd = FALSE;
			o_save_M = FALSE;
			o_save_SRC = TRUE;
			o_save_DST = FALSE;
			o_en_rx_data_load = FALSE;
			o_retransmision = FALSE;
		end
		DST: begin
			o_clean_interrupt = TRUE;
			o_save_length = FALSE;
			o_save_cmd = FALSE;
			o_save_M = FALSE;
			o_save_SRC = FALSE;
			o_save_DST = TRUE;
			o_en_rx_data_load = FALSE;
			o_retransmision = FALSE;
		end
		RECEIVING_LOAD: begin
			o_clean_interrupt = TRUE;
			o_save_length = FALSE;
			o_save_cmd = FALSE;
			o_save_M = FALSE;
			o_save_SRC = FALSE;
			o_save_DST = FALSE;
			o_en_rx_data_load = TRUE;
			o_retransmision = FALSE;
			
		end
		END: begin
			o_clean_interrupt = TRUE;
			o_save_length = FALSE;
			o_save_cmd = FALSE;
			o_save_M = FALSE;
			o_save_SRC = FALSE;
			o_save_DST = FALSE;
			o_en_rx_data_load = FALSE;
			o_retransmision = FALSE;
		end
		default: begin
			o_clean_interrupt = FALSE;
			o_save_length = FALSE;
			o_save_cmd = FALSE;
			o_save_M = FALSE;
			o_save_SRC = FALSE;
			o_save_DST = FALSE;
			o_en_rx_data_load = FALSE;
			o_retransmision = FALSE;
		end
	endcase
end
endmodule
