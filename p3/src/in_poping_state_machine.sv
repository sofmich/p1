// Coder:           Sofia Michel, Ethan Castillo
// Date:            May 8th, 2021
// File:            cmd_state_machine.sv
// Description:     State machine for cmd reception


import uart_pkg::*;

/*Operates at FPGA frequency 50MHz This module receives all commands from UART
 * and sets their respective flags*/

module in_poping_state_machine (
		input bit i_clk,
		input logic i_rst,
		input logic i_active_CMD5, // señal de inicio
		input data_t i_len_to_count,
		input logic i_fifo_empty,
		input logic i_rrb_processing,
		output logic o_pop_info,
		output logic o_pop_data,
		output logic o_enable_length_register,
		output logic o_data_transfer_overflow,
		output logic o_clean_register_dst
	);
	in_pop_e current_state = IDLE_IN_POP;
	in_pop_e next_state = IDLE_IN_POP;
	
	logic enable_len_counter;
	logic counter_len_ovf;

	logic counter_len_ovf_and_enable;
	assign counter_len_ovf_and_enable = counter_len_ovf & enable_len_counter & i_rrb_processing;
	always_comb begin: input_POP_states
		case(current_state)
			IDLE_IN_POP:
				if(i_active_CMD5 == TRUE & ~i_fifo_empty)
					next_state = POP_DST; //Go to get LENGTH
				else
					next_state = IDLE_IN_POP; //State IDLE_IN_POP
			POP_DST:
					next_state = POP_LEN;
			POP_LEN:
					next_state = WAIT_LEN;
			WAIT_LEN: /*For CMD 1*/
				if(counter_len_ovf_and_enable)
					next_state = IDLE_IN_POP;
				else
					next_state = WAIT_LEN;
			default: begin 
				next_state = IDLE_IN_POP;
				end
		endcase
	end


always_ff @( posedge i_clk or negedge i_rst ) begin 
    if(!i_rst)begin
        current_state <= IDLE_IN_POP;
    end
    else
        current_state <= next_state;
end


data_t_counter wait_for_len_counter(
	.i_clk(i_clk),
	.i_reset_n(i_rst),
	.i_enb(enable_len_counter & i_rrb_processing),
	.i_max_cnt(cnt_t'(i_len_to_count-'h04)),
	.o_ovf(counter_len_ovf),
	.i_clear_count(FALSE)
);

always_comb begin
	enable_len_counter = '0;
	o_pop_info = FALSE;
	o_pop_data = FALSE;
	o_enable_length_register = FALSE;
	o_clean_register_dst = FALSE;
	case(current_state)
		IDLE_IN_POP: begin	
			enable_len_counter = '0;
			o_pop_info = TRUE;
			o_pop_data = FALSE;
			o_enable_length_register = FALSE;
			o_clean_register_dst = TRUE;
		end
		POP_DST: begin
			enable_len_counter = '0;
			o_pop_info = TRUE;
			o_pop_data = FALSE;
			o_enable_length_register = FALSE;
			o_clean_register_dst = FALSE;
		end
		POP_LEN: begin
			enable_len_counter = '0;
			o_pop_info = FALSE;
			o_pop_data = FALSE;
			o_enable_length_register = TRUE;
			o_clean_register_dst = FALSE;
			end
		WAIT_LEN: begin
			enable_len_counter = 1'b1;
			o_pop_info = FALSE;
			o_pop_data = TRUE;
			o_enable_length_register = FALSE;
			o_clean_register_dst = FALSE;
			end
		default: begin
			enable_len_counter = '0;
			o_pop_info = FALSE;
			o_pop_data = FALSE;
			o_enable_length_register = FALSE;
			o_clean_register_dst = FALSE;
			end
	endcase
end

assign o_data_transfer_overflow = counter_len_ovf_and_enable;

endmodule
