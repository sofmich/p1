// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            February 8th, 2021
// File:			decoder_bcd_top.sv
// Module name:		decoder_bcd_top
// Project Name:	p1
// Description:		Decoder instantiations  

import top_p2_pkg::*;


module decoder_bcd_top(
input  datos_t i_number , /* N bits number*/
output segments_t o_units,
output segments_t o_tens,
output segments_t o_cents,
output logic sign
);

bcd_t units_bcd;
bcd_t tens_bcd;
bcd_t cents_bcd;
	
decoder_bcd conversion_bcd(
	.i_number(i_number), 
	.o_units(units_bcd),
	.o_tens(tens_bcd),
	.o_cents(cents_bcd),
	.o_sign(sign));
decoder7_segments decoder_units(
	.number(units_bcd),
	.digit(o_units));
decoder7_segments decoder_tens(
	.number(tens_bcd),
	.digit(o_tens));
decoder7_segments decoder_cents(
	.number(cents_bcd),
	.digit(o_cents));

endmodule

