// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            March 8th, 2021
// File:			
// Module name:		
// Project Name:	
// Description:		

import top_p2_pkg::*;

module shift_left_LSB(
    input datos_t i_data_in,
    input bit i_new_lsb,
    output datos_t o_shifted_data
);

assign o_shifted_data = (i_data_in << 1)| (i_new_lsb);
endmodule