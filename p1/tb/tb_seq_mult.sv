`timescale 1ns / 1ps

module tb_seq_mult;

//FIXME[DEV]: here import from your package
//import seq_mul_pkg::*;
import top_p1_pkg::*; 

localparam IN_DW =  top_p1_pkg::N;
//localparam IN_DW = 2;
//Inputs
logic			         start = 0;
logic			         clk = 1;
logic				      rst = 0;
logic [IN_DW-1:0]	   multiplier;
logic [IN_DW-1:0]	   multiplicand ;
logic [2*IN_DW-1:0]  product;
logic				      ready;
segments_t      o_segments_units;
segments_t      o_segments_tens;
segments_t      o_segments_hundreds;
logic sign;

/*
FIXME[DEV]: Create an instance of 
your sequential multiplier.
*/

/* EXAMPLE
my_ms u_ms(
 .my_clk          (clk)
,.my_multiplier   (multiplier)
,.my_multiplicand (multiplicand)
,.my_product      (product)
,.my_rdy          (ready)
);
*/
top_p1 top_module(
   .i_clk(clk),  // 50MHz
   .i_reset_n(rst),
   .i_start(start),
   .i_multd(multiplicand),
   .i_multr(multiplier),
   .o_ready(ready),
   .o_units(o_segments_units),
   .o_tens(o_segments_tens),
   .o_cents(o_segments_hundreds),
   .o_sign(sign),
   .o_product(product)
);

// DO NOT TOUCH //
// The code from here to the end //
always #1 clk <= ~clk;
integer cont = 0;
integer m0_temp;
integer m1_temp;
initial begin
   #0.1 rst = 0;
   #2 rst = 1;
   start = 0;
   clk = 0;
   multiplier   = -(2**(IN_DW-1));
   multiplicand = -(2**(IN_DW-1));
   repeat (2**IN_DW) begin
      $display("------Tabla del %d------",$signed(multiplier));
      repeat (2**IN_DW) begin
         m0_temp = multiplier    ;
         m1_temp = multiplicand    ;
         start = 1;  #2
         start = 0;
         #(2*IN_DW+6)     // Waiting N+3.5 cycles
         //$monitor() $strobe()
       //  $display("%d * %d = %d",$signed(multiplier),$signed(multiplicand),$signed(product));
         if (($signed(multiplier)*$signed(multiplicand) != $signed(product)) ) begin
            cont = cont + 1 ;
            $display("[%0dps] MISMATCH AT %d * %d = %d | FOUND = %d",$time, $signed(multiplier),$signed(multiplicand),$signed(multiplicand*multiplier),$signed(product));
         end
         multiplicand = multiplicand+1;
        end
        multiplier = multiplier+1;
    end   
    $display("%d",cont);
    #500
    $stop;
end
 


endmodule
