// Coder:           Sofia Michel, Ethan Castillo
// Date:            May 8th, 2021
// File:			enable_M_queues.sv
// Description:		This module is a queue enabler 

 
import uart_pkg::*;

	
module enable_M_queues(
input data_t i_registered_M,
output data_t o_enabled_in_queues
	);
	
	always_comb begin: enabled_queue_M
		case(i_registered_M)
		ONE:
			o_enabled_in_queues = 'b1;
		TWO:
			o_enabled_in_queues = 'b11;
		THREE:
			o_enabled_in_queues = 'b111;
		FOUR:
			o_enabled_in_queues = 'b1111;	
		FIVE:
			o_enabled_in_queues = 'b11111;
		SIX:
			o_enabled_in_queues = 'b111111;
		SEVEN:
			o_enabled_in_queues = 'b01111111;
		EIGHT:
			o_enabled_in_queues = 'hFF;
		default: 
			o_enabled_in_queues = 'b0;
		endcase
	end
endmodule

