import top_p2_pkg::*;

module remainder_overflow(
    input datos_t i_register_remainder,
    input logic flag,
    input op_code_t i_op_code,
    output datos_t o_data_remainder
);

datos_t aux;

always_comb begin : blockName
    aux = '0;
    if(flag) begin
        aux = i_register_remainder;
    end else begin
        case (i_op_code)
            MULTIPLICATION: begin
                aux = '0; 
            end
            default:
                aux = MINUS_1; 
        endcase
    end
end

assign o_data_remainder = i_register_remainder;

endmodule