//Coder: Ethan Castillo, Sofia Salazar
// file: ppc
import rrb_pkg::*;

module ppc_rrb(
    input vector i_req_vector,
    output vector o_ppc_output
);

genvar i;
vector ppc_output;
vector req_ppc_in;

assign req_ppc_in = i_req_vector;
assign ppc_output[0] = req_ppc_in[0]; //assign lsb, for | comparation

generate for (i = 1; i < N_REQ;i++) begin
        assign ppc_output[i] = req_ppc_in[i] | ppc_output[i-1];
    end
endgenerate

//atrix_t ppc_matrix;
//assign ppc_matrix[N_REQ-1:0 ][0] = i_req_vector;
/*genvar j;
generate for (j= 0; j < LEVELS ;j++) begin
        for (i= 0; i<N_REQ ;i++) begin
            assign ppc_matrix[i][j] = 
        end
    end
endgenerate
*/
assign o_ppc_output = ppc_output;

endmodule