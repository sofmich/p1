// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            March 8th, 2021
// File:			flipflopD.sv
// Module name:		flipflopD
// Project Name:	sequential_multiplier
// Description:		this is a flipflopD register module
import top_p2_pkg::*;

module flipflopD(
input bit       clk,
input logic       rst_n,
input logic		en,
input datos_t    d,
output datos_t   q

	);
datos_t q_aux;
	
	
always_ff@(posedge clk, negedge rst_n) begin
	if(!rst_n)
		q_aux <= '0;
	else if(en) /*This is PIPO register (array of D flip-flop)*/
		q_aux <= d;
end

assign q = q_aux;

endmodule

