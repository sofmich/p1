// Coder:           Eduardo Castillo, Sofia Michel
// Date:            March 21st, 2021
// File:			fifo_control_read.sv
// Description:		This is the top module for a fifo register

import uart_pkg::*;
module fifo_control_read#(
		parameter W_ADDR = 4,
		parameter W_DATA = 8
	) (
input   clk_fpga,
input 	rst_n, 
input 	logic i_pop,
input 	 logic [W_ADDR:0]  wptr,
output logic empty,
output logic [W_ADDR-1:0]  address,
output 	 logic [W_ADDR:0] rptr
	);

 logic [W_ADDR:0] wgraynext, wbinnext, wbin;
logic empty_aux;


always_ff@(posedge clk_fpga or negedge rst_n) begin
    if(!rst_n)
	    begin
			rptr  <= '0;
		    wbin <= '0;

		    empty <= '1;

		    
	    end 
    else begin
	    wbin <= wbinnext;
	    empty <= empty_aux;
	    rptr <= wgraynext;
    	
    end      
end
assign wgraynext = (wbinnext >> 1)^wbinnext;

assign wbinnext = wbin + (i_pop & ~empty);
assign address = wbin[W_ADDR-1:0];
assign empty_aux = (wptr == wgraynext); 
	
	

	
endmodule

