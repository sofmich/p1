// Coder:           Sofia Michel, Eduardo Castillo
// Date:            April 7th, 2021
// File:			uart_pkg.sv


`ifndef UART_PKG_SV
	`define UART_PKG_SV
	package uart_pkg;
	localparam 		DW = 8;
	localparam 		STATE_W = 3;
	typedef logic 	[DW-1:0] data_t;
	localparam UART_DW = DW;
	typedef logic [UART_DW-1:0] uart_data_t;
	// TENER UN PARAMETRO LOCAL QUE INDIQUE LA CANTIDAD...
	//... DE BITS A UTILIZAR EN LOS CONTADORES PARA LOS DELAYS


	localparam RB = 115_200;
	localparam TP = 1/RB;
	localparam F = 50_000_000;
	//localparam TS = 1/F;
	//localparam DELAY_MAXCNT   	 = TS/TP; ///esto se tiene que calcular en base al bit rate.... 1/Rb * frecuencia
	localparam DELAY_MAXCNT = F/RB+1;
	localparam HALF_DELAY_MAXCNT = DELAY_MAXCNT/2;
	localparam DELAY_DW       	= $clog2(DELAY_MAXCNT);
	typedef logic [DELAY_DW:0] cnt_t;
	typedef logic          ovf_t;

	typedef struct {
	cnt_t       count;
	ovf_t       ovf;
	} cntr_t;
	typedef enum logic [2:0]{ 
	 IDLE = 3'b000,
	 START = 3'b001,
	 START2 = 3'b110,
	 DATA = 3'b010,
	 PARITY = 3'b011,
	 STOP  = 3'b100,
	 WAITING = 3'b101
	} sm_uart_e;
		
	typedef enum logic{ 
	 FALSE = 0,
	 TRUE = 1
    } logic_e;
endpackage
`endif