//Coder: Sofia Salazar, Ethan Castillo
//filename: arbitro

import uart_pkg::*;


module arbiter(
    input vector i_req_vector,
    output vector o_grant_vector
);

vector ppc_output;
vector grant_vector;

ppc_rrb module_ppc(
    .i_req_vector(i_req_vector),
    .o_ppc_output(ppc_output)
);

granter granter_module(
    .i_ppc_output(ppc_output),
    //.o_mask(mask),
    .o_masked_grant_vector(grant_vector)
);


assign o_grant_vector = grant_vector;

endmodule