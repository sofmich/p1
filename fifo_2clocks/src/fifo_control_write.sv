// Coder:           Sofia Michel, Eduardo Ethankdrake 
// Date:            March 21st, 2021
// File:			fifo_control_write.sv
// Description:		This is the top module for a fifo register

import sdp_dc_ram_pkg::*;

	
module fifo_control_write #(
		parameter W_ADDR = 4,
		parameter W_DATA = 8
	) (
input   clk_fpga,
input 	rst_n, // reset low active asynchronous
input 	logic push,
input 	logic [W_ADDR:0] rptr,
output 	logic [W_ADDR-1:0] address,
output 	logic full,
output 	logic [W_ADDR:0] wptr
	);
	
logic [W_ADDR:0] wbin, wgraynext, wbinnext;
logic full_aux;
	
always_comb begin
	wbinnext = wbin + (push & ~full);
	wgraynext = (wbinnext >> 1) ^ wbinnext;
	address = wbin[W_ADDR-1:0];
	full_aux = (wgraynext =={~rptr[W_ADDR:W_ADDR-1],
 rptr[W_ADDR-2:0]});
		
end


always_ff@(posedge clk_fpga or negedge rst_n) begin
    if(!rst_n)
	    begin
			wptr  <= '0;
		    wbin <= '0;
		    full <= '0;
	    end 
    else begin
	    wbin <= wbinnext;
	    full <= full_aux;
	    wptr <= wgraynext;
    	
    end      
end
	
endmodule

