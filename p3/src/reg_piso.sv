// Coder:           Sofia Michel, Ethan Castillo
// Date:            April 7th, 2021
// File:			piso.sv
// Description:		This module is a reg_piso from LSB first
 
import uart_pkg::*;
module reg_piso(
input bit i_clk,
input logic	i_rst,
input logic i_enable,
input logic i_load,
input uart_data_t d,
output logic o_q
	);
uart_data_t shifted_reg;
logic q;
always_ff@(posedge i_clk, negedge i_rst)begin
        if(!i_rst)begin
            q <= '0;
        end
        else begin
	        if (i_enable)
		        if(i_load)
			        shifted_reg <= d;
		        else
			        shifted_reg <= shifted_reg >> 1;
            q <= shifted_reg[0];
        end
end

assign o_q = q;
endmodule

