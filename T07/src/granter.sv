//Coder: Ethan Castillo, Sofia Salazar
// file: binary decoding
import rrb_pkg::*;

module granter(
    input vector i_ppc_output,
    output vector o_mask,
    output vector o_masked_grant_vector 
);


always_comb begin : decoding
    o_mask = i_ppc_output<<1;
    o_masked_grant_vector = o_mask^i_ppc_output;
end

endmodule