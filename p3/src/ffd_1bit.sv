// Coder:           Sofia Michel, Ethan Castillo
// Date:            April 7th, 2021
// File:			ffd_1bit.sv
// Description:		This module is a 1bit flip flop

 
import uart_pkg::*;

	
module ffd_1bit(
input bit i_clk,
input logic	i_rst,
input logic enable,
input logic d,
output logic q
);
always_ff@(posedge i_clk, negedge i_rst)begin
        if(!i_rst)begin
            q <= '0;
        end
        else begin
	        if (enable)
            	q <= d;
        end
    end

endmodule

