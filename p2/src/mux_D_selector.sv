import top_p2_pkg::*;

module mux_D_selector(
    input op_code_t i_operation,
    input logic i_shift_D_flag,
    output logic o_flag
);

logic flag;

always_comb begin : flag_selector
    case (i_operation)
        SQRT: begin
            flag = i_shift_D_flag;
        end
        default: begin
            flag = '0;
        end
    endcase
end

assign o_flag = flag;

endmodule