// Coder:           Eduardo Castillo, Sofia Michel
// Date:            March 21st, 2021
// File:			fifo_control_read.sv
// Description:		This is the top module for a fifo register

import sdp_dc_ram_pkg::*;

module fifo_control_read (
input   clk_fpga,
input 	rst_n, 
input 	logic i_pop,
input 	register_t  wptr,
output logic empty,
output addr_t address,
output 	register_t rptr
	);

register_t wgraynext, wbinnext, wbin;
logic empty_aux;


always_ff@(posedge clk_fpga or negedge rst_n) begin
    if(!rst_n)
	    begin
			rptr  <= '0;
		    wbin <= '0;

		    empty <= '1;

		    
	    end 
    else begin
	    wbin <= wbinnext;
	    empty <= empty_aux;
	    rptr <= wgraynext;
    	
    end      
end
assign wgraynext = (wbinnext >> 1)^wbinnext;

assign wbinnext = wbin + (i_pop & ~empty);
assign address = wbin[W_ADDR-1:0];
assign empty_aux = (wptr == wgraynext); 
	
	

	
endmodule

