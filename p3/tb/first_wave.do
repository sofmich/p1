onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /top_t06_tb/top_module/i_rst_n
add wave -noupdate /top_t06_tb/top_module/i_clk
add wave -noupdate /top_t06_tb/top_module/o_serial_output
add wave -noupdate -radix hexadecimal /top_t06_tb/top_module/registered_cmd
add wave -noupdate -radix hexadecimal /top_t06_tb/top_module/registered_dst
add wave -noupdate -radix hexadecimal /top_t06_tb/top_module/registered_length
add wave -noupdate -radix hexadecimal /top_t06_tb/top_module/registered_M
add wave -noupdate -radix hexadecimal /top_t06_tb/top_module/registered_src
add wave -noupdate /top_t06_tb/top_module/save_cmd
add wave -noupdate /top_t06_tb/top_module/save_dst
add wave -noupdate /top_t06_tb/top_module/save_length
add wave -noupdate /top_t06_tb/top_module/save_M
add wave -noupdate /top_t06_tb/top_module/save_src
add wave -noupdate /top_t06_tb/top_module/uart_clean_interrupt
add wave -noupdate /top_t06_tb/top_module/uart_received_data
add wave -noupdate /top_t06_tb/top_module/state_machine_cmd/current_state
add wave -noupdate /top_t06_tb/top_module/uart_rx_interrupt
add wave -noupdate /top_t06_tb/top_module/uart_pc/reception/rx_states/current_state
add wave -noupdate /top_t06_tb/top_module/state_machine_cmd/i_uart_ready
add wave -noupdate -expand /top_t06_tb/top_module/vector_request
add wave -noupdate -expand /top_t06_tb/top_module/source_vector
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 2} {256343331 ps} 0} {{Cursor 4} {796045469 ps} 0} {{Cursor 5} {25755849 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 409
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {999630743 ps} {1000019435 ps}
