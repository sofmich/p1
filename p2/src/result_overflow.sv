import top_p2_pkg::*;

module result_overflow(
    input datos_t i_register_result,
    input logic flag,
    output datos_t o_data_res
);

datos_t aux;

always_comb begin : blockName
    aux = MINUS_1;
    if(flag) begin
        aux = MINUS_1;
    end else begin
        aux = i_register_result;
    end
end

assign o_data_res = aux;

endmodule