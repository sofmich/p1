// Coder:           Ethan Castillo,Sofia Michel
// Date:            April 7th, 2021
// File:			uart_rx.sv
// Description:		This module is the reception interface for a uart driver

 
import uart_pkg::*;

	
module sipo#(
    parameter SIZE = DW,
    type VAR_TYPE
)(
    input bit i_clk, 
    input logic i_rst_n,
    input logic  i_serial_data,
    input logic capture,
    output VAR_TYPE o_paralel_data
   // output logic ready
);

VAR_TYPE registered_data;
VAR_TYPE shifted_data;
VAR_TYPE data;

flipflop #(.SIZE(DW), .VAR_TYPE(data_t))ff(
    .clk(i_clk),
    .rst_n(i_rst_n),
    .en(capture),
    .d(data), 
    .q(registered_data)
);

always_comb begin : calculo
    shifted_data = registered_data>>1;
    data = {i_serial_data,shifted_data[DW-2:0]};
end

assign o_paralel_data = registered_data;

endmodule