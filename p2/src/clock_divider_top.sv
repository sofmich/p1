// Coder:           Sofia Michel
// Date:            February 21st, 2021
// File:			clock_divider_top.sv
// Description:		This module gives the requested clock frequency
//					NOTE: this smaller frequency must be smaller than the referenced
 
import top_p2_pkg::*;

	
module clock_divider_top #(
	//Parameters for clock
	parameter FREQUENCY = 5_000,
	parameter REFERENCE_CLOCK = 10_000//50_000_000
	)(
// clk is the FPGA clock or the PLL output adjust the parameter
input   clk_fpga,
// reset low active asynchronous
input 	rst_n,
output 	clk
		);
	// The amount to count
localparam CNT_VALUE = (REFERENCE_CLOCK/(2*FREQUENCY));
bit clk_status;
data_divider_t clk_counter;
data_divider_t count_aux;

//TODO: implement 2 states low and  high
// Make the counting until it is needed to make a flip

always_comb begin
	clk_counter = count_aux + 'd1;
end

always_ff@(posedge clk_fpga, negedge rst_n)
	begin: clk_divider
		if(!rst_n) begin
			clk_status <= 0;
			count_aux <= 0;
		end
		else begin
			if(clk_counter > CNT_VALUE-1 )	begin
				clk_status <= ~clk_status;
				count_aux <= 0;
			end
			else begin
				count_aux <= clk_counter;
				end
		end
	end: clk_divider

//Assign the new divided clock at the output
assign clk = clk_status;
endmodule

