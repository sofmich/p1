//Coder: Ethan Castillo, Sofia Salazar

import rrb_pkg::*;

module top_round_robin(
    input bit i_clk,  			// 50MHz from fpga
    input logic i_rst_n,	
    input vector req_vector,
    input logic i_ack,
    output logic o_valid,
    output vector o_grant
);

vector unmasked_grant_vector;
vector masked_grant_vector;
vector grant_selection;
vector vector_mask;
vector mask_and_request;
logic mux_grant_selector;
vector registered_request;
vector registered_grant;
logic pre_reg_valid;
logic reg_valid;

/*flipflop #(.VAR_TYPE(vector))ff_entry(
    .clk(i_clk),
    .rst_n(i_rst_n),
    .en(i_clk),
    .d(req_vector), 
    .q(registered_request)
);*/

arbiter unmasked_priority_arbiter(
    .i_req_vector(req_vector),//registered_request),
    .o_grant_vector(unmasked_grant_vector)
);

arbiter masked_priority_arbiter(
    .i_req_vector(mask_and_request),
    .o_grant_vector(masked_grant_vector)
);

multiplexor #(
.VAR_TYPE(vector)
)delay_mux(
    .i_first(masked_grant_vector),//opcion 0
    .i_second(unmasked_grant_vector),//opcion 1
    .i_selector(mux_grant_selector),
    .o_selection(grant_selection)
);

mask_logic module_mask_logic(
    .i_grant(registered_grant),
    .o_mask(vector_mask)
);

always_comb begin : logic_mask
    mask_and_request = req_vector/*registered_request*/ & vector_mask;
    mux_grant_selector = (mask_and_request == 0) ? 1:0;
    pre_reg_valid = (registered_grant != '0 && i_ack == '0 ) ? 1'b1 : '0;
end

/*state_machine rrb_sm(
    .i_clk(i_clk),  // 10kHz
    .i_reset_n(i_rst_n),
    .i_ack(i_ack),
    .i_no_req(registered_request == '0),
    .o_valid(pre_reg_valid)
);*/

flipflop #(.VAR_TYPE(vector))ff(
    .clk(i_clk),
    .rst_n(i_rst_n),
    .en(i_ack),
    .d(grant_selection), 
    .q(registered_grant)
);

flipflop #(.VAR_TYPE(logic))ff_valid(
    .clk(i_clk),
    .rst_n(i_rst_n),
    .en(1'b1),
    .d(pre_reg_valid), 
    .q(reg_valid)
);

assign o_grant = registered_grant;
assign o_valid = pre_reg_valid;//reg_valid;

endmodule