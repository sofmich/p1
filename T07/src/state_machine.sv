
import rrb_pkg::*;
module state_machine(
    input bit i_clk,  // 10kHz
    input logic i_reset_n,
    input logic i_ack,
    input logic i_no_req,
    output logic o_valid
);

ctrl_state current_state;
ctrl_state next_state;
logic valid = '0;

always_comb begin : estados
    case (current_state)
        COMPUTE: begin
            if(!i_no_req)
                next_state = GRANT;
            else
                next_state = COMPUTE;
        end 
        default: begin
            if(i_ack)
                next_state = COMPUTE;
            else
                next_state = GRANT;
        end 
    endcase
end

always_comb begin : acciones
    //valid = '0;
    case (current_state)
        COMPUTE: begin
           valid = '0;
        end 
        default: begin
        //    
         valid = (!i_no_req) ? 1'b1 : '0;
        end
    endcase
end

always_ff @( posedge i_clk or negedge i_reset_n ) begin : cuenta
    if(!i_reset_n)begin
        current_state <= COMPUTE;
    end
    else
        current_state <= next_state;
end

assign o_valid = valid;

endmodule