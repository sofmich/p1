// Coder:           Sofia Michel, Ethan Castillo
// Date:            April 7th, 2021
// File:			uart_tx.sv
// Description:		This module is the transmission interface for a uart driver

 
import uart_pkg::*;

	
module uart_tx(
	input bit i_clk,
	input logic	i_rst,
	input logic i_transmit,
	input uart_data_t i_data_to_transmit,
	output logic o_serial_output,
	output logic o_start_bit,
	output sm_uart_e o_current_state
	);
logic en_counter;
logic en_counter_cycle;
logic en_load_reg;
logic en_reg;
logic en_out;
logic en_parity;
logic bit_to_transmit;
logic parity;
logic parity_registered;
logic start_bit;
logic stop_bit;
logic start_stop;
logic data;
logic overflow_data;
logic overflow_cycle;
uart_data_t data_to_transmit;
cnt_t delay_selection;
assign delay_selection = cnt_t'(DELAY_MAXCNT);
/* Delay control flow*/
delay_counter delay_cntr(
	.i_clk(i_clk),
	.i_reset_n(i_rst),
	.i_enb(en_counter_cycle),//provisional),//en_cntr_cycle),
	.i_max_cnt(delay_selection),//delay_selection),
	.o_ovf(overflow_cycle)
);

capture_counter data_cntr(
	.i_clk(i_clk),
	.i_reset_n(i_rst),
	.i_enb(en_counter),//en_cntr_cycle),//provisional),//en_cntr_cycle),
    .i_stimulus(overflow_cycle),
	.i_max_cnt(data_t'(DW)),
	.o_ovf(overflow_data)//ovf_cycle)
);
tx_state_machine FSM_tx(
	.i_clk(i_clk),
	.i_rst(i_rst),
	.i_transmission(i_transmit),
	.i_overflow(overflow_data),
	.i_overflow_cycle(overflow_cycle),
	.o_en_cntr(en_counter),
	.o_en_cntr_cycle(en_counter_cycle),
	.o_en_start(start_bit),
	.o_load(en_load_reg),
	.o_en_piso(en_reg),
	.o_en_out(en_out),
	.o_en_parity(en_parity),
	.o_en_stop(stop_bit),
	.o_current_state(o_current_state)
	);
flipflop #(.SIZE(UART_DW)) ff_input(
	.clk(i_clk),
	.rst_n(i_rst),
	.en(i_transmit),
	.d(i_data_to_transmit),
	.q(data_to_transmit)
	);
// TODO: Add counters flow control and wire en_counter
reg_piso PISO_tx(
	.i_clk(i_clk),
	.i_rst(i_rst),
	.i_enable(overflow_cycle),
	.i_load(en_load_reg),
	.d(data_to_transmit),
	.o_q(bit_to_transmit)
	);
parity_gen parity_tx(
	.i_data(data_to_transmit),
	.o_parity_check(parity)
	);


assign start_stop = (start_bit)? 1'b0 : stop_bit;
assign data = (en_parity)? parity : bit_to_transmit;
assign o_serial_output = (en_out)? data : start_stop;
assign o_start_bit = start_bit;// & overflow_cycle;

endmodule

