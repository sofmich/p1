onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /fifo_tb/uut/clk_fpga
add wave -noupdate /fifo_tb/uut/rst_n
add wave -noupdate /fifo_tb/uut/push
add wave -noupdate /fifo_tb/uut/pop
add wave -noupdate -radix decimal /fifo_tb/uut/DataInput
add wave -noupdate -radix decimal /fifo_tb/uut/ram/data_a
add wave -noupdate -radix decimal /fifo_tb/uut/ram/rd_data_a
add wave -noupdate /fifo_tb/uut/ram/wr_addr_a
add wave -noupdate /fifo_tb/uut/ram/rd_addr_b
add wave -noupdate /fifo_tb/uut/write_ctrl_full/rptr
add wave -noupdate /fifo_tb/uut/write_ctrl_full/wptr
add wave -noupdate -radix decimal /fifo_tb/uut/read_ctrl_empty/rptr
add wave -noupdate -radix hexadecimal -childformat {{{/fifo_tb/uut/memory/ram[15]} -radix hexadecimal} {{/fifo_tb/uut/memory/ram[14]} -radix hexadecimal} {{/fifo_tb/uut/memory/ram[13]} -radix hexadecimal} {{/fifo_tb/uut/memory/ram[12]} -radix hexadecimal} {{/fifo_tb/uut/memory/ram[11]} -radix hexadecimal} {{/fifo_tb/uut/memory/ram[10]} -radix hexadecimal} {{/fifo_tb/uut/memory/ram[9]} -radix hexadecimal} {{/fifo_tb/uut/memory/ram[8]} -radix hexadecimal} {{/fifo_tb/uut/memory/ram[7]} -radix hexadecimal} {{/fifo_tb/uut/memory/ram[6]} -radix hexadecimal} {{/fifo_tb/uut/memory/ram[5]} -radix hexadecimal} {{/fifo_tb/uut/memory/ram[4]} -radix hexadecimal} {{/fifo_tb/uut/memory/ram[3]} -radix hexadecimal} {{/fifo_tb/uut/memory/ram[2]} -radix hexadecimal} {{/fifo_tb/uut/memory/ram[1]} -radix hexadecimal} {{/fifo_tb/uut/memory/ram[0]} -radix hexadecimal}} -expand -subitemconfig {{/fifo_tb/uut/memory/ram[15]} {-height 15 -radix hexadecimal} {/fifo_tb/uut/memory/ram[14]} {-height 15 -radix hexadecimal} {/fifo_tb/uut/memory/ram[13]} {-height 15 -radix hexadecimal} {/fifo_tb/uut/memory/ram[12]} {-height 15 -radix hexadecimal} {/fifo_tb/uut/memory/ram[11]} {-height 15 -radix hexadecimal} {/fifo_tb/uut/memory/ram[10]} {-height 15 -radix hexadecimal} {/fifo_tb/uut/memory/ram[9]} {-height 15 -radix hexadecimal} {/fifo_tb/uut/memory/ram[8]} {-height 15 -radix hexadecimal} {/fifo_tb/uut/memory/ram[7]} {-height 15 -radix hexadecimal} {/fifo_tb/uut/memory/ram[6]} {-height 15 -radix hexadecimal} {/fifo_tb/uut/memory/ram[5]} {-height 15 -radix hexadecimal} {/fifo_tb/uut/memory/ram[4]} {-height 15 -radix hexadecimal} {/fifo_tb/uut/memory/ram[3]} {-height 15 -radix hexadecimal} {/fifo_tb/uut/memory/ram[2]} {-height 15 -radix hexadecimal} {/fifo_tb/uut/memory/ram[1]} {-height 15 -radix hexadecimal} {/fifo_tb/uut/memory/ram[0]} {-height 15 -radix hexadecimal}} /fifo_tb/uut/memory/ram
add wave -noupdate /fifo_tb/uut/read_ctrl_empty/wptr
add wave -noupdate /fifo_tb/uut/full
add wave -noupdate /fifo_tb/uut/empty
add wave -noupdate /fifo_tb/uut/ram/we_a
add wave -noupdate /fifo_tb/uut/ram/rd_b
add wave -noupdate /fifo_tb/uut/DataOutput
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {4122 us} 0}
quietly wave cursor active 1
configure wave -namecolwidth 243
configure wave -valuecolwidth 256
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits us
update
WaveRestoreZoom {0 us} {4320 us}
