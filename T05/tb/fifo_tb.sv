// Coder:           Eduardo Castillo, Sofia Michel
// Date:            March 21st, 2021
// File:			fifo_tb.sv

`timescale 1us / 1us

module fifo_tb();
import sdp_dc_ram_pkg::*;

localparam  ClkPeriod10kHz = 100;  // 1/100 us => ~ 10 kHz
parameter  WLEN = 16;
parameter  DEPTH = 8;
//Signal declaration
bit clk;
bit rst;
logic push;
logic pop;
data_t DataInput;
logic full;
logic empty;
data_t DataOutput;

fifo_top uut(
	.clk_fpga(clk),
	.rst_n(rst),
	.push(push),
	.pop(pop),
	.DataInput(DataInput),
	.full(full),
	.empty(empty),
	.DataOutput(DataOutput)
	
	);
 // Button Input Stimulus Procedure
 initial begin
	   clk		= '0;
	  #(ClkPeriod10kHz) rst = 0;
      #(ClkPeriod10kHz) rst = 1;
 end 
 initial begin
	 
 end 
always  begin 
	 #(ClkPeriod10kHz/2) clk = ~clk;
end

// Make pop and push untill there is no space
initial begin
	# 0  push = 1'b0;
	# 0 	pop = 1'b0;
	#(ClkPeriod10kHz*3)  push = 1'b1;
	#(ClkPeriod10kHz)  push = 1'b1;
	#(ClkPeriod10kHz)  push = 1'b1;
	#(ClkPeriod10kHz)  push = 1'b1;
	#(ClkPeriod10kHz)  push = 1'b0;
	#(ClkPeriod10kHz)  push = 1'b1;
	#(ClkPeriod10kHz)  push = 1'b0;
	#(ClkPeriod10kHz)  push = 1'b1;
	#(ClkPeriod10kHz)  push = 1'b0;
	#(ClkPeriod10kHz)  push = 1'b1;
	#(ClkPeriod10kHz)  push = 1'b0;
	#(ClkPeriod10kHz)  push = 1'b1;
	#(ClkPeriod10kHz)  push = 1'b0;
	#(ClkPeriod10kHz)  push = 1'b1;
	#(ClkPeriod10kHz)  push = 1'b1;
	#(ClkPeriod10kHz)  push = 1'b0;
	#(ClkPeriod10kHz)  push = 1'b1;
	#(ClkPeriod10kHz)  push = 1'b1;
	#(ClkPeriod10kHz)  push = 1'b1;
	#(ClkPeriod10kHz)  push = 1'b0;
	#(ClkPeriod10kHz)  push = 1'b1;
	#(ClkPeriod10kHz)  push = 1'b0;
	#(ClkPeriod10kHz)  push = 1'b1;
	
	# 0  pop = 1'b0;
	#(ClkPeriod10kHz)  pop = 1'b1;
	#(ClkPeriod10kHz)  pop = 1'b1;
	#(ClkPeriod10kHz)  pop = 1'b1;
	#(ClkPeriod10kHz)  pop = 1'b1;
	#(ClkPeriod10kHz)  pop = 1'b0;
	#(ClkPeriod10kHz)  pop = 1'b1;
	#(ClkPeriod10kHz)  pop = 1'b0;
	#(ClkPeriod10kHz)  pop = 1'b1;
	#(ClkPeriod10kHz)  pop = 1'b0;
	#(ClkPeriod10kHz)  pop = 1'b1;
	#(ClkPeriod10kHz)  pop = 1'b0;
	#(ClkPeriod10kHz)  pop = 1'b1;
	#(ClkPeriod10kHz)  pop = 1'b0;
	#(ClkPeriod10kHz)  pop = 1'b1;
	#(ClkPeriod10kHz)  pop = 1'b1;
	#(ClkPeriod10kHz)  pop = 1'b0;
	#(ClkPeriod10kHz)  pop = 1'b1;
	#(ClkPeriod10kHz)  pop = 1'b1;
	#(ClkPeriod10kHz)  pop = 1'b1;
	#(ClkPeriod10kHz)  pop = 1'b0;
	#(ClkPeriod10kHz)  pop = 1'b1;
end


initial begin
	# 0  DataInput = 1'b0;
	#(ClkPeriod10kHz*3)  DataInput = 8;
	#(ClkPeriod10kHz)  DataInput = 7;
	#(ClkPeriod10kHz)  DataInput = 6;
	#(ClkPeriod10kHz)  DataInput = 5;
	#(ClkPeriod10kHz)  DataInput = 0;
	#(ClkPeriod10kHz)  DataInput = 5;
	#(ClkPeriod10kHz)  DataInput = 7;
	#(ClkPeriod10kHz)  DataInput = 5;
	#(ClkPeriod10kHz)  DataInput = 2;
	#(ClkPeriod10kHz)  DataInput = 5;
	#(ClkPeriod10kHz)  DataInput = 0;
	#(ClkPeriod10kHz)  DataInput = 5;
	#(ClkPeriod10kHz)  DataInput = 2;
	#(ClkPeriod10kHz)  DataInput = 5;
	#(ClkPeriod10kHz)  DataInput = 1;
	#(ClkPeriod10kHz)  DataInput = 5;
	#(ClkPeriod10kHz)  DataInput = 7;
	#(ClkPeriod10kHz)  DataInput = 5;
	#(ClkPeriod10kHz)  DataInput = 0;
	#(ClkPeriod10kHz)  DataInput = 1;
	#(ClkPeriod10kHz)  DataInput = 1;
	#(ClkPeriod10kHz)  DataInput = 2;
	#(ClkPeriod10kHz)  DataInput = 7;
	
end



endmodule

	
	
