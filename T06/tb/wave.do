onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /top_t06_tb/top_module/i_rst_n
add wave -noupdate /top_t06_tb/top_module/i_clk
add wave -noupdate /top_t06_tb/top_module/transmission/FSM_tx/current_state
add wave -noupdate /top_t06_tb/top_module/i_transmit
add wave -noupdate /top_t06_tb/top_module/transmission/ff_input/q
add wave -noupdate /top_t06_tb/top_module/o_serial_output
add wave -noupdate /top_t06_tb/top_module/transmission/delay_cntr/i_enb
add wave -noupdate /top_t06_tb/top_module/transmission/delay_cntr/i_max_cnt
add wave -noupdate /top_t06_tb/top_module/transmission/delay_cntr/o_ovf
add wave -noupdate /top_t06_tb/top_module/transmission/delay_cntr/o_count
add wave -noupdate -radix decimal -childformat {{/top_t06_tb/top_module/transmission/delay_cntr/cntr.count -radix decimal} {/top_t06_tb/top_module/transmission/delay_cntr/cntr.ovf -radix decimal}} -subitemconfig {/top_t06_tb/top_module/transmission/delay_cntr/cntr.count {-height 15 -radix decimal} /top_t06_tb/top_module/transmission/delay_cntr/cntr.ovf {-height 15 -radix decimal}} /top_t06_tb/top_module/transmission/delay_cntr/cntr
add wave -noupdate -radix decimal /top_t06_tb/top_module/transmission/delay_cntr/cntr_nxt
add wave -noupdate /top_t06_tb/top_module/transmission/data_cntr/i_enb
add wave -noupdate /top_t06_tb/top_module/transmission/data_cntr/i_stimulus
add wave -noupdate /top_t06_tb/top_module/transmission/data_cntr/o_ovf
add wave -noupdate -radix decimal /top_t06_tb/top_module/transmission/data_cntr/o_count
add wave -noupdate -radix decimal /top_t06_tb/top_module/transmission/data_cntr/cntr
add wave -noupdate -radix decimal /top_t06_tb/top_module/transmission/data_cntr/cntr_nxt
add wave -noupdate /top_t06_tb/top_module/transmission/FSM_tx/i_overflow
add wave -noupdate /top_t06_tb/top_module/transmission/PISO_tx/i_enable
add wave -noupdate /top_t06_tb/top_module/transmission/PISO_tx/i_load
add wave -noupdate /top_t06_tb/top_module/transmission/PISO_tx/d
add wave -noupdate /top_t06_tb/top_module/transmission/PISO_tx/o_q
add wave -noupdate /top_t06_tb/top_module/transmission/PISO_tx/shifted_reg
add wave -noupdate /top_t06_tb/top_module/transmission/PISO_tx/q
add wave -noupdate /top_t06_tb/top_module/transmission/parity_tx/o_parity_check
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 2} {82985 ns} 0} {{Cursor 4} {194568 ns} 0} {{Cursor 5} {39088 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 409
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {13131 ns} {100683 ns}
