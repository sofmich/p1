module mdr_wrapper 
//FIXME[DEV]: import your own package
import top_p2_pkg::*;

//import mdr_pkg::*;
import tb_mdr_pkg::*;
(
input          clk,
input          rst,
tb_mdr_if.mdr  itf
);

//FIXME[DEV]:define two variable using your RTL datatype
datos_t result     ;
datos_t remainder  ;

top_p2_MDR pp(
    .i_clk(itf.clk),  			// 50MHz from fpga
    .i_reset_n(rst),		/* Reset active on high*/
    .i_operand(operand_t'(itf.data)),  /* Data to operate with*/
    .i_start(itf.start),		/* Start button*/
    .i_operation(op_code_t'(itf.op)),/* Operation code*/
    .i_load(itf.load),			/* Load action*/
    .o_error(itf.error),			/* Bit error when overflow*/
    .o_result(result),		/* Result on N bits*/
    .o_reminder(remainder),
    .o_ready(itf.ready),		/* Ready flag*/
    .o_load_x(itf.load_x),
    .o_load_y(itf.load_y)
);

//Instance your own MDR and cast to the specific datatype to RTL
//mdr_top dut(
//    .clk_fpga   ( itf.clk                    ),
//    .rst        ( rst                        ),
//    .data       ( data_dw_t'(itf.data)       ),
//    .op         ( data_2b_t'(itf.op)         ),
//    .load       ( itf.load                   ),
//    .start      ( itf.start                  ),
//    .result     ( result     ),
//    .remainder  ( remainder  ),
//    .ready      ( itf.ready                  ),
//    .error      ( itf.error                  ),
//    .load_x     ( itf.load_x                 ),
//    .load_y     ( itf.load_y                 )
//);

//Cast using this testbench data_t type
assign itf.result    = data_t'( result     );
assign itf.remainder = data_t'( remainder  );
 
          
endmodule
