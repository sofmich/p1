onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /top_p2_tb/clk
add wave -noupdate /top_p2_tb/top_module/estados/o_en_pre_error
add wave -noupdate /top_p2_tb/top_module/estados/o_en_post_error
add wave -noupdate -color Red /top_p2_tb/top_module/error_mdr/o_pre_error
add wave -noupdate -color {Slate Blue} /top_p2_tb/top_module/error_mdr/o_post_error
add wave -noupdate /top_p2_tb/top_module/estados/o_ready
add wave -noupdate -color Pink /top_p2_tb/top_module/o_error
add wave -noupdate /top_p2_tb/top_module/mdr_module/o_product_vector
add wave -noupdate /top_p2_tb/top_module/product_vector_a2
add wave -noupdate /top_p2_tb/top_module/error_mdr/i_product_vector
add wave -noupdate -radix decimal /top_p2_tb/top_module/error_mdr/aux_mult_error
add wave -noupdate -radix decimal /top_p2_tb/top_module/registered_result
add wave -noupdate -radix binary /top_p2_tb/top_module/registered_remainder
add wave -noupdate -radix decimal /top_p2_tb/top_module/i_operand
add wave -noupdate -radix decimal /top_p2_tb/top_module/mdr_module/abs_X
add wave -noupdate -radix decimal /top_p2_tb/top_module/mdr_module/abs_Y
add wave -noupdate /top_p2_tb/top_module/mdr_module/a2_decomplement_X/o_sign
add wave -noupdate /top_p2_tb/top_module/mdr_module/a2_decomplement_Y/o_sign
add wave -noupdate -color {Violet Red} /top_p2_tb/top_module/start_debounced
add wave -noupdate /top_p2_t
b/top_module/load_debounced
add wave -noupdate -expand -group {OP CODE} /top_p2_tb/top_module/i_operation
add wave -noupdate -label {register_Op_Code/q Registered OPCODE} /top_p2_tb/top_module/register_Op_Code/q
add wave -noupdate /top_p2_tb/top_module/estados/current_state
add wave -noupdate -radix unsigned /top_p2_tb/top_module/display_result/conversion_bcd/number_aux
add wave -noupdate /top_p2_tb/top_module/decoder_bus
add wave -noupdate -radix decimal /top_p2_tb/top_module/display_result/conversion_bcd/o_units
add wave -noupdate -radix decimal /top_p2_tb/top_module/display_result/conversion_bcd/o_tens
add wave -noupdate -radix decimal /top_p2_tb/top_module/display_result/conversion_bcd/o_cents
add wave -noupdate -radix unsigned /top_p2_tb/top_module/display_result/conversion_bcd/o_sign
add wave -noupdate -radix decimal /top_p2_tb/top_module/display_result/conversion_bcd/tens_aux
add wave -noupdate -radix decimal /top_p2_tb/top_module/display_result/conversion_bcd/cents_aux
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {3378 us} 0} {{Cursor 2} {4186 us} 0} {{Cursor 3} {8675 us} 0} {{Cursor 4} {3707 us} 0} {{Cursor 5} {1913 us} 0} {{Cursor 6} {8804 us} 0} {{Cursor 7} {5730 us} 0}
quietly wave cursor active 5
configure wave -namecolwidth 378
configure wave -valuecolwidth 205
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits us
update
<<<<<<< HEAD
<<<<<<< HEAD
WaveRestoreZoom {1900 us} {2584 us}
=======
WaveRestoreZoom {2689 us} {5198 us}
>>>>>>> f213201d11e606b4b997e94dd7a1bb438d2006a5
=======
WaveRestoreZoom {0 us} {2509 us}
>>>>>>> 2bb1661c891fa27a307c47c8306a4d6ee09a7fab
