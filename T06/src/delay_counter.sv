import uart_pkg::*;

module delay_counter#(
//parameter i_max_cnt=10,
//parameter WORD=DELAY_DW
	)
	(
input  bit   i_clk,
input  logic i_reset_n,
input  logic i_enb,
input  cnt_t i_max_cnt,
output ovf_t o_ovf,
output cnt_t o_count
);

cntr_t   cntr     ;
cnt_t    cntr_nxt ;

always_ff@(posedge i_clk, negedge i_reset_n) begin: counter
    if (!i_reset_n)
        cntr.count         <=  '0      ;
    else if (i_enb)
        if (cntr.ovf)
            cntr.count     <= '0       ;
        else
            cntr.count     <= cntr_nxt ;
end:counter

always_comb begin: comparator
   cntr_nxt = cntr.count + 1'b1  ;
    if (cntr.count >= i_max_cnt-1'b1)
        cntr.ovf     =   1'b1    ;    
    else
        cntr.ovf     =   1'b0    ;
end:comparator

assign o_count    =   cntr.count   ;
assign o_ovf      =   cntr.ovf     ;

endmodule