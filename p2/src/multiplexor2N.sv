// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            March 8th, 2021
// File:			
// Module name:		
// Project Name:	
// Description:		

import top_p2_pkg::*;

module multiplexor2N(
    input datos_t i_first,//opcion 0
    input reg_2N_t i_second,//opcion 1
    input logic i_selector,
    output reg_2N_t o_selection
);

full_reg_t temp_output;

always_comb begin : multiplexor
    if(i_selector)begin
        temp_output = i_second;
    end else begin
        temp_output = {10'b0000000000,i_first};
    end
    
end

assign o_selection = temp_output;

endmodule