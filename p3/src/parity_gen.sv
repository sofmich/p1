// Coder:           Sofia Michel, Ethan Castillo
// Date:            April 7th, 2021
// File:			parity_gen.sv
// Description:		this module is a uart driver

 
import uart_pkg::*;

	
module parity_gen (
input uart_data_t i_data,
output logic o_parity_check
	);

assign o_parity_check = ((i_data[7] ^ i_data[6]) ^ (i_data[5] ^ i_data[4])) ^ ((i_data[3] ^ i_data[2]) ^ (i_data[1] ^ i_data[0]));

endmodule

