// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            February 15th, 2021
// File:			top_p1.sv
// Module name:		top_p1
// Project Name:	p1
// Description:		this is the top entity for a sequential multiplier 

import top_p1_pkg::*; 

module top_p1(
    input bit i_clk,  			// 50MHz from fpga
    input logic i_reset_n,
    input operand_t i_multd,
    input operand_t i_multr,
    input logic i_start,
    output bit o_ready,
    output product_t o_product,
    output segments_t  o_units,
    output segments_t  o_tens,
    output segments_t  o_cents,
    output logic o_sign
);

bit clk_10k;
logic start_debounced;
logic enable_data_path;
logic load_flag;
logic cleaning_flag;
logic display_flag;

`ifndef SIMULATION
    pll_50M_10k pll(
        .areset(i_reset_n),
        .inclk0(i_clk),
        .c0(clk_10k)  // 10 kHz

    );

    dbcr_top debouncer(
    .clk(clk_10k),
    .rst_n(i_reset_n),
    .Din(i_start),
    .one_shot(start_debounced)
    );

`endif

`ifdef SIMULATION
    assign start_debounced = i_start;
    assign clk_10k = i_clk;
`endif

control control_m(
    .i_clk(clk_10k),  
    .i_reset_n(i_reset_n),
    .i_start(start_debounced),  
    .o_ready(o_ready),
    .o_enable(enable_data_path),
    .o_load_flag(load_flag),
    .o_cleaning_flag(cleaning_flag),
    .o_display_flag(display_flag)
);

seq_multiplier_top data_path(
    .i_clk(clk_10k),
    .i_rst_n(i_reset_n),
    .i_start(start_debounced),
    .i_en_load(load_flag),
    .i_en_processing(enable_data_path),
    .i_en_output(display_flag),
    .i_clean(cleaning_flag),
    .i_multiplier(i_multr),
    .i_multiplicand(i_multd),
    .o_units(o_units),
    .o_tens(o_tens),
    .o_cents(o_cents),
    .o_sign(o_sign),
    .o_product(o_product)
);

endmodule