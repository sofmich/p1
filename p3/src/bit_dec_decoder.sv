
import uart_pkg::*;

module bit_dec_decoder(
    input data_t i_grant_shift_quantity,
    output data_t o_single_shift_quantity
);

    always_comb begin : blockName
        case (i_grant_shift_quantity)
            FIRST:begin
                o_single_shift_quantity = data_t'(ZERO);
            end
            SECOND:begin
                o_single_shift_quantity = data_t'(ONE);
            end
            THIRD:begin
                o_single_shift_quantity = data_t'(TWO);
            end
            FOURTH:begin
                o_single_shift_quantity = data_t'(THREE);
            end
            FIFTH:begin
                o_single_shift_quantity = data_t'(FOUR);
            end
            SIXTH:begin
                o_single_shift_quantity = data_t'(FIVE);
            end
            SEVENTH:begin
                o_single_shift_quantity = data_t'(SIX);
            end
            EIGHTH:begin
                o_single_shift_quantity = data_t'(SEVEN);
            end
            default:begin
                o_single_shift_quantity = data_t'(ZERO);
            end
        endcase
    end
endmodule