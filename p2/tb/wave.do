onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /top_p2_tb/clk
add wave -noupdate -radix decimal /top_p2_tb/top_module/registered_result
add wave -noupdate -radix decimal /top_p2_tb/top_module/registered_remainder
add wave -noupdate /top_p2_tb/top_module/load_debounced
add wave -noupdate -radix decimal /top_p2_tb/top_module/entry_reg_D/i_save_X
add wave -noupdate -radix decimal /top_p2_tb/top_module/entry_reg_D/i_save_Y
add wave -noupdate /top_p2_tb/top_module/entry_reg_D/q
add wave -noupdate /top_p2_tb/top_module/mdr_module/abs_X
add wave -noupdate /top_p2_tb/top_module/mdr_module/abs_Y
add wave -noupdate /top_p2_tb/top_module/mdr_module/registered_A
add wave -noupdate /top_p2_tb/top_module/mdr_module/registered_Q
add wave -noupdate /top_p2_tb/top_module/mdr_module/substracted_A
add wave -noupdate /top_p2_tb/top_module/mdr_module/restored_A
add wave -noupdate -expand -group {OP CODE} /top_p2_tb/top_module/i_operation
add wave -noupdate /top_p2_tb/top_module/registered_D
add wave -noupdate /top_p2_tb/top_module/multiplexor_D_selector
add wave -noupdate /top_p2_tb/top_module/shift_mux_selection
add wave -noupdate /top_p2_tb/top_module/shifted_D_2_left
add wave -noupdate /top_p2_tb/top_module/data
add wave -noupdate -color {Medium Violet Red} /top_p2_tb/top_module/mdr_module/full_vector_pre_reg
add wave -noupdate -color {Medium Violet Red} /top_p2_tb/top_module/mdr_module/registered_vector
add wave -noupdate /top_p2_tb/top_module/mdr_module/concatenation_R_Q_0
add wave -noupdate /top_p2_tb/top_module/mdr_module/mux_btw_0_and_1_selection
add wave -noupdate /top_p2_tb/top_module/mdr_module/mux_entry_Q_1
add wave -noupdate /top_p2_tb/top_module/mdr_module/post_add_subs_sqrt_mux_value
add wave -noupdate -group MDR -radix decimal /top_p2_tb/top_module/mdr_module/abs_X
add wave -noupdate -group MDR -radix decimal /top_p2_tb/top_module/mdr_module/abs_Y
add wave -noupdate -group MDR /top_p2_tb/top_module/mdr_module/full_vector_mult
add wave -noupdate -group MDR /top_p2_tb/top_module/mdr_module/full_vector_pre_reg
add wave -noupdate -group MDR /top_p2_tb/top_module/mdr_module/registered_vector
add wave -noupdate -group MDR /top_p2_tb/top_module/mdr_module/q0q1_selected_data
add wave -noupdate -group MDR /top_p2_tb/top_module/mdr_module/add_subs_result
add wave -noupdate /top_p2_tb/top_module/i_start
add wave -noupdate /top_p2_tb/top_module/Q_selector
add wave -noupdate /top_p2_tb/top_module/ready
add wave -noupdate /top_p2_tb/top_module/estados/current_state
add wave -noupdate /top_p2_tb/top_module/enable_D
add wave -noupdate -group adder /top_p2_tb/top_module/mdr_module/adder_element_1
add wave -noupdate -group adder /top_p2_tb/top_module/mdr_module/adder_element_2
add wave -noupdate -group adder /top_p2_tb/top_module/mdr_module/adder_result
add wave -noupdate -group substract /top_p2_tb/top_module/mdr_module/substract_result
add wave -noupdate -group substract /top_p2_tb/top_module/mdr_module/subs_element_1
add wave -noupdate -group substract /top_p2_tb/top_module/mdr_module/subs_element_2
add wave -noupdate /top_p2_tb/top_module/mdr_module/mux_pre_adder_Q_selection
add wave -noupdate /top_p2_tb/top_module/mdr_module/mux_pre_adder_R_selection
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1555 us} 0} {{Cursor 2} {1411 us} 0} {{Cursor 3} {3738 us} 0} {{Cursor 4} {683 us} 0} {{Cursor 5} {57 us} 0} {{Cursor 6} {2277 us} 0} {{Cursor 7} {2954 us} 0}
quietly wave cursor active 4
configure wave -namecolwidth 378
configure wave -valuecolwidth 144
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits us
update
WaveRestoreZoom {0 us} {937 us}
