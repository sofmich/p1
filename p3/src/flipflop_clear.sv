// Coder:           Ethan Castillo,Sofia Michel
// Date:            April 7th, 2021
// File:			uart_rx.sv
// Description:		This module is the reception interface for a uart driver

 
import uart_pkg::*;

	
module flipflop_clear#(
    parameter SIZE = DW

)(
    input bit       clk,
    input logic     rst_n,
    input logic		en,
    input logic[SIZE-1:0]    d, 
    output logic[SIZE-1:0]   q,
    input logic clean_register
);

logic[SIZE-1:0] q_aux;
	
	
always_ff@(posedge clk, negedge rst_n) begin
	if(!rst_n)
		q_aux <= '0;
	else if(en) /*This is PIPO register (array of D flip-flop)*/
		q_aux <= d;
    else if(clean_register)
		q_aux <= '0;
end

assign q =  q_aux;

endmodule