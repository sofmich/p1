// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            March 8th, 2021
// File:			
// Module name:		
// Project Name:	
// Description:		

import top_p2_pkg::*;

module multiplexor_OP_N(
    input datos_t i_vector_mult,//opcion 0
    input datos_t i_vector_div,//opcion 1
    input datos_t i_vector_sqrt, //opcion 2
    input op_code_t  i_op_code_selector,
    //input logic i_q0_value,
    //input datos_t i_q_value,
    output datos_t o_selection
);

datos_t temp_output;

always_comb begin : multiplexor
    case (i_op_code_selector)
        MULTIPLICATION: begin
            temp_output = i_vector_mult;
        end
        DIVISION: begin
            temp_output = i_vector_div;
        end
        SQRT: begin
            temp_output = i_vector_sqrt;
        end
        default: begin
            temp_output = '0;
        end
    endcase
end

assign o_selection = temp_output;

endmodule