// Coder:           Sofia Michel, Ethan Castillo
// Date:            April 7th, 2021
// File:			rx_state_machine.sv
// Description:		This module is the reception interface for a uart driver

 
import uart_pkg::*;
module mux8to1#(
parameter MUX_DW = DW
)(
    input logic[MUX_DW-1:0] in_1,
    input logic[MUX_DW-1:0] in_2,
    input logic[MUX_DW-1:0] in_3,
    input logic[MUX_DW-1:0] in_4,
    input logic[MUX_DW-1:0] in_5,
    input logic[MUX_DW-1:0] in_6,
    input logic[MUX_DW-1:0] in_7,
    input logic[MUX_DW-1:0] in_8,
   	input logic[MUX_DW-1:0] i_request_processing,
    output logic[MUX_DW-1:0] o_data_selected
);


always_comb begin : multiplexor
    case(i_request_processing)
	    FIRST:
	    	o_data_selected = in_1;
	    SECOND:
	    	o_data_selected = in_2;
	    THIRD:
	    	o_data_selected = in_3;
	    FOURTH:
	    	o_data_selected = in_4;
	    FIFTH:
	    	o_data_selected = in_5;
	    SIXTH:
	    	o_data_selected = in_6;
	    SEVENTH:
	    	o_data_selected = in_7;
	  //  EIGHT:
	   // 	o_data_selected = in_8;
	    default:
	    	o_data_selected = in_8;
    endcase
end

endmodule