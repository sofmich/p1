// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            February 8th, 2021
// File:			flipflopD_2N.sv
// Module name:		flipflopD_2N
// Project Name:	sequential_multiplier
// Description:		this is a flipflopD register module with data width of 2N
import top_p1_pkg::*;

module flipflopD_2N(
input bit       clk,
input bit       rst_n,
input bit		en,
input product_t    d,
output product_t   q

	);
product_t q_aux;
	
always_ff@(posedge clk, negedge rst_n) begin
	if(!rst_n)
		q_aux <= '0;
	else if(en) /*This is PIPO register (array of D flip-flop)*/
		q_aux <= d;
end

assign q = q_aux;

endmodule

