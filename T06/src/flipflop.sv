// Coder:           Ethan Castillo,Sofia Michel
// Date:            April 7th, 2021
// File:			uart_rx.sv
// Description:		This module is the reception interface for a uart driver

 
import uart_pkg::*;

	
module flipflop#(
    parameter SIZE = DW,
    type VAR_TYPE

)(
    input bit       clk,
    input logic     rst_n,
    input logic		en,
    input VAR_TYPE    d, 
    output VAR_TYPE   q
);

VAR_TYPE q_aux;
	
	
always_ff@(posedge clk, negedge rst_n) begin
	if(!rst_n)
		q_aux <= '0;
	else if(en) /*This is PIPO register (array of D flip-flop)*/
		q_aux <= d;
end

assign q = q_aux;

endmodule