onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_mdr/itf/clk
add wave -noupdate /tb_mdr/itf/op
add wave -noupdate /tb_mdr/itf/data
add wave -noupdate /tb_mdr/itf/result
add wave -noupdate /tb_mdr/itf/remainder
add wave -noupdate /tb_mdr/itf/load
add wave -noupdate /tb_mdr/itf/start
add wave -noupdate /tb_mdr/itf/load_x
add wave -noupdate /tb_mdr/itf/load_y
add wave -noupdate /tb_mdr/itf/error
add wave -noupdate /tb_mdr/itf/ready
add wave -noupdate /tb_mdr/uut/pp/en_load_X
add wave -noupdate /tb_mdr/uut/pp/en_load_Y
add wave -noupdate /tb_mdr/uut/pp/o_ready
add wave -noupdate -color Gold /tb_mdr/uut/pp/o_error
add wave -noupdate -color {Violet Red} /tb_mdr/uut/pp/error_mdr/i_en_post_error
add wave -noupdate /tb_mdr/uut/pp/error_mdr/i_en_pre_error
add wave -noupdate -color Red /tb_mdr/uut/pp/error_mdr/o_post_error
add wave -noupdate /tb_mdr/uut/pp/error_mdr/o_pre_error
add wave -noupdate /tb_mdr/uut/pp/estados/current_state
add wave -noupdate /tb_mdr/uut/pp/register_Op_Code/q
add wave -noupdate -radix decimal /tb_mdr/uut/pp/error_mdr/i_product_vector
add wave -noupdate -color Pink -radix decimal /tb_mdr/uut/pp/registered_result
add wave -noupdate /tb_mdr/uut/pp/o_result
add wave -noupdate /tb_mdr/uut/pp/rmndr_overflow/i_register_remainder
add wave -noupdate -radix binary /tb_mdr/uut/pp/error_mdr/aux_mult_error
add wave -noupdate /tb_mdr/uut/pp/error_mdr/aux_mult_error_abs
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 7} {43158933 ps} 0} {{Cursor 2} {44169087848 ps} 0} {{Cursor 3} {43186668 ps} 0} {{Cursor 4} {44083149132 ps} 0} {{Cursor 5} {44169084117 ps} 0} {{Cursor 6} {44083157506 ps} 0} {{Cursor 7} {44083210364 ps} 0}
quietly wave cursor active 6
configure wave -namecolwidth 315
configure wave -valuecolwidth 158
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {44083136177 ps} {44083223174 ps}
