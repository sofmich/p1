// Coder:           Eduardo Ethandrake Castillo, Sofia Michel Salazar Valdovinos
// Date:            March 8th, 2021
// File:			
// Module name:		
// Project Name:	
// Description:		

import top_p2_pkg::*;

module sqrt_error(
    input datos_t i_signed_num,
    output bit o_div_error 
);

assign o_div_error = (i_signed_num[N-1] == 1'b1)? 1'b1 : 1'b0;
 
endmodule