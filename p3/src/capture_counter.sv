import uart_pkg::*;

module capture_counter//#(
//parameter i_max_cnt=DW
//parameter WORD=DELAY_DW
//	)
(
        
    input  bit   i_clk,
    input  logic i_reset_n,
    input  logic i_enb,
    input  data_t i_max_cnt,
    input  logic i_stimulus,
    output ovf_t o_ovf
   // output cnt_t o_count
);

cntr_t   cntr     ;
cnt_t    cntr_nxt ;

always_ff@(posedge i_clk, negedge i_reset_n) begin: counter
    if (!i_reset_n)
        cntr.count         <= '0       ;
    else if (i_enb)
        if (cntr.ovf)
            cntr.count     <= '0       ;
        else if(i_stimulus)
            cntr.count     <= cntr_nxt ;
end:counter

always_comb begin: comparator
   cntr_nxt = cntr.count + 1'b1  ;
    if (cntr.count >= i_max_cnt)
        cntr.ovf     =   1'b1    ;    
    else
        cntr.ovf     =   1'b0    ;
end:comparator

//assign o_count    =   cntr.count   ;
assign o_ovf      =   cntr.ovf     ;

endmodule